#!/usr/bin/env python3

import numpy as np
from numpy.lib.recfunctions import append_fields
import os

import scipy.optimize as opt
from scipy.interpolate import interp1d
from scipy import ndimage

from rw_utils import *
from xsbook import xsLookup
from dsid_to_mass import dsid_to_mass_dict 
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-i", "--input_dir", dest="input_dir", default="",
                  help="Input directory")
parser.add_option("-o", "--output_dir", dest="output_dir", default="",
                  help="Output directory")
parser.add_option("-y", "--year", dest="year", default="2015",
                  help="Year")
parser.add_option("--signal-list", dest="signal_list", default="",
                  help="Input text file with signal sample dirs")
parser.add_option("-r", "--release", dest="rel", default="20",
                  help="Release 20 or 21")
parser.add_option("--HCrescale",
                  action="store_true", dest="HCrescale", default=False,
                  help="Rescale HC 4 vecs to have m=125 GeV")
parser.add_option("-l", "--label", dest="label", default="",
                  help="Label for output")

(options, args) = parser.parse_args()

import root_numpy
import ROOT
from ROOT import TLorentzVector, TFile, TParameter
from rootpy.plotting import *
ROOT.gROOT.SetBatch(ROOT.kTRUE)

year_in = options.year
input_dir = options.input_dir
output_dir = options.output_dir
rel = int(options.rel[:2])
label= options.label
signal_list_file = options.signal_list
HCrescale = options.HCrescale

if HCrescale:
    label+= "_HCrescale"

if label:
   if label[0] != '_':
       label = '_'+label

if rel != 20 and rel !=21:
    rel = 20
    print("Invalid option, defaulting to rel 20")

year =''
yr_short=''
if len(year_in) == 4:
    year=year_in
    yr_short=year_in[2:]
elif len(year_in) == 2:
    year='20'+year_in
    yr_short=year_in
else:
    print("Invalid year format, defaulting to 2015")
    year='2015'
    yr_short='15'


if input_dir:
    if input_dir[-1] != '/': input_dir+='/'

if output_dir:
    if output_dir[-1] != '/': output_dir+='/'

scalar_masses = []
scalar_syst_names = {}
smnr_syst_files = []
smnr_syst_names = []
scalar_syst_files = {}
if signal_list_file:
    mass_dict = dsid_to_mass_dict()
    period_dict = { '2015': 'mc16a', '2016': 'mc16a', '2017': 'mc16d', '2018': 'mc16e'}
    with open(signal_list_file) as f:
        for line in f:
            if line.find(period_dict[year]) != -1:
                if line.find("450000") != -1:
                    dir_name = line.rstrip()
                    all_smnr_files=os.listdir(dir_name)
                    for fname in all_smnr_files:
                        if fname.find("UP.root") != -1 or fname.find("DOWN.root") != -1:
                            smnr_syst_files.append(os.path.join(dir_name, fname))
                            smnr_syst_names.append(os.path.splitext(fname)[0])

                s_idx = line.find("4502")
                if s_idx != -1:
                    s_mass = mass_dict[line[s_idx:s_idx+6]]
                    scalar_syst_files[s_mass] = []
                    scalar_syst_names[s_mass] = []
                    dir_name = line.rstrip()
                    all_scalar_files=os.listdir(dir_name)
                    for fname in all_scalar_files:
                        if fname.find('nominal.root') != -1:
                            scalar_masses.append(s_mass)
                        if fname.find("UP.root") != -1 or fname.find("DOWN.root") != -1:
                            scalar_syst_files[s_mass].append(os.path.join(dir_name, fname))
                            scalar_syst_names[s_mass].append(os.path.splitext(fname)[0])


#Set up appropriate columns for data, mc, and rw. Data/MC slightly different due to some weights
columns = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2',
           'njets', 'ntag', 'm_h1', 'm_h2', 'run_number', 'm_hh', 'X_wt']

columns_mc = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2',
              'njets', 'ntag', 'm_h1', 'm_h2', 'mc_sf', 'run_number', 'm_hh', 'X_wt']

rw_columns = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2']

vec4_vars = ['pT_h1', 'eta_h1', 'phi_h1', 'm_h1', 'pT_h2', 'eta_h2', 'phi_h2', 'm_h2']


if HCrescale:
    for var in vec4_vars:
        if var not in columns: 
            columns += [var]
        if var not in columns_mc: 
            columns_mc += [var]

it_smnr_systs = {}

print("About to load SM HH")
for i in range(len(smnr_syst_files)):
    print(smnr_syst_names[i])
    it_smnr_systs[smnr_syst_names[i]] = root_numpy.root2array(smnr_syst_files[i],
                                                              treename='sig', branches=columns_mc)

print("Loaded SM HH systs")

print("About to load scalar")
it_scalar_systs = {}
for mass in scalar_masses:
    print("Mass =", mass,"GeV")
    it_scalar_systs[mass] = {}
    to_remove = []
    for i in range(len(scalar_syst_files[mass])):
        try:
            it_scalar_systs[mass][scalar_syst_names[mass][i]] = root_numpy.root2array(scalar_syst_files[mass][i],
                                                                                      treename='sig', branches=columns_mc)
        except:
            print("Unable to load "+scalar_syst_names[mass][i], "for scalar mass", mass,"GeV. Skipping for now")
            to_remove.append(scalar_syst_names[mass][i])
    for name in to_remove:
        scalar_syst_names[mass].remove(name)

    print("Loaded scalar systs, mass", mass)

  
#Grab appropriate events for each year's triggers. Run numbers taken from resolved-recon's trigger list
run_bounds = { '2015' : [0, 296939], '2016' : [296939, 324320],
               '2017' : [324320, 348197], '2018' : [348197, 364486] }
year_run_range = run_bounds[year]

lumi15 = 3.23749*(rel==20)+3.2195*(rel==21)
lumi16 = 24.3219*(rel==20)+24.5556*(rel==21)
lumi17 = 44.3072
lumi18 = 36.1593
lumi = (year=='2015')*lumi15 + (year=='2016')*lumi16 + (year=='2017')*lumi17 + (year=='2018')*lumi18

#For rel21 15+16, overall normalization is to combined 15+16 lumi
if rel == 21 and (year == '2015' or year == '2016'):
    lumi = lumi15+lumi16

if rel == 20 and (year == '2017' or year == '2018'):
    print('Warning! Rel 20 should not exist for', year,'. Continuing, but check inputs!')

if not lumi:
    print('Warning! No lumi specified!!')

lim_file = ROOT.TFile(output_dir+"resolved_4bSR"+label+"_"+year+"_signal_systematics.root", "recreate")

smnr_weights = {}
for name in smnr_syst_names:
    run_numbers = it_smnr_systs[name]['run_number']
    it_smnr_systs[name] = it_smnr_systs[name][(run_numbers > year_run_range[0]) & (run_numbers < year_run_range[1])]
    it_smnr_systs[name] = it_smnr_systs[name][it_smnr_systs[name]['ntag']>=4]
    smnr_weights[name] = it_smnr_systs[name]['mc_sf']*lumi

    if HCrescale:
        print("About to rescale m_hh for SM HH syst,", name)
        h1 = TLorentzVector()
        h2 = TLorentzVector()
        for event in range(len(it_smnr_systs[name])):
            if event % 10000 == 0: print(event, "out of", len(it_smnr_systs[name]), "for SM HH syst", name)
            h1.SetPtEtaPhiM(it_smnr_systs[name]['pT_h1'][event], it_smnr_systs[name]['eta_h1'][event], 
                            it_smnr_systs[name]['phi_h1'][event], it_smnr_systs[name]['m_h1'][event])
            h2.SetPtEtaPhiM(it_smnr_systs[name]['pT_h2'][event], it_smnr_systs[name]['eta_h2'][event], 
                            it_smnr_systs[name]['phi_h2'][event], it_smnr_systs[name]['m_h2'][event])
            it_smnr_systs[name]['m_hh'][event] = corr_mass(h1,h2)
  
    for column in ['m_hh']:
        n_bins= 66
        xlim = [100,1400]

        smnr_label = "sm_hh_"+name+"_4"
        smnr_title = "sm_hh_"+name
        hist_smnr = Hist(n_bins, xlim[0], xlim[1], title=smnr_label, legendstyle='F',drawstyle='hist')
        hist_smnr.fill_array(it_smnr_systs[name][column], weights=smnr_weights[name])
        hist_smnr = makePositive_hist(hist_smnr)
        hist_smnr.Write(smnr_title)



scalar_weights = {}
for mass in scalar_masses:
    scalar_weights[mass] = {}
    for name in scalar_syst_names[mass]:
        run_numbers = it_scalar_systs[mass][name]['run_number']
        it_scalar_systs[mass][name] = it_scalar_systs[mass][name][(run_numbers > year_run_range[0]) & (run_numbers < year_run_range[1])]
        it_scalar_systs[mass][name] = it_scalar_systs[mass][name][it_scalar_systs[mass][name]['ntag']>=4]
        scalar_weights[mass][name] = it_scalar_systs[mass][name]['mc_sf']*lumi

        if HCrescale:
            h1 = TLorentzVector()
            h2 = TLorentzVector()
            for event in range(len(it_scalar_systs[mass][name])):
                if event % 10000 == 0: print(event, "out of", len(it_scalar_systs[mass][name]), "for scalar syst", name+", mass", mass, "GeV")
                h1.SetPtEtaPhiM(it_scalar_systs[mass][name]['pT_h1'][event], it_scalar_systs[mass][name]['eta_h1'][event], 
                                it_scalar_systs[mass][name]['phi_h1'][event], it_scalar_systs[mass][name]['m_h1'][event])
                h2.SetPtEtaPhiM(it_scalar_systs[mass][name]['pT_h2'][event], it_scalar_systs[mass][name]['eta_h2'][event], 
                                it_scalar_systs[mass][name]['phi_h2'][event], it_scalar_systs[mass][name]['m_h2'][event])
                it_scalar_systs[mass][name]['m_hh'][event] = corr_mass(h1,h2)


        for column in ['m_hh']:
            n_bins= 66
            xlim = [100,1400]

            s_label = ("scalar_M%d_"+name+"_4") % mass
            s_title = ("s_hh_m%d_"+name) % mass
            hist_scalar = Hist(n_bins, xlim[0], xlim[1], title=s_label, legendstyle='F',drawstyle='hist')
            hist_scalar.fill_array(it_scalar_systs[mass][name][column], weights=scalar_weights[mass][name])
            hist_scalar = makePositive_hist(hist_scalar)
            hist_scalar.Write(s_title)

lim_file.Close()
