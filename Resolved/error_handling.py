import numpy as np
from rootpy.plotting import Hist
from numpy.lib.recfunctions import append_fields

class errors:
    def __init__(self, bkgd_arr, year, bins):
        self.bkgd_arr = bkgd_arr
        self.year = year
        self.yr_short = year[-2:]
        self.bins = bins

    #Run bootstrap errors
    def runBootstrap(self, BDT_norms, BDT_norm_nom):
        arr = self.bkgd_arr
        bins = self.bins
        yr_short = self.yr_short

        n_bootstraps = 0
        for colname in arr.dtype.names:
            if colname.find("resampling") != -1:
                n_bootstraps+=1

        hists_for_std = []
        for bstrap in range(n_bootstraps):
            hist_for_bstrap = Hist(bins)
            hist_for_bstrap.fill_array(arr['m_hh'], weights=arr[('BDT_d24_weight_resampling_%d_'+yr_short)%bstrap])

            hist_for_bstrap.Scale(BDT_norms[bstrap])
            hists_for_std.append(hist_for_bstrap)

        bstrap_errs = []
        for binx in range(1, hists_for_std[0].GetNbinsX()+1):
            bin_vals = []
            for hist in hists_for_std:
                bin_vals.append(hist.GetBinContent(binx))
            bstrap_errs.append(np.std(bin_vals))


        hist_qcd = Hist(bins, title='BDT_hh', legendstyle='F',drawstyle='hist')
        hist_qcd.fill_array(arr['m_hh'], weights=arr['BDT_d24_weight_'+yr_short])
        hist_qcd.Scale(BDT_norm_nom)

        for binx in range(1, hist_qcd.GetNbinsX()+1):
            poisson_err = hist_qcd.GetBinError(binx)
            bstrap_err = bstrap_errs[binx-1]
            tot_error=np.sqrt(bstrap_err**2+poisson_err**2)
            hist_qcd.SetBinError(binx, tot_error)
 
        return hist_qcd

    def runShapeSystematics(self, BDT_norm, BDT_norm_CRderiv):
        arr = self.bkgd_arr
        bins = self.bins
        yr_short = self.yr_short

        reg_arrs = {}

        dataHT = arr['pT_h1_j1'] + arr['pT_h1_j2'] + arr['pT_h2_j1'] + arr['pT_h2_j2']
        arr = append_fields(arr, 'H_T', dataHT, usemask=False)

        reg_list = ['', 'LowHt', 'HighHt', 'LowHtCRw', 'HighHtCRw']
  
        reg_arrs['']        = arr.copy() 
        reg_arrs['LowHt']   = arr[arr['H_T'] < 300]
        reg_arrs['HighHt']  = arr[arr['H_T'] >= 300]
        reg_arrs['LowHtCRw']   = reg_arrs['LowHt'].copy()
        reg_arrs['HighHtCRw']  = reg_arrs['HighHt'].copy()

        store_for_symm = {}
        store_for_add = {}
        for reg in reg_list:
            BDT_weight_label = 'BDT_d24_weight_'+yr_short
            if reg.find('CRw') != -1:
                BDT_weight_label = 'BDT_d24_weight_CRderiv_'+yr_short

            hist_qcd = Hist(bins, title='BDT_hh', legendstyle='F',drawstyle='hist')
            hist_qcd.fill_array(reg_arrs[reg]['m_hh'], weights=reg_arrs[reg][BDT_weight_label])
            if reg.find("CRw") != -1:
                hist_qcd.Scale(BDT_norm_CRderiv)
            else:
                hist_qcd.Scale(BDT_norm)

            store_for_symm[reg] = hist_qcd.Clone()
            store_for_add[reg] = hist_qcd.Clone()


        store_for_symm['LowHt'].Scale(2.)
        store_for_symm['LowHt'].Add(store_for_symm['LowHtCRw'],-1)
        store_for_add['LowHtCRi'] = store_for_symm['LowHt'].Clone()

        store_for_symm['HighHt'].Scale(2.)
        store_for_symm['HighHt'].Add(store_for_symm['HighHtCRw'],-1)
        store_for_add['HighHtCRi'] = store_for_symm['HighHt'].Clone()

        store_for_add['LowHtCRw'].Add(store_for_add['HighHt'])
        store_for_add['HighHtCRw'].Add(store_for_add['LowHt'])

        store_for_add['LowHtCRi'].Add(store_for_add['HighHt'])
        store_for_add['HighHtCRi'].Add(store_for_add['LowHt']) 

        return store_for_add
