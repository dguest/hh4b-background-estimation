#!/usr/bin/env python3

import numpy as np
from rw_utils import *
from xs_old import old_scalar_xs
from xsbook import xsLookup
import matplotlib.pyplot as plt
import os

from optparse import OptionParser

parser = OptionParser()

parser.add_option("-i", "--input_dir", dest="input_dir", default="",
                  help="Input directory")
parser.add_option("--old-limit", dest="old_limit_dir", default="",
                  help="Directory for old limits to compare")
parser.add_option("-l", "--label", dest="label", default="",
                  help="Label")

(options, args) = parser.parse_args()

input_dir=options.input_dir
old_limit_dir = options.old_limit_dir
label = options.label

if input_dir.find("HCrescale") != -1:
    label += '_HCrescale'

if input_dir.find("Shape") != -1:
    label+='_ShapeSyst'

if input_dir.find("NoSyst") != -1:
    label+="_StatOnly"

if input_dir.find("AllSyst") != -1:
    label+="_AllSyst"

if label and label[0] != '_':
    label = '_' + label

if input_dir:
    if input_dir[-1] != '/':
        input_dir+='/'

old_label=''
if old_limit_dir:
    if old_limit_dir[-1] != '/':
        old_limit_dir+='/'
    old_label='_paper_comparison'

year=''
yr_short=''

isg10 = False
isScalar = False
isHH = False

if input_dir.find("g_hh_c10") != -1: 
    print("Comparing limits for c=1.0 graviton")
    isg10 = True
if input_dir.find("sm_hh") != -1:
    print("Comparing limits for SM HH")
    isHH = True
if input_dir.find("s_hh") != -1:
    print("Comparing limits for scalar")
    isScalar = True

if input_dir.find("BDT") != -1:
    back_label = "_BDT"
    print("BDT background")
else:
    back_label="_spline"
    print("Spline background")

if input_dir.find("2016") != -1: 
    print("Year is 2016")
    year = '2016'
    yr_short = '16'

if input_dir.find("2017") != -1:
    print("Year is 2017")
    year = '2017'
    yr_short = '17'

if input_dir.find("2018") != -1:
    print("Year is 2018")
    year = '2018'
    yr_short = '18'

if input_dir.find("combined") != -1:
    print("Combined years")
    year = 'combined'
    yr_short = 'combined'

if (not isg10 and not isHH and not isScalar) or (year==''):
    print("Can't plot! Check inputs")


lims = {}
masses_str = []

import ROOT
for file in os.listdir(input_dir):
     f = ROOT.TFile.Open(input_dir+file)
     lim_hist = f.Get("limit")

     mass = file[:-5]
     masses_str.append(mass)
     lims[mass] = {'obs': [], 'exp' : [], '2s_u' : [], '1s_u' : [],'1s_l' : [],'2s_l' : []}

     if mass == "-999":
         #xs = xsLookup("SMNR", 20, with_init=False)
         xs = 1.
     else:
         if isg10:
             xs = xsLookup("g10_M"+mass, 21, with_init=False)
         elif isScalar:
             xs = xsLookup("scalar_M"+mass, 21, with_init=False)
         else:
             xs = 1.
     
     for i in range(len(lims[mass].keys())):
         key = list(lims[mass].keys())[i]
         lims[mass][key] = lim_hist.GetBinContent(i+1)*xs

if old_limit_dir:
    old_lims = {}
    old_file_list = os.listdir(old_limit_dir)
    for mass in masses_str:
        for old_file in old_file_list:
            if old_file.find(mass+'.root') != -1:
                f_old = ROOT.TFile.Open(old_limit_dir+old_file)
                old_lim_hist = f_old.Get("limit")
                old_lims[mass] = {'obs': [], 'exp' : [], '2s_u' : [], '1s_u' : [],'1s_l' : [],'2s_l' : []}
             
                xs=1.
                if isScalar:
                    xs = old_scalar_xs(mass)
                for i in range(len(lims[mass].keys())):
                    key = list(old_lims[mass].keys())[i]
                    old_lims[mass][key] = old_lim_hist.GetBinContent(i+1)*xs
   

exp = []
m_2s = []
m_1s = []
p_1s = []
p_2s = []

if old_limit_dir:
    exp_old = []
    m_2s_old = []
    m_1s_old = []
    p_1s_old = []
    p_2s_old = []

masses_str.sort(key=lambda x: int(x))

nom_xs = []

masses = []

for mass in masses_str:
    if mass == "-999":
        print("SMNR")
        print('{:^15}'.format('Exp Limit (SM xs)'), '{:^15}'.format('-2 sigma'), '{:^15}'.format('-1 sigma'), '{:^15}'.format('+1 sigma'), '{:^15}'.format('+2 sigma'))
        print('{:^15}'.format('%.2f' % lims[mass]['exp']), '{:^15}'.format('%.2f' % lims[mass]['2s_l']), '{:^15}'.format('%.2f' % lims[mass]['1s_l']),
                             '{:^15}'.format('%.2f' % lims[mass]['1s_u']), '{:^15}'.format('%.2f' % lims[mass]['2s_u']))

        print("")
        xs = xsLookup("SMNR", 20, with_init=False)
        print("Using xs = %.2f fb" % xs)
        print('{:^15}'.format('Exp Limit (SM xs)'), '{:^15}'.format('-2 sigma'), '{:^15}'.format('-1 sigma'), '{:^15}'.format('+1 sigma'), '{:^15}'.format('+2 sigma'))
        print('{:^15}'.format('%.2f' % (lims[mass]['exp']*xs)), '{:^15}'.format('%.2f' % (lims[mass]['2s_l']*xs)), 
                                     '{:^15}'.format('%.2f' % (lims[mass]['1s_l']*xs)),
                                     '{:^15}'.format('%.2f' % (lims[mass]['1s_u']*xs)), '{:^15}'.format('%.2f' % (lims[mass]['2s_u']*xs)))

        sig_label = "SMNR" 
    else:
        if isg10:
            sig_label = "g10_M"+mass
        elif isScalar:
            sig_label = "scalar_M"+mass
        else:
            sig_label = ""

    exp.append(lims[mass]['exp'])
    m_2s.append(lims[mass]['2s_l'])
    m_1s.append(lims[mass]['1s_l'])
    p_1s.append(lims[mass]['1s_u'])
    p_2s.append(lims[mass]['2s_u'])

    if old_limit_dir:
        exp_old.append(old_lims[mass]['exp'])
        m_2s_old.append(old_lims[mass]['2s_l'])
        m_1s_old.append(old_lims[mass]['1s_l'])
        p_1s_old.append(old_lims[mass]['1s_u'])
        p_2s_old.append(old_lims[mass]['2s_u'])

    masses.append(int(mass))

    print(sig_label)
    nom_xs.append(xsLookup(sig_label, 21, with_init=False))

if len(masses) > 0:
    from rootpy.plotting.style import get_style, set_style
    from matplotlib.ticker import AutoMinorLocator, MultipleLocator

    set_style('ATLAS', mpl=True)

    if masses[0] == -999 and len(masses) == 1:
        fig, ax = plt.subplots(figsize=(6,6))

        ax.fill_between([-1000,-998], m_2s*2, p_2s*2, color='yellow', label='Expected $\pm$ 2$\sigma$')
        ax.fill_between([-1000,-998], m_1s*2, p_1s*2, color='chartreuse', label='Expected $\pm$ 1$\sigma$')
        ax.plot([-1000,-998], exp*2, 'k--', label='Expected Limit (95% CL)')

        ax.plot([-1000,-998], [1,1], 'r-', label='SM HH',
                linewidth=2)
        ax.set_xticks(masses)
        ax.set_xticklabels(['SM HH'], fontsize=18)

        ax.legend()
        ax.set_yscale('log')
        ax.axis([-1000.5, -997.5, 0.8, 1500.])
        ax.set_ylabel(r'$\sigma(pp\rightarrow H^{*} \rightarrow HH \rightarrow b\overline{b}b\overline{b})$ [SM xs]',
                      horizontalalignment='right', y=1.0)
        ax.tick_params(direction='in', which='both')
 
        fig.tight_layout()
        fig.savefig('SMNR_limit'+label+back_label+'_'+year+'SMxs.pdf')


        #Scale by xs now
        xs = xsLookup("SMNR", 20, with_init=False)
        exp = [a*xs for a in exp]
        m_2s = [a*xs for a in m_2s]
        m_1s = [a*xs for a in m_1s]
        p_1s = [a*xs for a in p_1s]
        p_2s = [a*xs for a in p_2s]

        fig, ax = plt.subplots(figsize=(6,6))

        ax.fill_between([-1000,-998], m_2s*2, p_2s*2, color='yellow', label='Expected $\pm$ 2$\sigma$')
        ax.fill_between([-1000,-998], m_1s*2, p_1s*2, color='chartreuse', label='Expected $\pm$ 1$\sigma$')
        ax.plot([-1000,-998], exp*2, 'k--', label='Expected Limit (95% CL)')

        ax.plot([-1000,-998], nom_xs*2, 'r-', label='SM HH',
                  linewidth=2)
        ax.set_xticks(masses)
        ax.set_xticklabels(['SM HH'], fontsize=18)

        ax.legend()
        ax.set_yscale('log')
        ax.axis([-1000.5, -997.5, 0.8, 15000.])
        ax.set_ylabel(r'$\sigma(pp\rightarrow H^{*} \rightarrow HH \rightarrow b\overline{b}b\overline{b})$ [fb]',
                       horizontalalignment='right', y=1.0)
        ax.tick_params(direction='in', which='both')

        ax.text(0.33, 0.2, label[1:]+", "+back_label[1:]+" "+year,
                verticalalignment='center', horizontalalignment='center',
                transform=ax.transAxes, fontsize=18)


        fig.tight_layout()
        fig.savefig('SMNR_limit'+label+back_label+'_'+year+'.pdf')


    else:
        if isg10:
            plot_label ='Bulk RS, k/$\overline{M}_{Pl} = 1.0$'
            x_label = 'm$_{G_{KK}^{*}}$ [GeV]'
            y_label = r'$\sigma(pp\rightarrow G_{KK}^{*} \rightarrow$ HH $\rightarrow b\overline{b}b\overline{b})$ [fb]'
            out_name='GKK_limit'+label+back_label+'_'+year+'.pdf'
            y_range = [0.8, 15000.]
        if isScalar:
            x_label = 'm(Scalar) [GeV]'
            y_label = r'$\sigma(pp\rightarrow$ Scalar $\rightarrow$ HH $\rightarrow b\overline{b}b\overline{b})$ [fb]'
            out_name='scalar_limit'+label+back_label+'_'+year+old_label+'.pdf'
            y_range = [0.8, 15000.]

        fig, ax = plt.subplots()

        ax.fill_between(masses, m_2s, p_2s, color='yellow', label='Expected $\pm$ 2$\sigma$')
        ax.fill_between(masses, m_1s, p_1s, color='chartreuse', label='Expected $\pm$ 1$\sigma$')
        ax.plot(masses, exp, 'k--', label='Expected Limit (95% CL)')

        if old_limit_dir:
            ax.plot(masses, exp_old, 'r--', label='Expected Limit (2015+16 Paper)')

        if isg10:
            ax.plot(masses, nom_xs, 'r-', label=plot_label,
                    linewidth=2)
        ax.legend()
        ax.set_yscale('log')
        ax.axis([masses[0]-50, masses[-1]+50, y_range[0], y_range[1]])
        ax.set_xlabel(x_label, horizontalalignment='right', x=1.0)
        ax.set_ylabel(y_label,
               horizontalalignment='right', y=1.0)
        ax.tick_params(direction='in', which='both')
        ax.xaxis.set_minor_locator(AutoMinorLocator())

        ax.text(0.33, 0.2, label[1:]+", "+back_label[1:]+" " +year,
                verticalalignment='center', horizontalalignment='center',
                transform=ax.transAxes, fontsize=18)

        fig.savefig(out_name)

