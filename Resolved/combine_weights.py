#!/usr/bin/env python3
import numpy as np
import os

from optparse import OptionParser

def commasep_callback(option, opt, value, parser):
    setattr(parser.values, option.dest, value.split(','))

parser = OptionParser()
parser.add_option("-i", "--input_dir", dest="input_dir", default="",
                  help="Input directory")
parser.add_option("-d", "--data", dest="data_file", default="",
                  help="Input data filename")
parser.add_option("-t", "--tth", dest="tth_file", default="",
                  help="Input all had ttbar filename")
parser.add_option("-n", "--tnh", dest="tnh_file", default="",
                  help="Input non all-had ttbar filename")
parser.add_option("-w", "--weight_dir", dest="weight_dir", default="",
                   help="Directory with weight files")
parser.add_option("-b", "--BDT",
                  action="store_true", dest="BDT", default=False,
                  help="Turn on storage of BDT weight")
parser.add_option("-s", "--spline",
                  dest="spline", default=-1,
                  help="Turn on storage of spline weight, give number of iters. If default (=-1) don't store.")
parser.add_option("-y", "--year", dest="year", default="2015",
                  type='string',
                  action='callback',
                  callback=commasep_callback,
                  help="Year. If more than one, comma separate (2015,2016) - branches written with year subscript. If you want to do inclusive, pass option all.")
parser.add_option("-o", "--output_str", dest="output_str", default="with_weights",
                  help="Writes tree to file with argument as suffix. Use --append to write onto input file.")
parser.add_option("-a", "--append",
                  action="store_true", dest="append", default=False,
                  help="Turn on write to input file")
parser.add_option("-r", "--release", dest="rel", default="20",
                  help="Release 20 or 21")

parser.add_option("-l", "--label", dest="label", default="",
                  help="Label for output file (e.g., pflow)")

parser.add_option("--isCRderiv",
                  action="store_true", dest="isCRderiv", default=False,
                  help="Adjust labeling for CR derived weights")

parser.add_option("--bootstrap", dest="n_resamples", default="-1",
                  help="Turn on bootstrapping. Put in number of resamples.")

parser.add_option("--n_jobs", dest="n_jobs", default="",
                  help="Total number of jobs (to be used for bootstrap only)") 

parser.add_option("--BDTttbar",
                  action="store_true", dest="BDTttbar", default=False,
                  help="Turn on BDT weight for ttbar")

(options, args) = parser.parse_args()


from ROOT import TFile, TTree, TObject, TParameter
from root_numpy import root2array, array2tree

BDT =options.BDT
append = options.append
n_it = int(options.spline)
spline = (n_it != -1)
input_dir = options.input_dir
weight_dir = options.weight_dir
data_file = options.data_file
tth_file = options.tth_file
tnh_file = options.tnh_file
year_in = options.year
output_str = options.output_str
rel = int(options.rel[:2])
label=options.label
isCRderiv = options.isCRderiv
doBDTttbar = options.BDTttbar
n_resamples=int(options.n_resamples)
n_jobs = options.n_jobs

if label:
    if label[0] != '_':
        label = '_'+label

doBootstrap = False
if n_resamples != -1:
    print("Combining bootstrapping from  %d total resamples" % n_resamples)
    doBootstrap = True
    label+='_bootstrap'

if n_jobs and not doBootstrap:
    print("You should only use n_jobs for bootstrap! Check inputs")

if not n_jobs:
    n_jobs = 0

n_jobs=int(n_jobs)

reg_label=''
if isCRderiv:
    reg_label="_CRderiv" 

if rel != 20 and rel !=21:
    rel = 20
    print("Invalid option, defaulting to rel 20")

if output_str != "":
    if output_str[-5:] == '.root':
        output_str = output_str[:-5]
    if output_str[0] == '_':
        output_str = output_str[1:]

year =[]
yr_short = []
out_combine = (year_in[0] == 'all')
for yr in year_in:
    if yr == 'all':
       year+=['2015','2016','2017','2018']
       yr_short+=['15','16','17','18']
    elif len(yr) == 4:
        year.append(yr)
        yr_short.append(yr[2:])
    elif len(yr) == 2:
        year.append('20'+yr)
        yr_short.append(yr)
    else:
        print("Invalid year format, defaulting to 2015")
        year.append('2015')
        yr_short.append('15')
        continue

if input_dir:
    if input_dir[-1] != '/': input_dir+='/'

if weight_dir:
    if weight_dir[-1] != '/': weight_dir+='/'
else:
    weight_dir = input_dir

do_data = False
do_tth = False
do_tnh = False

if data_file:
    do_data = True
    if data_file[0] == '/':
        input_dir = ''
    data_file = input_dir+data_file
    if data_file[-4:] != 'root':
        print("Warning! Data file not a root file")
    if data_file.find(yr_short[0]) == -1:
        print("Warning! Input data file name may not match year.")

if tth_file:
    do_tth = True
    if tth_file[0] == '/':
        input_dir = ''
    tth_file = input_dir+tth_file
    if tth_file[-4:] != 'root':
        print("Warning! All had ttbar file not a root file")

if tnh_file:
    do_tnh = True
    if tnh_file[0] == '/':
        input_dir = ''
    tnh_file = input_dir+tnh_file
    if tnh_file[-4:] != 'root':
        print("Warning! Non all had ttbar file not a root file")

str_to_add = []
str_target = []
file_to_add = []
in_file = []

n_ftypes = do_data+do_tth+do_tnh

data_dir = os.path.dirname(data_file)
if data_dir:
    if data_dir[-1] != '/':
        data_dir+='/'

if BDT:
    for job in range(n_jobs+(n_jobs==0)):
        n_job_label=""
        if n_jobs != 0:
            n_job_label= "_%d" % job

        if doBDTttbar:
            str_to_add+=[["BDT_d24_weight"+reg_label] for _ in range(n_ftypes)]
            str_target+=[["BDT_d24_weight"+reg_label] for _ in range(n_ftypes)]

        else:
            str_to_add+=[["BDT_d24_weight"+reg_label]]
            str_target+=[["BDT_d24_weight"+reg_label]]

        if do_data:
            if out_combine:
                print("Warning: inclusive packaging of weights doesn't make sense for data! Try running an individual year")
            else:
                file_to_add.append([(weight_dir+"dat_BDT_d24_"+yr+"_rel%d"+reg_label+label+n_job_label+".root") % rel for yr in yr_short])
                in_file.append(data_file)
        if do_tth and doBDTttbar: 
            file_to_add.append([(weight_dir+"tth_BDT_d24_"+yr+"_rel%d"+reg_label+label+n_job_label+".root") % rel for yr in yr_short])
            in_file.append(tth_file)
        if do_tnh and doBDTttbar: 
            file_to_add.append([(weight_dir+"ttnh_BDT_d24_"+yr+"_rel%d"+reg_label+label+n_job_label+".root")% rel for yr in yr_short])
            in_file.append(tnh_file)

if spline:
    info_outfile=TFile((data_dir+"spline_info_all_rel%d"+reg_label+".root") % (rel), "update")
    for it in range(n_it):
        str_to_add+=[["rw_weight"+reg_label, "njet_weight"+reg_label] for _ in range(n_ftypes)]
        str_target+=[[("rw_weight_it_%d"+reg_label) % it, ("njet_weight_it_%d"+reg_label) % it] for _ in range(n_ftypes)]

        if do_data:
            if out_combine:
                print("Warning: inclusive packaging of weights doesn't make sense for data! Try running an individual year")
            else:
                file_to_add.append([(weight_dir+"dat_it_%d_"+yr+"_rel%d"+reg_label+label+".root") % (it,rel) for yr in yr_short])
                in_file.append(data_file)
        if do_tth: 
            file_to_add.append([(weight_dir+"tth_it_%d_"+yr+"_rel%d"+reg_label+label+".root") % (it,rel) for yr in yr_short])
            in_file.append(tth_file)
        if do_tnh: 
            file_to_add.append([(weight_dir+"ttnh_it_%d_"+yr+"_rel%d"+reg_label+label+".root") % (it,rel) for yr in yr_short])
            in_file.append(tnh_file)

        for yr in yr_short:
            info_in_file=TFile((weight_dir+"info_it_%d_"+yr+"_rel%d"+reg_label+".root") % (it,rel), "read")
            for key in info_in_file.GetListOfKeys():
                info_in = info_in_file.Get(key.GetName())
                info_out = info_in.Clone()
                info_outfile.cd()
                info_out.Write(key.GetName()+"_"+yr, TObject.kOverwrite)
            del info_in_file

for i in range(len(file_to_add)):

    f = TFile(in_file[i],"update")
   
    treeNames = []
    objNames = []
    for key in f.GetListOfKeys():
        if key.GetName():
            objNames.append(key.GetName())
            if key.GetClassName() == "TTree":
                treeNames.append(key.GetName())

    if doBootstrap:
        new_str_to_add = []
        f_for_bnames = TFile(file_to_add[i][0], "read")
        for key in f_for_bnames.GetListOfKeys():
            if key.GetName():
                if key.GetClassName() == "TTree":
                    tree = f_for_bnames.Get(key.GetName())
                    for branch in tree.GetListOfBranches():
                        bname = branch.GetName()
                        if bname.find("BDT_d24_weight") != -1:
                            new_str_to_add.append(bname)
                    break

        str_to_add[i] = new_str_to_add.copy()
        str_target[i] = new_str_to_add.copy()

    out_bnames = []
    bnames_to_check = []
    if out_combine:
        out_bnames = [str_target[i] for yr in yr_short]
        bnames_to_check = out_bnames.copy()
    else:
        out_bnames = [[t+'_'+yr for t in str_target[i]] for yr in yr_short]
        bnames_to_check = [t+'_'+yr for t in str_target[i] for yr in yr_short]


    for treeCount in range(len(treeNames)):
        treeName=treeNames[treeCount]
        T = f.Get(treeName)
        print(in_file[i])
        branch_list = T.GetListOfBranches()
        duplicate = False
        dup_names = []
        for obj in branch_list:
            for name in bnames_to_check:
                if obj.GetName() == name:
                    duplicate = True
                    dup_names.append(name)
        if duplicate:
           print("Branches", dup_names, "already found in tree "+treeName+". Skipping to avoid issues")
           continue

        print("Loading", file_to_add[i])

        weights = [root2array(fname, treename=treeName, 
                             branches=str_to_add[i]+['event_number', 'run_number']) for fname in file_to_add[i]]

        for b in range(len(weights)):
            print(out_bnames[b]+['event_number', 'run_number'])
            weights[b].dtype.names = out_bnames[b]+['event_number','run_number']

        if out_combine:
            weights = [np.concatenate(weights)]

        print("Tree with", np.array(weights).shape)

        e_run_nums = root2array(in_file[i], treename=treeName,
                            branches=['event_number','run_number'])
        e_nums = list(zip(e_run_nums['event_number'], e_run_nums['run_number']))        

        merge = [np.array(np.ones(len(e_nums)),
                         dtype=weights[b][out_bnames[b]].dtype) for b in range(len(weights))]

        idxs = np.arange(len(e_nums))
        idx_dict = {}
        for j in range(len(e_nums)):
            idx_dict[e_nums[j]] = idxs[j]

        for b in range(len(weights)):
            weights_to_merge =  weights[b][out_bnames[b]]
            for l in range(len(weights[b])):
                event_num = (weights[b]['event_number'][l], weights[b]['run_number'][l])
                idx = idx_dict[event_num]
                merge[b][idx] = weights_to_merge[l]

            array2tree(merge[b], tree=T)

        T.Print()
        if not append and output_str != '':
            if in_file[i].find((output_str+'.root')) != -1:
                f.cd()
                T.Write(treeName, TObject.kOverwrite)
                if treeCount == len(treeNames)-1:
                    for fidx in range(len(file_to_add[i])):
                        fname = file_to_add[i][fidx]
                        f_add = TFile(fname, "read")
                        for key in f_add.GetListOfKeys():
                            if key.GetName() not in objNames and key.GetName():
                                extra = f_add.Get(key.GetName())
                                extra_out = extra.Clone()
                                if key.GetClassName().find('TParameter') != -1:
                                    outname = extra_out.GetName()+"_"+yr_short[fidx]
                                else:
                                    outname = extra_out.GetName()
                                f.cd()
                                extra_out.Write(outname, TObject.kOverwrite)
            else:
                out_name = in_file[i][:-5]+'_'+output_str+'.root'
                if treeCount==0:
                    mode = "recreate"
                else:
                    mode = "update"
                f_out = TFile(out_name, mode)
                print("About to clone tree", treeName, "- this might take a sec")
                T_out = T.CloneTree()
                T_out.Write(treeName, TObject.kOverwrite)

                if treeCount == len(treeNames)-1: 
                    f.cd()
                    for key in f.GetListOfKeys():
                        if key.GetClassName().find('TTree') == -1:
                            extra_obj = f.Get(key.GetName())
                            f_out.cd()
                            extra_obj_out = extra_obj.Clone()
                            extra_obj_out.Write(key.GetName(), TObject.kOverwrite)
                            del extra_obj_out
                        f.cd()


                    for fidx in range(len(file_to_add[i])):
                        fname = file_to_add[i][fidx]
                        f_add = TFile(fname, "read")
                        for key in f_add.GetListOfKeys():
                            if key.GetClassName().find('TParameter') != -1:
                                par = f_add.Get(key.GetName())
                                par_out = par.Clone()
                                f_out.cd()
                                par_out.Write(par_out.GetName()+"_"+yr_short[fidx], TObject.kOverwrite)

                    new_in = []
                    for name in in_file:
                        if name == in_file[i]:
                            name = out_name
                        new_in.append(name)

                    in_file = new_in

                del f_out
        else:
            T.Write(treeName, TObject.kOverwrite)
            if treeCount == len(treeNames)-1:
                for fidx in range(len(file_to_add[i])):
                    fname = file_to_add[i][fidx]
                    f_add = TFile(fname, "read")
                    for key in f_add.GetListOfKeys():
                        if key.GetName() not in objNames and key.GetName():
                            extra = f_add.Get(key.GetName())
                            extra_out = extra.Clone()
                            if key.GetClassName().find('TParameter') != -1:
                                outname = extra_out.GetName()+"_"+yr_short[fidx]
                            else:
                                outname = extra_out.GetName()
                            f.cd()
                            extra_out.Write(outname, TObject.kOverwrite)

    del f
