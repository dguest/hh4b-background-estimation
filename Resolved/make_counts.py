#!/usr/bin/env python3

import numpy as np
import scipy.optimize as opt
from scipy.interpolate import interp1d
from scipy import ndimage

from rw_utils import *

from optparse import OptionParser

parser = OptionParser()
parser.add_option("-i", "--input_dir", dest="input_dir", default="",
                  help="Input directory")
parser.add_option("-o", "--output_dir", dest="output_dir", default="",
                  help="Output directory")
parser.add_option("-d", "--data", dest="data_file", default="",
                  help="Input data filename")
parser.add_option("-t", "--tth", dest="tth_file", default="",
                  help="Input all had ttbar filename")
parser.add_option("-n", "--tnh", dest="tnh_file", default="",
                  help="Input non all-had ttbar filename")
parser.add_option("-y", "--year", dest="year", default="2015",
                  help="Year")
parser.add_option("-r", "--release", dest="rel", default="20",
                  help="Release 20 or 21")
parser.add_option("-b", "--BDT",
                  action="store_true", dest="BDT", default=False,
                  help="Do plots with BDT")
parser.add_option("-s", "--spline",
                  dest="spline", default=-1,
                  help="Turn spline plot, give number of the iteration. If default (=-1) don't plot.")

(options, args) = parser.parse_args()

import root_numpy

BDT = options.BDT
it = int(options.spline)
spline = (it != -1)
data_file = options.data_file
tth_file = options.tth_file
tnh_file = options.tnh_file
year_in = options.year
input_dir = options.input_dir
output_dir = options.output_dir
rel = int(options.rel[:2])

if rel != 20 and rel !=21:
    rel = 20
    print("Invalid option, defaulting to rel 20")

year =''
yr_short=''
if len(year_in) == 4:
    year=year_in
    yr_short=year_in[2:]
elif len(year_in) == 2:
    year='20'+year_in
    yr_short=year_in
else:
    print("Invalid year format, defaulting to 2015")
    year='2015'
    yr_short='15'


if input_dir:
    if input_dir[-1] != '/': input_dir+='/'

if output_dir:
    if output_dir[-1] != '/': output_dir+='/'

if data_file:
    if data_file[0] == '/':
        input_dir = ''
    data_file = input_dir+data_file
    if data_file[-4:] != 'root':
        print("Warning! Data file not a root file")
    if data_file.find(yr_short) == -1:
        print("Warning! Input data file name may not match year.")
else:
    if BDT:
        print("Warning! Need to specify input data name for BDT. Defaulting to spline")
        BDT = False
        spline= True
    data_file = (input_dir+'dat_it_%d_'+yr_short+'_rel%d.root') % (it,rel)

if tth_file:
    if tth_file[0] == '/':
        input_dir = ''
    tth_file = input_dir+tth_file
    if tth_file[-4:] != 'root':
        print("Warning! All had ttbar file not a root file")
else:
    tth_file = (input_dir+'tth_it_%d_'+yr_short+'_rel%d.root') % (it,rel)

if tnh_file:
    if tnh_file[0] == '/':
        input_dir = ''
    tnh_file = input_dir+tnh_file
    if tnh_file[-4:] != 'root':
        print("Warning! Non all had ttbar file not a root file")
else:
    tnh_file = (input_dir+'ttnh_it_%d_'+yr_short+'_rel%d.root') % (it,rel)


#Set up appropriate columns for data, mc, and rw. Data/MC slightly different due to some weights
columns = ['njets', 'ntag', 'm_h1', 'm_h2', 'run_number', 'X_wt']

columns_mc = ['njets', 'ntag', 'm_h1', 'm_h2', 'mc_sf', 'run_number', 'X_wt']

if rel == 20:
  #  columns = ['n_jets', 'n_tag', 'passTrig_'+yr_short, 'event.m_h1', 'event.m_h2', 'Xwt']
#
#    columns_mc =['n_jets', 'n_tag', 'passTrig_'+yr_short, 'mc_weight', 'event.m_h1', 'event.m_h2', 'bFix70_weight', 'Xwt']
    columns = ['n_jets', 'n_tag', 'passTrig_'+yr_short, 'm_h1', 'm_h2', 'Xwt']

    columns_mc =['n_jets', 'n_tag', 'passTrig_'+yr_short, 'mc_weight', 'm_h1', 'm_h2', 'bFix70_weight', 'Xwt']

if BDT: 
    columns += ['BDT_d24_weight_'+yr_short]
if spline:
    if data_file.find('_it_') != -1:
        columns += ['njet_weight', 'rw_weight']
    else:
        columns += [('njet_weight_it_%d_'+yr_short) % it, ('rw_weight_it_%d_'+yr_short) % it]

    if tth_file.find('_it_') != -1 and ttnh_file.find('_it_') != -1:
        columns_mc += ['njet_weight', 'rw_weight']
    else:
        columns_mc += [('njet_weight_it_%d_'+yr_short) % it, ('rw_weight_it_%d_'+yr_short) % it]

post_adj_dat = False
post_adj_mc = False
#Adjust rel 20 naming to match (see rw_utils for mapping). Note that adjusted naming is what is saved in reweight.py output
if rel == 20:
    rel20to21 = rel20to21_dict()
    if 'njet_weight' not in columns:
        post_adj_dat = True
    else:
        columns = [rel20to21[key] for key in columns]
 
    if 'njet_weight' not in columns_mc:
        post_adj_mc = True
    else:
        columns_mc = [rel20to21[key] for key in columns_mc]


it_data = root_numpy.root2array(data_file,
                             treename='fullmassplane', branches=columns)

if rel == 20 and post_adj_dat:
    rel20to21 = rel20to21_dict()
    columns = [rel20to21[key] for key in it_data.dtype.names]

    it_data.dtype.names = tuple(columns)

#Adjust verbose branchnames (with it, year) to be base ones
columns = [vanillize_name(key) for key in it_data.dtype.names]
it_data.dtype.names = tuple(columns)

if spline:
    it_tth = root_numpy.root2array(tth_file,
                                 treename='fullmassplane', branches=columns_mc)
    it_ttnh = root_numpy.root2array(tnh_file,
                                 treename='fullmassplane', branches=columns_mc)
    if rel == 20 and post_adj_mc:
        rel20to21 = rel20to21_dict()
        columns_mc = [rel20to21[key] for key in it_tth.dtype.names]
        it_tth.dtype.names = tuple(columns_mc)
        it_ttnh.dtype.names = tuple(columns_mc) 

    #Adjust verbose branchnames (with it, year) to be base ones
    columns_mc = [vanillize_name(key) for key in it_tth.dtype.names]
    it_tth.dtype.names = tuple(columns_mc)
    it_ttnh.dtype.names = tuple(columns_mc)


    sf = {}
    sf_err = {}
    file = open((input_dir+'info_it_%d_' + yr_short + '_rel%d.txt') % (it,rel), "r")
    lines=file.readlines()
    sf['tnh4'] = float(lines[0][8:14])
    sf_err['tnh4'] = float(lines[0][19:25])

    sf['th4'] = float(lines[1][8:14])
    sf_err['th4'] = float(lines[1][19:25])

    sf['qcd'] =float(lines[2][8:14])
    sf_err['qcd'] = float(lines[2][19:25])

    sf['tnh2'] =float(lines[3][8:14])
    sf_err['tnh2'] = float(lines[3][19:25])

    f = float(lines[4][8:14])
    if rel == 20:
        fth24 = float(lines[5][8:14])
    file.close()


if rel == 21:

    #Grab appropriate events for each year's triggers. Run numbers taken from resolved-recon's trigger list
    run_bounds = { '2015' : [0, 296939], '2016' : [296939, 324320],
                   '2017' : [324320, 348197], '2018' : [348197, 364486] }
    year_run_range = run_bounds[year]

    it_data = it_data[(it_data['run_number'] > year_run_range[0]) & (it_data['run_number'] < year_run_range[1])]
    if spline:
        it_tth = it_tth[(it_tth['run_number'] > year_run_range[0]) & (it_tth['run_number'] < year_run_range[1])]
        it_ttnh = it_ttnh[(it_ttnh['run_number'] > year_run_range[0]) & (it_ttnh['run_number'] < year_run_range[1])]

arrs = {}
hists = {}
weights = {}
reg_arrs = {'SB':{}, 'CR': {}, 'SR' : {}}
reg_weights = {'SB':{}, 'CR': {}, 'SR' : {}}

data = it_data.copy()
if rel == 20:
    data = data[data['passTrig_'+yr_short]==True]
    data = data[data['X_wt'] > 1.5]
arrs['dat2']= data[data['ntag'] == 2]
arrs['dat4']= data[data['ntag'] >= 4]

keys = ['dat2', 'dat4']

if spline:
    tth = it_tth.copy()
    if rel == 20:
        tth = tth[tth['passTrig_'+yr_short]==True]
        tth = tth[tth['X_wt'] > 1.5]
    arrs['th2']= tth[tth['ntag'] == 2]
    if rel == 20:
        arrs['th4']= arrs['th2'].copy()
    else:
        arrs['th4']= tth[tth['ntag'] >= 4]

    ttnh = it_ttnh.copy()
    if rel == 20:
        ttnh = ttnh[ttnh['passTrig_'+yr_short]==True]
        ttnh = ttnh[ttnh['X_wt'] > 1.5]
    arrs['tnh2']= ttnh[ttnh['ntag'] == 2]
    arrs['tnh4']= ttnh[ttnh['ntag'] >= 4]

    keys = ['dat2', 'dat4', 'th2', 'th4', 'tnh2', 'tnh4']

    if rel == 20:
       arrs['th4']['njet_weight'] = nJetWeight(arrs['th4'], fth24)

    weights = {}
    for key in keys:
        weights[key] = makeWeights(arrs, key, int(it), int(year), rel)

for key in keys:
    reg = whichRegions(arrs[key])
    isSB = (reg['SB']) & (~reg['CR']) &(~reg['SR'])
    isCR = (reg['CR']) & (~reg['SR'])
    isSR = (reg['SR'])
 
    reg_arrs['SB'][key] = arrs[key][isSB]
    reg_arrs['CR'][key] = arrs[key][isCR]
    reg_arrs['SR'][key] = arrs[key][isSR]

    reg_weights['SB'][key] = weights[key][isSB]
    reg_weights['CR'][key] = weights[key][isCR]
    reg_weights['SR'][key] = weights[key][isSR]


methods = []
if BDT:
    methods.append('BDT')
if spline:
    methods.append('spline')

labels = {'tnh4': 'nonallhad ttbar', 'th4': 'allhad ttbar', 'qcd' : 'multijet', 'dat4': 'data', 'BDT' : 'BDT Background', 'total': 'total'}

for method in methods:
    counts = {}
    errors = {}
    for region in ['SB', 'CR', 'SR']:
        counts[region] = {}
        errors[region] = {}
     
        counts[region]['dat4'] = len(reg_arrs[region]['dat4'])*1.
        errors[region]['dat4'] = np.sqrt(counts[region]['dat4'])
        if method == 'spline':
            counts[region]['th4'] = np.sum(reg_weights[region]['th4'])*sf['th4']
            counts[region]['tnh4'] = np.sum(reg_weights[region]['tnh4'])*sf['tnh4']
            counts[region]['qcd'] = (np.sum(reg_weights[region]['dat2'])-np.sum(reg_weights[region]['th2'])-np.sum(reg_weights[region]['tnh2'])*sf['tnh2'])*sf['qcd']

            counts[region]['total'] = counts[region]['th4'] + counts[region]['tnh4'] + counts[region]['qcd']

            counts[region]['tnh2'] = np.sum(reg_weights[region]['tnh2'])*sf['tnh2'] 
     
            for key in ['th4', 'tnh4', 'tnh2']: 
                count_err_term = np.sqrt(np.sum(reg_weights[region][key]**2))/np.sum(reg_weights[region][key])
                sf_err_term = sf_err[key]/sf[key]
                errors[region][key] = counts[region][key]*np.sqrt(count_err_term**2+sf_err_term**2)

            errors[region]['th2'] = np.sqrt(np.sum(reg_weights[region]['th2']**2))
            errors[region]['dat2'] = np.sqrt(np.sum(reg_weights[region]['dat2']**2))

            count_err_term = np.sqrt(errors[region]['dat2']**2 + errors[region]['th2']**2 + errors[region]['tnh2']**2)
            count_err_term *= 1./(np.sum(reg_weights[region]['dat2'])-np.sum(reg_weights[region]['th2'])-np.sum(reg_weights[region]['tnh2'])*sf['tnh2'])
            sf_err_term = sf_err['qcd']/sf['qcd']
            errors[region]['qcd'] = counts[region]['qcd']*np.sqrt(count_err_term**2+sf_err_term**2)

            errors[region]['total'] = np.sqrt(errors[region]['qcd']**2 + errors[region]['th4']**2 + errors[region]['tnh4']**2)
            
            dnames = ['tnh4', 'th4', 'qcd', 'total', 'dat4']


        if method == 'BDT':
            scale = len(reg_arrs['SB']['dat4'])*1./np.sum(reg_arrs['SB']['dat2']['BDT_d24_weight_'+yr_short])
            counts[region]['BDT'] = np.sum(reg_arrs[region]['dat2']['BDT_d24_weight_'+yr_short])

            scale_err = scale*np.sqrt((np.sqrt((len(reg_arrs['SB']['dat4'])*1.))/(len(reg_arrs['SB']['dat4'])*1.))**2 +
                                      (np.sqrt(np.sum(reg_arrs['SB']['dat2']['BDT_d24_weight_'+yr_short]**2))/
                                       np.sum(reg_arrs['SB']['dat2']['BDT_d24_weight_'+yr_short]))**2)

            count_err_term = np.sqrt(np.sum(reg_arrs[region]['dat2']['BDT_d24_weight_'+yr_short]**2))/np.sum(reg_arrs[region]['dat2']['BDT_d24_weight_'+yr_short])

            sf_err_term = scale_err/scale
            errors[region]['BDT'] = counts[region]['BDT']*np.sqrt(count_err_term**2+sf_err_term**2)

            dnames = ['BDT', 'dat4']

    print('{:>25}'.format(''), '{:>25}'.format('Sideband'), '{:>25}'.format('Control'), '{:>25}'.format('Signal'))
    for dname in dnames:
         print('{:>25}'.format(labels[dname]), '{:^25}'.format('%.2f +/- %.2f' % (counts['SB'][dname], errors['SB'][dname])), 
                                               '{:^25}'.format('%.2f +/- %.2f' % (counts['CR'][dname], errors['CR'][dname])),
                                               '{:^25}'.format('%.2f +/- %.2f' % (counts['SR'][dname], errors['SR'][dname])))





