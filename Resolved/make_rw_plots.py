#!/usr/bin/env python3

import numpy as np
import scipy.optimize as opt
from scipy.interpolate import interp1d
from scipy import ndimage

from rw_utils import *

from optparse import OptionParser

parser = OptionParser()
parser.add_option("-i", "--input_dir", dest="input_dir", default="",
                  help="Input directory")
parser.add_option("-o", "--output_dir", dest="output_dir", default="",
                  help="Output directory")
parser.add_option("-d", "--data", dest="data_file", default="",
                  help="Input data filename")
parser.add_option("-t", "--tth", dest="tth_file", default="",
                  help="Input all had ttbar filename")
parser.add_option("-n", "--tnh", dest="tnh_file", default="",
                  help="Input non all-had ttbar filename")
parser.add_option("-y", "--year", dest="year", default="2015",
                  help="Year")
parser.add_option("-r", "--release", dest="rel", default="20",
                  help="Release 20 or 21")
parser.add_option("-b", "--BDT",
                  action="store_true", dest="BDT", default=False,
                  help="Do plots with BDT")
parser.add_option("-s", "--spline",
                  dest="spline", default=-1,
                  help="Turn spline plot, give number of the iteration. If default (=-1) don't plot.")
parser.add_option("-l", "--label", dest="label", default="",
                  help="Label for output")
parser.add_option("--tree", dest="tree", default="",
                  help="Tree on which to make plots")
parser.add_option("--do-CRw", action="store_true", dest="doCRw", default=False,
                  help="Use CR derived weights")
parser.add_option("--HCrescale",
                  action="store_true", dest="HCrescale", default=False,
                  help="Rescale HC 4 vecs to have m=125 GeV")

(options, args) = parser.parse_args()

import root_numpy
import ROOT
from ROOT import TFile, TLorentzVector

BDT = options.BDT
it = int(options.spline)
spline = (it != -1)
data_file = options.data_file
tth_file = options.tth_file
tnh_file = options.tnh_file
year_in = options.year
input_dir = options.input_dir
output_dir = options.output_dir
rel = int(options.rel[:2])
label= options.label
tree = options.tree
doCRw = options.doCRw
HCrescale=options.HCrescale

if HCrescale:
    label+= "_HCrescale"

reg_label=''
if doCRw:
    reg_label='_CRderiv'

if label:
   if label[0] != '_':
       label = '_'+label

if rel != 20 and rel !=21:
    rel = 20
    print("Invalid option, defaulting to rel 20")

year =''
yr_short=''
if len(year_in) == 4:
    year=year_in
    yr_short=year_in[2:]
elif len(year_in) == 2:
    year='20'+year_in
    yr_short=year_in
else:
    print("Invalid year format, defaulting to 2015")
    year='2015'
    yr_short='15'


if input_dir:
    if input_dir[-1] != '/': input_dir+='/'

if output_dir:
    if output_dir[-1] != '/': output_dir+='/'

if data_file:
    if data_file[0] == '/':
        input_dir = ''
    data_file = input_dir+data_file
    if data_file[-4:] != 'root':
        print("Warning! Data file not a root file")
    if data_file.find(yr_short) == -1:
        print("Warning! Input data file name may not match year.")
else:
    if BDT:
        print("Warning! Need to specify input data name for BDT. Defaulting to spline")
        BDT = False
        spline= True
    data_file = (input_dir+'dat_it_%d_'+yr_short+'_rel%d.root') % (it,rel)

if tth_file:
    if tth_file[0] == '/':
        input_dir = ''
    tth_file = input_dir+tth_file
    if tth_file[-4:] != 'root':
        print("Warning! All had ttbar file not a root file")
else:
    tth_file = (input_dir+'tth_it_%d_'+yr_short+'_rel%d.root') % (it,rel)

if tnh_file:
    if tnh_file[0] != '/':
        tnh_file = input_dir+tnh_file
    if tnh_file[-4:] != 'root':
        print("Warning! Non all had ttbar file not a root file")
else:
    tnh_file = (input_dir+'ttnh_it_%d_'+yr_short+'_rel%d.root') % (it,rel)


#Set up appropriate columns for data, mc, and rw. Data/MC slightly different due to some weights
columns = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2',
           'njets', 'ntag', 'm_h1', 'm_h2', 'run_number', 'm_hh', 'X_wt']

columns_mc = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2',
              'njets', 'ntag', 'm_h1', 'm_h2', 'mc_sf', 'run_number', 'm_hh', 'X_wt']

rw_columns = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2']

if rel == 20:
    columns = ['pT_4', 'pT_2', 'ave_eta', 'dRjj_close', 'dRjj_notclose',
               'n_jets', 'n_tag', 'passTrig_'+yr_short, 'nmuon_isIsolated_custom','mc_weight',
               'm_h1', 'm_h2', 'Xwt', 'm_4j']

    columns_mc = ['pT_4', 'pT_2', 'ave_eta', 'dRjj_close', 'dRjj_notclose',
                  'n_jets', 'n_tag', 'passTrig_'+yr_short, 'nmuon_isIsolated_custom', 'mc_weight',
                  'm_h1', 'm_h2', 'Xwt', 'bFix70_weight', 'm_4j']


if BDT: 
    columns += ['BDT_d24_weight'+reg_label+'_'+yr_short]
if spline:
    if data_file.find('_it_') != -1:
        columns += ['njet_weight'+reg_label, 'rw_weight'+reg_label]
    else:
        columns += [('njet_weight_it_%d'+reg_label+'_'+yr_short) % it, ('rw_weight_it_%d'+reg_label+'_'+yr_short) % it]

    if tth_file.find('_it_') != -1 and ttnh_file.find('_it_') != -1:
        columns_mc += ['njet_weight'+reg_label, 'rw_weight'+reg_label]
    else:
        columns_mc += [('njet_weight_it_%d'+reg_label+'_'+yr_short) % it, ('rw_weight_it_%d'+reg_label+'_'+yr_short) % it]



vec4_vars = ['pT_h1', 'eta_h1', 'phi_h1', 'm_h1', 'pT_h2', 'eta_h2', 'phi_h2', 'm_h2']

if HCrescale:
    for var in vec4_vars:
        if var not in columns:
            columns += [var]
        if var not in columns_mc:
            columns_mc += [var]

post_adj_dat = False
post_adj_mc = False
#Adjust rel 20 naming to match (see rw_utils for mapping). Note that adjusted naming is what is saved in reweight.py output
if rel == 20:
    rel20to21 = rel20to21_dict()
    if 'njet_weight' not in columns:
        post_adj_dat = True
    else:
        columns = [rel20to21[key] for key in columns]
 
    if 'njet_weight' not in columns_mc:
        post_adj_mc = True
    else:
        columns_mc = [rel20to21[key] for key in columns_mc]


#Set up x labels and plotting variables
x_label_dict = {'pT_4': 'HC Jet 4 p_{T} [GeV]', 'pT_2': 'HC Jet 2 p_{T} [GeV]', 'eta_i': '<|HC #eta|>', 
                'dRjj_1': '#Delta R_{jj} Close', 'dRjj_2': '#Delta R_{jj} Not Close', 'njets' : '# of additional jets', 'm_hh' : 'm_{HH} [GeV]'}


if HCrescale:
    x_label_dict['m_hh'] = 'm_{HH} (corrected) [GeV]'

columns_to_plot = rw_columns + ['njets','m_hh']

it_data = root_numpy.root2array(data_file,
                             treename=tree, branches=columns)

if rel == 20 and post_adj_dat:
    rel20to21 = rel20to21_dict()
    columns = [rel20to21[key] for key in it_data.dtype.names]

    it_data.dtype.names = tuple(columns)

#Adjust verbose branchnames (with it, year) to be base ones
columns = [vanillize_name(key) for key in it_data.dtype.names]
it_data.dtype.names = tuple(columns)

if spline:
    it_tth = root_numpy.root2array(tth_file,
                                 treename=tree, branches=columns_mc)
    it_ttnh = root_numpy.root2array(tnh_file,
                                 treename=tree, branches=columns_mc)
    if rel == 20 and post_adj_mc:
        rel20to21 = rel20to21_dict()
        columns_mc = [rel20to21[key] for key in it_tth.dtype.names]
        it_tth.dtype.names = tuple(columns_mc)
        it_ttnh.dtype.names = tuple(columns_mc) 

    #Adjust verbose branchnames (with it, year) to be base ones
    columns_mc = [vanillize_name(key) for key in it_tth.dtype.names]
    it_tth.dtype.names = tuple(columns_mc)
    it_ttnh.dtype.names = tuple(columns_mc)


    sf_file = TFile((input_dir+'spline_info_all_rel%d'+reg_label+'.root') % (rel), "read")
    sf_hist = sf_file.Get(("sf_hist_it_%d_"+yr_short) % it)
    sftnh = sf_hist.GetBinContent(1)
    sfth = sf_hist.GetBinContent(2)
    sfqcd = sf_hist.GetBinContent(3)
    sftnh2 = sf_hist.GetBinContent(4)
    f = (sf_file.Get(("f_it_%d_"+yr_short) % it)).GetVal()
    if rel == 20:
        fth24 = (sf_file.Get(("f_th24_it_%d_"+yr_short) % it)).GetVal()
    sf_file.Close()

if rel == 21:

    #Grab appropriate events for each year's triggers. Run numbers taken from resolved-recon's trigger list
    run_bounds = { '2015' : [0, 296939], '2016' : [296939, 324320],
                   '2017' : [324320, 348197], '2018' : [348197, 364486] }
    year_run_range = run_bounds[year]

    it_data = it_data[(it_data['run_number'] > year_run_range[0]) & (it_data['run_number'] < year_run_range[1])]
    if spline:
        it_tth = it_tth[(it_tth['run_number'] > year_run_range[0]) & (it_tth['run_number'] < year_run_range[1])]
        it_ttnh = it_ttnh[(it_ttnh['run_number'] > year_run_range[0]) & (it_ttnh['run_number'] < year_run_range[1])]

arrs = {}
hists = {}
weights = {}
fit_counts = {}

data = it_data[it_data['X_wt'] > 1.5]
if rel == 20:
    data = data[data['passTrig_'+yr_short]==True]
arrs['dat2']= data[data['ntag'] == 2]
arrs['dat4']= data[data['ntag'] >= 4]

if spline:
    tth = it_tth[it_tth['X_wt'] > 1.5]
    if rel == 20:
        tth = tth[tth['passTrig_'+yr_short]==True]
    arrs['th2']= tth[tth['ntag'] == 2]
    if rel == 20:
        arrs['th4']= arrs['th2'].copy()
    else:
        arrs['th4']= tth[tth['ntag'] >= 4]

    ttnh = it_ttnh[it_ttnh['X_wt'] > 1.5]
    if rel == 20:
        ttnh = ttnh[ttnh['passTrig_'+yr_short]==True]
    arrs['tnh2']= ttnh[ttnh['ntag'] == 2]
    arrs['tnh4']= ttnh[ttnh['ntag'] >= 4]

    keys = ['dat2', 'dat4', 'th2', 'th4', 'tnh2', 'tnh4']

    if rel == 20:
       arrs['th4']['njet_weight'+reg_label] = nJetWeight(arrs['th4'], fth24)

    weights = {}
    for key in keys:
        weights[key] = makeWeights(arrs, key, int(it), int(year), rel, doCRw=doCRw)

all_keys=[]
if spline:
    keys_to_add = ['dat2', 'dat4', 'th2', 'th4', 'tnh2', 'tnh4']
    for key in keys_to_add:
        if key not in all_keys:
            all_keys.append(key)
if BDT:
    keys_to_add = ['dat2', 'dat4']
    for key in keys_to_add:
        if key not in all_keys:
            all_keys.append(key)

if HCrescale:
    print("About to rescale m_hh")
    h1 = TLorentzVector()
    h2 = TLorentzVector()
    for key in all_keys:
        print(key)
        for event in range(len(arrs[key])):
            if event % 10000 == 0: print(event, "out of", len(arrs[key]))
            h1.SetPtEtaPhiM(arrs[key]['pT_h1'][event], arrs[key]['eta_h1'][event], arrs[key]['phi_h1'][event], arrs[key]['m_h1'][event])
            h2.SetPtEtaPhiM(arrs[key]['pT_h2'][event], arrs[key]['eta_h2'][event], arrs[key]['phi_h2'][event], arrs[key]['m_h2'][event])
            arrs[key]['m_hh'][event] = corr_mass(h1,h2)

from rootpy.plotting import *
ROOT.gROOT.SetBatch(ROOT.kTRUE)

#range_dict = {"m_hh" : [100, 1200], 'pT_4': [0, 200], 'pT_2': [0,550], 'eta_i':[0,4], 'dRjj_1': [0,2.4], 'dRjj_2' : [0,2.4],
 #             'njets':
              

methods = []
if BDT:
    methods.append('BDT')
    f_for_norm = TFile(data_file, "read")
    BDTnorm_par = f_for_norm.Get("BDT_norm"+reg_label+"_"+yr_short)
    BDTnorm = BDTnorm_par.GetVal()
    f_for_norm.Close()
if spline:
    methods.append('spline')

for method in methods:
    for column in columns_to_plot:
        offset = 0.
        if column == 'njets':
            offset=-4

        xlim = np.percentile(np.hstack([arrs['dat4'][column]+offset]), [0.01, 99.99])
        width = xlim[1]-xlim[0]
        xlim[0] -= width*0.1
        xlim[1] += width*0.1

        n_bins = 50
        if column == 'njets':
            n_bins = 8
            xlim = [-0.5,7.5]
        if column == 'm_hh':
            n_bins= 66
            xlim = [100,1400]
        if method == 'spline':
            hist_tth_4tag = Hist(n_bins, xlim[0], xlim[1], title='All hadronic t#bar{t}', legendstyle='F', drawstyle='hist')
            hist_tth_4tag.fill_array(arrs['th4'][column]+offset, weights=weights['th4']*sfth)
            hist_ttnh_4tag =  Hist(n_bins, xlim[0], xlim[1], title='Leptonic t#bar{t}', legendstyle='F', drawstyle='hist')
            hist_ttnh_4tag.fill_array(arrs['tnh4'][column]+offset, weights=weights['tnh4']*sftnh)
            hist_qcd = Hist(n_bins, xlim[0], xlim[1], title='QCD', legendstyle='F',drawstyle='hist')
            hist_qcd.fill_array(arrs['dat2'][column]+offset, weights=weights['dat2'])

            hist_ttnh_2tag = Hist(n_bins, xlim[0], xlim[1], title='QCD', legendstyle='F',drawstyle='hist')
            hist_ttnh_2tag.fill_array(arrs['tnh2'][column]+offset, weights=weights['tnh2']*sftnh2)

            hist_tth_2tag = Hist(n_bins, xlim[0], xlim[1], title='QCD', legendstyle='F',drawstyle='hist')
            hist_tth_2tag.fill_array(arrs['th2'][column]+offset, weights=weights['th2'])

            print("ttnh2:", hist_ttnh_2tag.Integral(), "tth2:", hist_tth_2tag.Integral())
            hist_qcd.Add(hist_ttnh_2tag,-1)
            hist_qcd.Add(hist_tth_2tag,-1)
            hist_qcd.Scale(sfqcd)
  
            hist_tth_4tag.SetFillColor(860-9)
            hist_ttnh_4tag.SetFillColor(860-4)

            hist_tth_4tag.SetFillStyle('solid')
            hist_ttnh_4tag.SetFillStyle('solid')
  
            hist_tth_4tag.SetLineColor(1)
            hist_ttnh_4tag.SetLineColor(1)


        hist_dat_4tag = Hist(n_bins, xlim[0], xlim[1], title='Data', legendstyle='PE')
        hist_dat_4tag.fill_array(arrs['dat4'][column]+offset)

        if method == 'BDT':
             hist_qcd = Hist(n_bins, xlim[0], xlim[1], title='QCD', legendstyle='F',drawstyle='hist')
             if label == '_norw':
                 hist_qcd.fill_array(arrs['dat2'][column]+offset)
             else:
                 hist_qcd.fill_array(arrs['dat2'][column]+offset, weights=arrs['dat2']['BDT_d24_weight'+reg_label])

             hist_qcd.Scale(BDTnorm)
            # hist_qcd.Scale(hist_dat_4tag.Integral()/hist_qcd.Integral())

        hist_qcd.SetFillColor(5)
        hist_qcd.SetFillStyle('solid')
        hist_qcd.SetLineColor(1)

        if method == 'spline':
            stack = HistStack(hists=[hist_ttnh_4tag,hist_tth_4tag,hist_qcd])
        if method == 'BDT':
             stack = HistStack(hists=[hist_qcd])

        stack.SetMaximum(hist_dat_4tag.GetMaximum()*1.4)
        hist_dat_4tag.SetMarkerStyle(20)
        hist_dat_4tag.SetMarkerSize(0.6)
        hist_dat_4tag.SetMarkerColor(1)
        hist_dat_4tag.SetLineColor(1)


        canvas = Canvas()
        canvas.cd()
        pad1 = Pad(0.0,0.33,1.0,1.0)
        pad2 = Pad(0.0,0.0,1.0,0.33)
        pad1.SetBottomMargin(0.015)
        pad1.SetTopMargin(0.05)
        pad2.SetTopMargin(0.05)
        pad2.SetBottomMargin(0.3)
        pad1.Draw()
        pad2.Draw()
        pad1.SetTicks(1, 1)

        pad1.cd()
        if method == 'spline':
            legend = Legend([hist_tth_4tag,hist_ttnh_4tag, hist_qcd,hist_dat_4tag])
        if method == 'BDT':
            if label == '_norw':
                hist_qcd.SetTitle("No reweighting")
            else:
                hist_qcd.SetTitle("BDT Background")
            legend = Legend([hist_qcd,hist_dat_4tag], textsize=0.09, leftmargin=0.27, entrysep=0.1, header =year +' '+tree+reg_label)

        stack.Draw()
        hist_dat_4tag.Draw("same pe")

        stack.GetXaxis().SetLabelSize(0)
        stack.GetYaxis().SetLabelSize(0.038)

        if method == 'spline':
            legend.SetHeader((year + ' Iteration %d, '+ tree+reg_label) % (it+1))

        legend.SetBorderSize(0)
        legend.SetFillStyle(0)
        legend.Draw()

        pad2.cd()
        ROOT.gStyle.SetOptStat(0)
        rat_dat_pred = hist_dat_4tag.Clone("rat_dat_pred")
        rat_dat_pred.Divide(stack.GetStack().Last())
        rat_dat_pred.GetYaxis().SetRangeUser(0, 2)
        rat_dat_pred.GetYaxis().SetNdivisions(503)
        rat_dat_pred.GetXaxis().SetLabelSize(0.1)
        rat_dat_pred.GetYaxis().SetLabelSize(0.1)
        rat_dat_pred.GetYaxis().SetLabelOffset(0.03)
        rat_dat_pred.GetYaxis().SetTitle("Data / Bkgd")
        rat_dat_pred.GetYaxis().CenterTitle();
        rat_dat_pred.GetXaxis().SetTitle(x_label_dict[column])
        rat_dat_pred.SetTitle('')
    

        rat_dat_pred.GetYaxis().SetTitleSize(0.12)
        #rat_dat_pred.GetXaxis().SetTitleSize(0.1)
        rat_dat_pred.GetXaxis().SetTitleSize(0.15)
        rat_dat_pred.GetYaxis().SetTitleOffset(0.35)
        #rat_dat_pred.GetXaxis().SetTitleOffset(1.1)
        rat_dat_pred.GetXaxis().SetTitleOffset(0.8)

        rat_dat_pred.SetMarkerStyle(20)
        rat_dat_pred.SetMarkerSize(0.6)
        rat_dat_pred.SetMarkerColor(1)
        rat_dat_pred.SetLineColor(1)
        rat_dat_pred.Draw();

        line = ROOT.TLine(rat_dat_pred.GetXaxis().GetXmin(), 1, rat_dat_pred.GetXaxis().GetXmax(), 1)
        line.SetLineColor(1)
        line.SetLineWidth(1)
        line.SetLineStyle(1)
        line.Draw()

        canvas.Draw()
  
        if method == 'spline':
            canvas.SaveAs((output_dir+column+label+'_'+tree+reg_label+'_spline_it_%d_'+yr_short+'_rel%d.pdf') % (it, rel))
        if method == 'BDT':
            canvas.SaveAs((output_dir+column+label+'_'+tree+reg_label+'_BDT_'+yr_short+'_rel%d.pdf') % (rel))
