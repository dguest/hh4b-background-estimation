#!/usr/bin/env python3

import numpy as np
from numpy.lib.recfunctions import append_fields
import scipy.optimize as opt
from scipy.interpolate import interp1d
from scipy import ndimage
import pickle

from pandas import DataFrame 

import sys
sys.path.insert(0, 'hep_ml')
sys.path.insert(0, '../hep_ml')
from hep_ml import reweight

from rw_utils import *

import yaml
from optparse import OptionParser

def commasep_callback(option, opt, value, parser):
    setattr(parser.values, option.dest, value.split(','))

parser = OptionParser()
parser.add_option("-i", "--input_dir", dest="input_dir", default="",
                  help="Input directory")
parser.add_option("-d", "--data", dest="data_file", default="",
                  help="Input data filename")
parser.add_option("-t", "--tth", dest="tth_file", default="",
                  help="Input all had ttbar filename")
parser.add_option("-n", "--tnh", dest="tnh_file", default="",
                  help="Input non all-had ttbar filename")
parser.add_option("-o", "--output_dir", dest="output_dir", default="",
                  help="Output directory")
parser.add_option("-y", "--year", dest="year", default="2015",
                  help="Year")

parser.add_option("-r", "--release", dest="rel", default="20",
                  help="Release 20 or 21")

parser.add_option("-f", "--full",
                  action="store_true", dest="full", default=False,
                  help="Turn on write variables - otherwise, just weights/event_number")

parser.add_option("--not-all-trees",
                  action="store_true", dest="not_all_trees", default=False,
                  help="Turn off calculating weight for all trees")

parser.add_option("-p", "--parameters", dest="params", default="",
                  type='string',
                  action='callback',
                  callback=commasep_callback,
                  help = "Read in parameters for BDT optimization. Feed in columns as a comma sep list or use a YAML file (recommended)")

parser.add_option("-s", "--save",
                  action="store_true", dest="save", default=False,
                  help="Turn on save BDT (pickle)")

parser.add_option("--reweight-tree", dest="reweight_tree", default="",
                  help="Tree on which to run reweighting.")

parser.add_option("--bootstrap", dest="n_resamples", default="-1",
                  help="Turn on bootstrapping. Put in number of resamples.")

parser.add_option("--job", dest="job", default ="",
                  help="Condor job number (to be used for bootstrap only)")

parser.add_option("--n_jobs", dest="n_jobs", default="",
                  help="Total number of jobs (to be used for bootstrap only)")

parser.add_option("-l", "--label", dest="label", default="",
                  help="Label for output file (e.g., pflow)")

(options, args) = parser.parse_args()

import root_numpy
from ROOT import TParameter, TFile

year_in = options.year
input_dir = options.input_dir
output_dir = options.output_dir
data_file = options.data_file
tth_file = options.tth_file
tnh_file = options.tnh_file
rel = int(options.rel[:2])
full = options.full
label = options.label
n_resamples=int(options.n_resamples)
job=options.job
n_jobs=options.n_jobs

doBootstrap = False
if n_resamples != -1:
    print("Running bootstrapping with %d total resamples" % n_resamples)
    doBootstrap = True
    label+='_bootstrap'

if doBootstrap and ((job and not n_jobs) or (n_jobs and not job)):
    print("Invalid condor info - running everything here")
    job = ""
    n_jobs = ""

n_resamples_here = 0
doNominal=True
if doBootstrap:
    if job and n_jobs:
        print("Job number", job, "(%d out of %d)" % (int(job)+1, int(n_jobs)))
        label+=('_'+job)
        job = int(job)
        n_jobs = int(n_jobs)
        n_per_job = int(n_resamples/n_jobs)
        remainder = n_resamples % n_jobs

        print("Splitting", n_resamples, "resamples into", n_jobs)

        start = job * (n_per_job + 1)* (job < remainder) + (remainder + job * n_per_job)*(job >= remainder)
        end = (job+1) * (n_per_job + 1)* (job < remainder) + (remainder + (job+1)* n_per_job)*(job >= remainder)

        n_resamples_here = end-start
        print("Will resample here", n_resamples_here, "times")
        if job == 0:
            print("Running nominal here as well")
        else:
            doNominal=False
    else:
        start=0
        end=n_resamples
        n_resamples_here=end-start
 
        print("Running nominal and", n_resamples_here, "boostrap resamples here")

rw_tree = options.reweight_tree

all_trees= not options.not_all_trees

params = options.params
save = options.save

param_dict = {}
param_label = ''
if params:
    if len(params) == 1:
        params = params[0]

    if params.find('.yml') != -1 or params.find('.yaml') != -1:
        print("Loading in BDT parameters from file", params)
        param_label = params[:params.find('.y')]
        with open(params, 'r') as stream:
            param_dict = yaml.load(stream)
        print("Loaded in params:")
        for key in param_dict.keys():
            print(key+':', param_dict[key])      
    else:
        print("Interpreting input as list of columns for training")
        param_label = 'col_list'
        param_dict['columns'] = params
        

if rel != 20 and rel !=21:
    rel = 20
    print("Invalid option, defaulting to rel 20")

year =''
yr_short=''
if len(year_in) == 4:
    year=year_in
    yr_short=year_in[2:]
elif len(year_in) == 2:
    year='20'+year_in
    yr_short=year_in
else:
    print("Invalid year format, defaulting to 2015")
    year='2015'
    yr_short='15'

if input_dir:
    if input_dir[-1] != '/': input_dir+='/'

if data_file:
    if data_file[0] == '/':
        input_dir = ''
    data_file = input_dir+data_file
    if data_file[-4:] != 'root':
        print("Warning! Data file not a root file")
    if data_file.find(yr_short) == -1:
        print("Warning! Input data file name may not match year.")
else:
    data_file = input_dir+'output_data'+yr_short+'.root'


do_tth = False
do_tnh = False
if tth_file:
    if tth_file[0] == '/':
        input_dir = ''
    tth_file = input_dir+tth_file
    if tth_file[-4:] != 'root':
        print("Warning! All had ttbar file not a root file")
else:
    do_tth = False

if tnh_file:
    if tnh_file[0] == '/':
        input_dir = ''
    tnh_file = input_dir+tnh_file
    if tnh_file[-4:] != 'root':
        print("Warning! Non all had ttbar file not a root file")
else:
    do_tnh = False

if output_dir:
    if output_dir[-1] != '/': output_dir+='/'

if rw_tree:
    if rw_tree not in ["SBtree", "sideband", "CRtree", "control"]:
        print("Warning! Reweighting tree not recognized. Continuing, but check inputs.")
else:
    if rel == 20:
        rw_tree = "SBtree"
    else:
        rw_tree = "sideband"

reg_label=''
if rw_tree in ["CRtree", "control"]:
    reg_label="_CRderiv"

if label:
    if label[0] != '_':
        label='_'+label

print("Input data file:", data_file)

if do_tth:
    print("Input all-had ttbar file:", tth_file)

if do_tnh:
    print("Input non all-had ttbar file:", tnh_file)

print("Output directory:", output_dir)
print("Year:", year)
print("Training reweighting model on tree", rw_tree)



#Set up appropriate columns for data, mc, and rw. Data/MC slightly different due to some weights
columns_load = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2',
           'njets', 'ntag', 'm_h1', 'm_h2', 'run_number', 'event_number', 'X_wt']

columns_mc_load = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2',
              'njets', 'ntag', 'm_h1', 'm_h2', 'mc_sf', 'run_number', 'event_number', 'X_wt']

if rel == 20:
    columns_load = ['rw_vals.pT_4', 'rw_vals.pT_2', 'rw_vals.ave_eta', 'rw_vals.dRjj_close', 'rw_vals.dRjj_notclose',
                'n_jets', 'n_tag', 'passTrig_'+yr_short, 'mc_weight', 'event.m_h1', 'event.m_h2', 'event_number', 'Xwt']

    columns_mc_load = ['rw_vals.pT_4', 'rw_vals.pT_2', 'rw_vals.ave_eta', 'rw_vals.dRjj_close', 'rw_vals.dRjj_notclose',
                  'n_jets', 'n_tag', 'passTrig_'+yr_short, 'mc_weight',
                  'event.m_h1', 'event.m_h2', 'bFix70_weight', 'event_number', 'Xwt']

if 'columns' in param_dict.keys():
    for column in param_dict['columns']:
        if column not in columns_load:
            columns_load+=[column]
        if column not in columns_mc_load:
            columns_mc_load+=[column] 


print("About to load in data...")
full_data = root_numpy.root2array(data_file,
                             treename=rw_tree, branches=columns_load)
if do_tth:
    full_tth = root_numpy.root2array(tth_file,
                                 treename=rw_tree, branches=columns_mc_load)

if do_tnh:
    full_ttnh = root_numpy.root2array(tnh_file,
                                 treename=rw_tree, branches=columns_mc_load)

#Adjust naming to match (see rw_utils for mapping)
if rel == 20:
    rel20to21 = rel20to21_dict()

    columns21 = [rel20to21[key] for key in full_data.dtype.names]
    if do_tth: columns_mc21 = [rel20to21[key] for key in full_tth.dtype.names]
    if do_tnh and not do_tth: columns_mc21 = [rel20to21[key] for key in full_tnh.dtype.names]

    full_data.dtype.names = tuple(columns21)
    if do_tth: full_tth.dtype.names = tuple(columns_mc21)
    if do_tnh: full_ttnh.dtype.names = tuple(columns_mc21)

rw_columns = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2', 'njets']
if 'columns' in param_dict.keys():
    rw_columns = param_dict['columns']

print("Loaded! Setting up for training...")
full_data = append_fields(full_data, 'BDT_d24_weight'+reg_label, np.ones(len(full_data)), usemask=False)
if do_tth: full_tth = append_fields(full_tth, 'BDT_d24_weight'+reg_label, np.ones(len(full_tth)), usemask=False)
if do_tnh: full_ttnh = append_fields(full_ttnh, 'BDT_d24_weight'+reg_label, np.ones(len(full_ttnh)), usemask=False)

#Seed for reproducibility
np.random.seed(1000)

#Run random so that the call here corresponds to the start'th call  
if doBootstrap:
    for run in range(start):
        _ = np.random.poisson(1, len(full_data))

    for count in range(n_resamples_here+doNominal):
        if doNominal and count == 0:
            resamp_id = ""
        else:
            resamp_id = "%d" % (start+count-doNominal)
            poisson_weight = np.random.poisson(1, len(full_data))
            full_data = append_fields(full_data, 'poissonWeight_resamp_'+resamp_id, poisson_weight, usemask=False)


if rel == 21:
    #Grab appropriate events for each year's triggers. Run numbers taken from resolved-recon's trigger list
    run_bounds = { '2015' : [0, 296939], '2016' : [296939, 324320],
                   '2017' : [324320, 348197], '2018' : [348197, 364486] }
    year_run_range = run_bounds[year]

    full_data = full_data[(full_data['run_number'] > year_run_range[0]) & (full_data['run_number'] < year_run_range[1])]
    if do_tth: full_tth = full_tth[(full_tth['run_number'] > year_run_range[0]) & (full_tth['run_number'] < year_run_range[1])]
    if do_tnh: full_ttnh = full_ttnh[(full_ttnh['run_number'] > year_run_range[0]) & (full_ttnh['run_number'] < year_run_range[1])]

    print('For year', year, 'selected runs between', year_run_range[0], 'and', year_run_range[1])
  #  print('Have', len(full_data), 'data events', len(full_tth), '(unweighted) tth events, and', len(full_ttnh), '(unweighted) ttnh events')


arrs = {}
data = full_data[full_data['X_wt'] > 1.5]
if rel == 20:
    data = data[data['passTrig_'+yr_short]==True]
arrs['dat2']= data[data['ntag'] == 2]
arrs['dat4']= data[data['ntag'] >= 4]

if do_tth:
    tth = full_tth[full_tth['X_wt'] > 1.5]
    if rel == 20:
        tth = tth[tth['passTrig_'+yr_short]==True]
    arrs['th2']= tth[tth['ntag'] == 2]
    arrs['th4']= tth[tth['ntag'] >= 4]

if do_tnh:
    ttnh = full_ttnh[full_ttnh['X_wt'] > 1.5]
    if rel == 20:
        ttnh = ttnh[ttnh['passTrig_'+yr_short]==True]
    arrs['tnh2']= ttnh[ttnh['ntag'] == 2]
    arrs['tnh4']= ttnh[ttnh['ntag'] >= 4]


original = DataFrame(arrs['dat2'][rw_columns])
target = DataFrame(arrs['dat4'][rw_columns])

#Defaults
param_list = ['n_estimators', 'learning_rate', 'max_depth', 'min_samples_leaf', 'gb_args']
BDT_params = {}
BDT_params['n_estimators'] = 50
BDT_params['learning_rate'] = 0.1
BDT_params['max_depth'] = 3
BDT_params['min_samples_leaf'] = 125
BDT_params['gb_args'] = {'subsample': 0.4}

print("Setting up BDT with parameters:")
for key in param_list:
    if key in param_dict.keys():
        BDT_params[key] = param_dict[key]
    print(key+':', BDT_params[key])


print("About to train...")
print("Training on columns:", rw_columns)

reweighter_dict = {}
for count in range(n_resamples_here+doNominal):
    if doNominal and count == 0:
        resamp_id = ""
    else:
        resamp_id = "%d" % (start+count-doNominal)

    reweighter_dict[resamp_id] = reweight.GBReweighter(n_estimators=BDT_params['n_estimators'], learning_rate=BDT_params['learning_rate'], max_depth=BDT_params['max_depth'], 
                                                       min_samples_leaf=BDT_params['min_samples_leaf'], 
                                                       gb_args=BDT_params['gb_args'])

    if resamp_id:
        print("Resampling id:", resamp_id)
        original_weights = arrs['dat2']['poissonWeight_resamp_'+resamp_id]
        target_weights = arrs['dat4']['poissonWeight_resamp_'+resamp_id]
    else:
        original_weights = np.ones(len(arrs['dat2']))
        target_weights = np.ones(len(arrs['dat4']))

    reweighter_dict[resamp_id].fit(original, target, original_weight=original_weights, target_weight=target_weights)


treeNames=[rw_tree]
if all_trees:
    f = TFile(data_file, "read")
    treeNames= [key.GetName() for key in f.GetListOfKeys() if key.GetClassName() == "TTree"]
    f.Close()

save_list = ['event_number']
if rel == 21:
    save_list += ['run_number']
for resamp_id in reweighter_dict.keys():
    if resamp_id:
        resamp_label = "_resampling_"+resamp_id
        save_list += ['BDT_d24_weight'+reg_label+resamp_label]
    else:
        save_list += ['BDT_d24_weight'+reg_label]
save_list_mc = save_list.copy()

if full:
    save_list = columns_load.copy()
    save_list_mc = columns_mc_load.copy()


print("About to write weights for trees: ", treeNames)
for treeCount in range(len(treeNames)):
    if treeCount == 0:
        treemode = 'recreate'
    else:
        treemode = 'update'
 
    treeName = treeNames[treeCount]    

    print("Writing tree", treeName)
    data_out = root_numpy.root2array(data_file, treename=treeName, branches=columns_load)
    if rel == 20:
        data_out.dtype.names = tuple(columns21)
   
    for resamp_id in reweighter_dict.keys():
        if resamp_id:
            resamp_label = "_resampling_"+resamp_id
        else:
            resamp_label = ""
        data_out = append_fields(data_out, 'BDT_d24_weight'+reg_label+resamp_label, np.ones(len(data_out)), usemask=False)
        isdat2 = data_out['ntag'] == 2
        data_out['BDT_d24_weight'+reg_label+resamp_label][isdat2] = reweighter_dict[resamp_id].predict_weights(DataFrame(data_out[isdat2][rw_columns]))
    root_numpy.array2root(data_out[save_list], (output_dir+'dat_BDT_d24_'+yr_short+'_rel%d'+reg_label+label+'.root') % rel,
                          treename=treeName, mode=treemode)

    if do_tth:
        tth_out = root_numpy.root2array(tth_file, treename=treeName, branches=columns_mc_load)
        if rel == 20:
            tth_out.dtype.names = tuple(columns_mc21)

        for resamp_id in reweighter_dict.keys():
            if resamp_id:
                resamp_label = "_resampling_"+resamp_id
            else:
                resamp_label = ""
            tth_out = append_fields(tth_out, 'BDT_d24_weight'+reg_label+resamp_label, np.ones(len(tth_out)), usemask=False)
            istth2 = tth_out['ntag'] == 2
            tth_out['BDT_d24_weight'+reg_label+resamp_label][istth2] = reweighter_dict[resamp_id].predict_weights(DataFrame(tth_out[istth2][rw_columns]))
        root_numpy.array2root(tth_out[save_list_mc], (output_dir+'tth_BDT_d24_'+yr_short+'_rel%d'+reg_label+label+'.root') % rel, 
                              treename=treeName, mode=treemode)

    if do_tnh:
        ttnh_out = root_numpy.root2array(tnh_file, treename=treeName, branches=columns_mc_load)
        if rel == 20:
            tth_out.dtype.names = tuple(columns_mc21)

        for resamp_id in reweighter_dict.keys():
            if resamp_id:
                resamp_label = "_resampling_"+resamp_id
            else:
                resamp_label = ""
            ttnh_out = append_fields(ttnh_out, 'BDT_d24_weight'+reg_label+resamp_label, np.ones(len(ttnh_out)), usemask=False)
            isttnh2 = ttnh_out['ntag'] == 2
            ttnh_out['BDT_d24_weight'+reg_label+resamp_label][isttnh2] = reweighter_dict[resamp_id].predict_weights(DataFrame(ttnh_out[isttnh2][rw_columns]))
        root_numpy.array2root(ttnh_out[save_list_mc], (output_dir+'ttnh_BDT_d24_'+yr_short+'_rel%d'+reg_label+label+'.root') % rel, 
                              treename=treeName, mode=treemode)

isdat2 = full_data['ntag']==2
for resamp_id in reweighter_dict.keys():
    if resamp_id:
        resamp_label = "_resampling_"+resamp_id
        original_weights = full_data['poissonWeight_resamp_'+resamp_id]
        full_data = append_fields(full_data, 'BDT_d24_weight'+reg_label+resamp_label, np.ones(len(full_data)), usemask=False)
    else:
        resamp_label = ""
        original_weights = np.ones(len(full_data))

    full_data['BDT_d24_weight'+reg_label+resamp_label][isdat2] = reweighter_dict[resamp_id].predict_weights(DataFrame(full_data[isdat2][rw_columns]),
                                                                                            original_weight=original_weights[isdat2])
    norm_factor = 1.*np.sum(original_weights[full_data['ntag']>=4])/np.sum(full_data[full_data['ntag']==2]['BDT_d24_weight'+reg_label+resamp_label])

    norm = TParameter("double")("BDT_norm"+reg_label+resamp_label, norm_factor)
    f = TFile.Open((output_dir+'dat_BDT_d24_'+yr_short+'_rel%d'+reg_label+label+'.root') % rel, "update")
    norm.Write()
    f.Close()

if save:
    if param_label:
        param_label = '_config_'+param_label
    BDT_fname = (output_dir+'BDT_d24_'+yr_short+'_rel%d'+param_label+reg_label+label+'.p') % rel
    pickle.dump(reweighter, open( BDT_fname, "wb" ))
