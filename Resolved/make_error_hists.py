#!/usr/bin/env python3

import numpy as np
from numpy.lib.recfunctions import append_fields
import scipy.optimize as opt
from rw_utils import *
from error_handling import errors
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-i", "--input_dir", dest="input_dir", default="",
                  help="Input directory")
parser.add_option("-o", "--output_dir", dest="output_dir", default="",
                  help="Output directory")
parser.add_option("-d", "--data", dest="data_file", default="",
                  help="Input data filename")
parser.add_option("-y", "--year", dest="year", default="2015",
                  help="Year")
parser.add_option("--HCrescale",
                  action="store_true", dest="HCrescale", default=False,
                  help="Rescale HC 4 vecs to have m=125 GeV")
parser.add_option("-l", "--label", dest="label", default="",
                  help="Label for output")
parser.add_option("--run-shape-systs",
                  action="store_true", dest="doShapeSysts", default=False,
                  help="Include multijet shape variations in output")
parser.add_option("--bootstrap", dest="n_resamples", default="-1",
                  help="Turn on bootstrapping. Put in number of resamples.")

(options, args) = parser.parse_args()

import root_numpy
import ROOT
from ROOT import TLorentzVector, TFile, TParameter

data_file = options.data_file
year_in = options.year
input_dir = options.input_dir
output_dir = options.output_dir
HCrescale = options.HCrescale
doShapeSysts = options.doShapeSysts
n_resamples = int(options.n_resamples)
label = options.label

doBootstrap = (n_resamples != -1)

if label:
   if label[0] != '_':
       label = '_'+label

year =''
yr_short=''
if len(year_in) == 4:
    year=year_in
    yr_short=year_in[2:]
elif len(year_in) == 2:
    year='20'+year_in
    yr_short=year_in
else:
    print("Invalid year format, defaulting to 2015")
    year='2015'
    yr_short='15'


if input_dir:
    if input_dir[-1] != '/': input_dir+='/'

if output_dir:
    if output_dir[-1] != '/': output_dir+='/'

if data_file:
    if data_file[0] != '/':
        data_file = input_dir+data_file
    if data_file[-4:] != 'root':
        print("Warning! Data file not a root file")
    if data_file.find(yr_short) == -1:
        print("Warning! Input data file name may not match year.")

columns = ['pT_4', 'pT_2', 'njets', 'ntag', 'm_h1', 'm_h2', 'run_number',
           'm_hh', 'X_wt', 'pT_h1', 'eta_h1', 'phi_h1', 'pT_h2', 'eta_h2', 'phi_h2', 
           'BDT_d24_weight_'+yr_short]


vec4_vars = ['pT_h1', 'eta_h1', 'phi_h1', 'm_h1', 'pT_h2', 'eta_h2', 'phi_h2', 'm_h2']

if HCrescale:
    label+= "_HCrescale"
    for var in vec4_vars:
        if var not in columns:
            columns += [var]

HT_vars = ['pT_h1_j1', 'pT_h1_j2', 'pT_h2_j1', 'pT_h2_j2']

if doShapeSysts:
    print("Making shape systematics for year", year)
    label+='_ShapeSysts'
    for var in HT_vars+['BDT_d24_weight_CRderiv_'+yr_short]:
        if var not in columns:
            columns+=[var]
    

f=TFile(data_file, "read")
BDT_norm_nom = (f.Get("BDT_norm_"+yr_short)).GetVal()
if doShapeSysts:
    BDT_norm_CRderiv = (f.Get("BDT_norm_CRderiv_"+yr_short)).GetVal()
if doBootstrap:
    print("Making bootstrap errors for year", year)
    label+='_bootstrap'
    BDT_norms={}
    for bstrap in range(n_resamples):
        columns+=[('BDT_d24_weight_resampling_%d_'+yr_short)%bstrap]
        BDT_norms[bstrap]=(f.Get(("BDT_norm_resampling_%d_"+yr_short) % bstrap)).GetVal()
f.Close()

print("About to load data...")
full_data = root_numpy.root2array(data_file,
                             treename='fullmassplane', branches=columns)
print("Loaded!")

#Grab appropriate events for each year's triggers. Run numbers taken from resolved-recon's trigger list
run_bounds = { '2015' : [0, 296939], '2016' : [296939, 324320],
               '2017' : [324320, 348197], '2018' : [348197, 364486] }
year_run_range = run_bounds[year]

full_data = full_data[(full_data['run_number'] > year_run_range[0]) & (full_data['run_number'] < year_run_range[1])]

full_data = full_data[full_data['X_wt']>1.5]
dat2 = full_data[full_data['ntag'] == 2]
dat2 = SRsel(dat2)


if HCrescale:
    print("About to rescale m_hh")
    h1 = TLorentzVector()
    h2 = TLorentzVector()
    for event in range(len(arrs[key])):
        if event % 10000 == 0: print(event, "out of", len(dat2))
        h1.SetPtEtaPhiM(dat2['pT_h1'][event], dat2['eta_h1'][event], dat2['phi_h1'][event], dat2['m_h1'][event])
        h2.SetPtEtaPhiM(dat2['pT_h2'][event], dat2['eta_h2'][event], dat2['phi_h2'][event], dat2['m_h2'][event])
        dat2['m_hh'][event] = corr_mass(h1,h2)


BDTerrs=errors(dat2, year, np.linspace(0,1400,66))

lim_file = ROOT.TFile(output_dir+"resolved_4bSR_BDTbackground"+label+"_"+year+".root", "recreate")

if doBootstrap:
    hist_BDT = BDTerrs.runBootstrap(BDT_norms, BDT_norm_nom)
    hist_BDT.Write('BDT_hh')

if doShapeSysts:
    shape_systs = BDTerrs.runShapeSystematics(BDT_norm_nom, BDT_norm_CRderiv)

    if not doBootstrap:
        shape_systs[''].Write('BDT_hh')
 
    shape_systs['LowHtCRw'].Write('BDT_hh_LowHtCRw')
    shape_systs['HighHtCRw'].Write('BDT_hh_HighHtCRw')
    shape_systs['LowHtCRi'].Write('BDT_hh_LowHtCRi')
    shape_systs['HighHtCRi'].Write('BDT_hh_HighHtCRi')

lim_file.Close()

