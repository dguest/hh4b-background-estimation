#!/usr/bin/env python3

import yaml
import io

# Define data
data = {'columns': ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2', 'njets'],
        'n_estimators': 50,
        'learning_rate': 0.1,
        'max_depth': 3, 
        'min_samples_leaf': 125, 
        'gb_args': {'subsample': 0.4}}

# Write YAML file
with io.open('template.yaml', 'w', encoding='utf8') as outfile:
    yaml.dump(data, outfile, default_flow_style=False, allow_unicode=True)
