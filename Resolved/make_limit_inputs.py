#!/usr/bin/env python3

import numpy as np
from numpy.lib.recfunctions import append_fields

import scipy.optimize as opt
from scipy.interpolate import interp1d
from scipy import ndimage

from rw_utils import *
from xsbook import xsLookup
from dsid_to_mass import dsid_to_mass_dict 
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-i", "--input_dir", dest="input_dir", default="",
                  help="Input directory")
parser.add_option("-o", "--output_dir", dest="output_dir", default="",
                  help="Output directory")
parser.add_option("-d", "--data", dest="data_file", default="",
                  help="Input data filename")
parser.add_option("-t", "--tth", dest="tth_file", default="",
                  help="Input all had ttbar filename")
parser.add_option("-n", "--tnh", dest="tnh_file", default="",
                  help="Input non all-had ttbar filename")
parser.add_option("-y", "--year", dest="year", default="2015",
                  help="Year")
parser.add_option("--graviton", dest="graviton", default="",
                  help="Input graviton signal file template. Should have mass as mXXX")
parser.add_option("--smnr", dest="smnr", default="",
                  help="Input SMNR signal file ")
parser.add_option("--signal-list", dest="signal_list", default="",
                  help="Input text file with signal samples")
parser.add_option("-r", "--release", dest="rel", default="20",
                  help="Release 20 or 21")
parser.add_option("-b", "--BDT",
                  action="store_true", dest="BDT", default=False,
                  help="Do plots with BDT")
parser.add_option("--HCrescale",
                  action="store_true", dest="HCrescale", default=False,
                  help="Rescale HC 4 vecs to have m=125 GeV")
parser.add_option("-s", "--spline",
                  dest="spline", default=-1,
                  help="Turn on spline plot, give number of the iteration. If default (=-1) don't plot.")
parser.add_option("-l", "--label", dest="label", default="",
                  help="Label for output")
parser.add_option("--run-shape-systs", 
                  action="store_true", dest="doShapeSysts", default=False,
                  help="Include multijet shape variations in output")

(options, args) = parser.parse_args()

import root_numpy
import ROOT
from ROOT import TLorentzVector, TFile, TParameter

BDT = options.BDT
it = int(options.spline)
spline = (it != -1)
data_file = options.data_file
tth_file = options.tth_file
tnh_file = options.tnh_file
year_in = options.year
input_dir = options.input_dir
output_dir = options.output_dir
rel = int(options.rel[:2])
label= options.label
grav_template = options.graviton
signal_list_file = options.signal_list
smnr_file = options.smnr
HCrescale = options.HCrescale
doShapeSysts = options.doShapeSysts

if HCrescale:
    label+= "_HCrescale"

if label:
   if label[0] != '_':
       label = '_'+label
else:
   print("Please use a label!")

if rel != 20 and rel !=21:
    rel = 20
    print("Invalid option, defaulting to rel 20")

year =''
yr_short=''
if len(year_in) == 4:
    year=year_in
    yr_short=year_in[2:]
elif len(year_in) == 2:
    year='20'+year_in
    yr_short=year_in
else:
    print("Invalid year format, defaulting to 2015")
    year='2015'
    yr_short='15'


if input_dir:
    if input_dir[-1] != '/': input_dir+='/'

if output_dir:
    if output_dir[-1] != '/': output_dir+='/'

if smnr_file:
    if smnr_file[0] != '/':
        smnr_file = input_dir+smnr_file
    if smnr_file[-4:] != 'root':
        print("Warning! SMNR file not a root file")

grav_files = []
masses = [400, 500, 600, 700, 800, 900, 1000]
if grav_template:
    if grav_template.find('M') != -1:
        pos = grav_template.find('M')
    elif grav_template.find('m') != -1:
        pos = grav_template.find('m')
    else:
        print("Not able to find mass in grav template!")
    
    front = grav_template[:pos+1]
    back = grav_template[pos+1:]
    back = back[back.find('_'):]

    if grav_template[0] != '/':
        front=input_dir+front

    for mass in masses:
        grav_files.append(front+str(mass)+back)
else:
    masses=[]

scalar_files = []
scalar_masses = []
if signal_list_file:
    mass_dict = dsid_to_mass_dict()
    period_dict = { '2015': 'mc16a', '2016': 'mc16a', '2017': 'mc16d', '2018': 'mc16e'}
    with open(signal_list_file) as f:
        for line in f:
            if line.find(period_dict[year]) != -1:
                if line.find("450000") != -1:
                    smnr_file=line.rstrip()

                s_idx = line.find("4502")
                if s_idx != -1:
                    scalar_files.append(line.rstrip())
                    scalar_masses.append(mass_dict[line[s_idx:s_idx+6]])

print(smnr_file)
if data_file:
    if data_file[0] != '/':
        data_file = input_dir+data_file
    if data_file[-4:] != 'root':
        print("Warning! Data file not a root file")
    if data_file.find(yr_short) == -1:
        print("Warning! Input data file name may not match year.")
else:
    if BDT:
        print("Warning! Need to specify input data name for BDT. Defaulting to spline")
        BDT = False
        spline= True
    data_file = (input_dir+'dat_it_%d_'+yr_short+'_rel%d.root') % (it,rel)

if tth_file:
    if tth_file[0] != '/':
        tth_file = input_dir+tth_file
    if tth_file[-4:] != 'root':
        print("Warning! All had ttbar file not a root file")
else:
    tth_file = (input_dir+'tth_it_%d_'+yr_short+'_rel%d.root') % (it,rel)

if tnh_file:
    if tnh_file[0] != '/':
        tnh_file = input_dir+tnh_file
    if tnh_file[-4:] != 'root':
        print("Warning! Non all had ttbar file not a root file")
else:
    tnh_file = (input_dir+'ttnh_it_%d_'+yr_short+'_rel%d.root') % (it,rel)


#Set up appropriate columns for data, mc, and rw. Data/MC slightly different due to some weights
columns = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2',
           'njets', 'ntag', 'm_h1', 'm_h2', 'run_number', 'm_hh', 'X_wt']

columns_mc = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2',
              'njets', 'ntag', 'm_h1', 'm_h2', 'mc_sf', 'run_number', 'm_hh', 'X_wt']

rw_columns = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2']

vec4_vars = ['pT_h1', 'eta_h1', 'phi_h1', 'm_h1', 'pT_h2', 'eta_h2', 'phi_h2', 'm_h2']

syst_vars = ['pT_h1_j1', 'pT_h1_j2', 'pT_h2_j1', 'pT_h2_j2']

if rel == 20:
    columns = ['pT_4', 'pT_2', 'ave_eta', 'dRjj_close', 'dRjj_notclose',
               'n_jets', 'n_tag', 'passTrig_'+yr_short, 'nmuon_isIsolated_custom','mc_weight',
               'm_h1', 'm_h2', 'Xwt', 'm_4j', 'Xhh']

    columns_mc = ['pT_4', 'pT_2', 'ave_eta', 'dRjj_close', 'dRjj_notclose',
                  'n_jets', 'n_tag', 'passTrig_'+yr_short, 'nmuon_isIsolated_custom', 'mc_weight',
                  'm_h1', 'm_h2', 'Xwt', 'bFix70_weight', 'm_4j', 'Xhh']

if HCrescale:
    for var in vec4_vars:
        if var not in columns: 
            columns += [var]
        if var not in columns_mc: 
            columns_mc += [var]

if doShapeSysts:
    for var in syst_vars:
        if var not in columns:
            columns += [var]
        if var not in columns_mc:
            columns_mc += [var]


if smnr_file:
   it_smnr = root_numpy.root2array(smnr_file,
                                 treename='fullmassplane', branches=columns_mc)
   if rel == 20:
        rel20to21 = rel20to21_dict()
        columns_adj = [rel20to21[key] for key in it_smnr.dtype.names]

        it_smnr.dtype.names = tuple(columns_adj)

   print("Loaded SMNR")

it_grav = {}
for i in range(len(grav_files)):
    it_grav[masses[i]] = root_numpy.root2array(grav_files[i],
                                 treename='fullmassplane', branches=columns_mc)
    if rel == 20:
        rel20to21 = rel20to21_dict()
        columns_adj = [rel20to21[key] for key in it_grav[masses[i]].dtype.names]

        it_grav[masses[i]].dtype.names = tuple(columns_adj)
    
    print("Loaded graviton, mass", masses[i])

it_scalar = {}
for i in range(len(scalar_files)):
    it_scalar[scalar_masses[i]] = root_numpy.root2array(scalar_files[i],
                                 treename='fullmassplane', branches=columns_mc)
    if rel == 20:
        rel20to21 = rel20to21_dict()
        columns_adj = [rel20to21[key] for key in it_scalar[scalar_masses[i]].dtype.names]

        it_scalar[scalar_masses[i]].dtype.names = tuple(columns_adj)

    print("Loaded scalar, mass", scalar_masses[i])


if BDT: 
    columns += ['BDT_d24_weight_'+yr_short]
    if doShapeSysts:
        columns+=['BDT_d24_weight_CRderiv_'+yr_short]
if spline:
    if data_file.find('_it_') != -1:
        columns += ['njet_weight', 'rw_weight']
        if doShapeSysts:
            columns+=['njet_weight_CRderiv', 'rw_weight_CRderiv']
    else:
        columns += [('njet_weight_it_%d_'+yr_short) % it, ('rw_weight_it_%d_'+yr_short) % it]
        if doShapeSysts:
            columns+=[('njet_weight_it_%d_CRderiv_'+yr_short) % it, ('rw_weight_it_%d_CRderiv_'+yr_short) % it]

    if tth_file.find('_it_') != -1 and ttnh_file.find('_it_') != -1:
        columns_mc += ['njet_weight', 'rw_weight']
        if doShapeSysts:
            columns_mc+=['njet_weight_CRderiv', 'rw_weight_CRderiv']
    else:
        columns_mc += [('njet_weight_it_%d_'+yr_short) % it, ('rw_weight_it_%d_'+yr_short) % it]
        if doShapeSysts:
            columns_mc+=[('njet_weight_it_%d_CRderiv_'+yr_short) % it, ('rw_weight_it_%d_CRderiv_'+yr_short) % it]

post_adj_dat = False
post_adj_mc = False
#Adjust rel 20 naming to match (see rw_utils for mapping). Note that adjusted naming is what is saved in reweight.py output
if rel == 20:
    rel20to21 = rel20to21_dict()
    if 'njet_weight' not in columns:
        post_adj_dat = True
    else:
        columns = [rel20to21[key] for key in columns]
 
    if 'njet_weight' not in columns_mc:
        post_adj_mc = True
    else:
        columns_mc = [rel20to21[key] for key in columns_mc]


#Set up x labels and plotting variables
x_label_dict = {'pT_4': 'HC Jet 4 p_{T} [GeV]', 'pT_2': 'HC Jet 2 p_{T} [GeV]', 'eta_i': '<|HC #eta|>', 
                'dRjj_1': '#Delta R_{jj} Close', 'dRjj_2': '#Delta R_{jj} Not Close', 'njets' : '# of additional jets', 'm_hh' : 'm_{4j} [GeV]'}


columns_to_plot = rw_columns + ['njets','m_hh']

it_data = root_numpy.root2array(data_file,
                             treename='fullmassplane', branches=columns)

print("Loaded data")

sfs = {'nom':{}}
if doShapeSysts:
    sfs['CRw'] = {}

if rel == 20 and post_adj_dat:
    rel20to21 = rel20to21_dict()
    columns = [rel20to21[key] for key in it_data.dtype.names]

    it_data.dtype.names = tuple(columns)

#Adjust verbose branchnames (with it, year) to be base ones
columns = [vanillize_name(key) for key in it_data.dtype.names]
it_data.dtype.names = tuple(columns)

if spline:
    it_tth = root_numpy.root2array(tth_file,
                                 treename='fullmassplane', branches=columns_mc)
    it_ttnh = root_numpy.root2array(tnh_file,
                                 treename='fullmassplane', branches=columns_mc)
    if rel == 20 and post_adj_mc:
        rel20to21 = rel20to21_dict()
        columns_mc = [rel20to21[key] for key in it_tth.dtype.names]
        it_tth.dtype.names = tuple(columns_mc)
        it_ttnh.dtype.names = tuple(columns_mc) 

    #Adjust verbose branchnames (with it, year) to be base ones
    columns_mc = [vanillize_name(key) for key in it_tth.dtype.names]
    it_tth.dtype.names = tuple(columns_mc)
    it_ttnh.dtype.names = tuple(columns_mc)

    print("Loaded ttbar")
   
    sf_file = TFile((input_dir+'spline_info_all_rel%d.root') % (rel), "read")
    sf_hist = sf_file.Get(("sf_hist_it_%d_"+yr_short) % it)
    sfs['nom']['tnh'] = sf_hist.GetBinContent(1)
    sfs['nom']['th'] = sf_hist.GetBinContent(2)
    sfs['nom']['qcd'] = sf_hist.GetBinContent(3)
    sfs['nom']['tnh2'] = sf_hist.GetBinContent(4)
    sfs['nom']['f'] = (sf_file.Get(("f_it_%d_"+yr_short) % it)).GetVal()
    if rel == 20:
        sfs['nom']['fth24'] = (sf_file.Get(("f_th24_it_%d_"+yr_short) % it)).GetVal()
    sf_file.Close()

    if doShapeSysts:
        sf_file_CRw = TFile((input_dir+'spline_info_all_rel%d_CRderiv.root') % (rel), "read")
        sf_hist_CRw = sf_file_CRw.Get(("sf_hist_it_%d_"+yr_short) % it)
        sfs['CRw']['tnh'] = sf_hist_CRw.GetBinContent(1)
        sfs['CRw']['th'] = sf_hist_CRw.GetBinContent(2)
        sfs['CRw']['qcd'] = sf_hist_CRw.GetBinContent(3)
        sfs['CRw']['tnh2'] = sf_hist_CRw.GetBinContent(4)
        sfs['CRw']['f'] = (sf_file_CRw.Get(("f_it_%d_"+yr_short) % it)).GetVal()
        if rel == 20:
            sfs['CRw']['fth24'] = (sf_file_CRw.Get(("f_th24_it_%d_"+yr_short) % it)).GetVal()
        sf_file_CRw.Close()
  
    

if BDT:
    file_for_norm = TFile(data_file, "read")
    sfs['nom']['BDT_norm']= (file_for_norm.Get("BDT_norm_"+yr_short)).GetVal()
    if doShapeSysts:
        sfs['CRw']['BDT_norm'] = (file_for_norm.Get("BDT_norm_CRderiv_"+yr_short)).GetVal()
    file_for_norm.Close()
   
if rel == 21:

    #Grab appropriate events for each year's triggers. Run numbers taken from resolved-recon's trigger list
    run_bounds = { '2015' : [0, 296939], '2016' : [296939, 324320],
                   '2017' : [324320, 348197], '2018' : [348197, 364486] }
    year_run_range = run_bounds[year]

    it_data = it_data[(it_data['run_number'] > year_run_range[0]) & (it_data['run_number'] < year_run_range[1])]

    if doShapeSysts:
        dataHT = it_data['pT_h1_j1'] + it_data['pT_h1_j2'] + it_data['pT_h2_j1'] + it_data['pT_h2_j2']
        it_data = append_fields(it_data, 'H_T', dataHT, usemask=False)

    if spline:
        it_tth = it_tth[(it_tth['run_number'] > year_run_range[0]) & (it_tth['run_number'] < year_run_range[1])]
        it_ttnh = it_ttnh[(it_ttnh['run_number'] > year_run_range[0]) & (it_ttnh['run_number'] < year_run_range[1])]

        if doShapeSysts:
            tthHT = it_tth['pT_h1_j1'] + it_tth['pT_h1_j2'] + it_tth['pT_h2_j1'] + it_tth['pT_h2_j2']
            it_tth = append_fields(it_tth, 'H_T', tthHT, usemask=False)

            ttnhHT = it_ttnh['pT_h1_j1'] + it_ttnh['pT_h1_j2'] + it_ttnh['pT_h2_j1'] + it_ttnh['pT_h2_j2']
            it_ttnh = append_fields(it_ttnh, 'H_T', ttnhHT, usemask=False)

 
    if smnr_file:
        it_smnr = it_smnr[(it_smnr['run_number'] > year_run_range[0]) & (it_smnr['run_number'] < year_run_range[1])]
    for mass in masses:
        it_grav[mass] = it_grav[mass][(it_grav[mass]['run_number'] > year_run_range[0]) & (it_grav[mass]['run_number'] < year_run_range[1])]
    for mass in scalar_masses:
        it_scalar[mass] = it_scalar[mass][(it_scalar[mass]['run_number'] > year_run_range[0]) & (it_scalar[mass]['run_number'] < year_run_range[1])]

arrs = {}
hists = {}
weights = {}
fit_counts = {}

data = it_data.copy()
if rel == 20:
    data = data[data['passTrig_'+yr_short]==True]

data = data[data['X_wt'] > 1.5]
arrs['dat2']= data[data['ntag'] == 2]
arrs['dat4']= data[data['ntag'] >= 4]

arrs['dat2'] = SRsel(arrs['dat2'])
arrs['dat4'] = SRsel(arrs['dat4'])

if doShapeSysts:
    arrs['dat2_LowHt']   = arrs['dat2'][arrs['dat2']['H_T'] < 300]
    arrs['dat2_HighHt']  = arrs['dat2'][arrs['dat2']['H_T'] >= 300]
    arrs['dat2_LowHtCRw']   = arrs['dat2_LowHt'].copy()
    arrs['dat2_HighHtCRw']  = arrs['dat2_HighHt'].copy()

lumi15 = 3.23749*(rel==20)+3.2195*(rel==21)
lumi16 = 24.3219*(rel==20)+24.5556*(rel==21)
lumi17 = 44.3072
lumi18 = 36.1593
lumi = (year=='2015')*lumi15 + (year=='2016')*lumi16 + (year=='2017')*lumi17 + (year=='2018')*lumi18

#For rel21 15+16, overall normalization is to combined 15+16 lumi
if rel == 21 and (year == '2015' or year == '2016'):
    lumi = lumi15+lumi16

if rel == 20 and (year == '2017' or year == '2018'):
    print('Warning! Rel 20 should not exist for', year,'. Continuing, but check inputs!')

if not lumi:
    print('Warning! No lumi specified!!')

if smnr_file:
    smnr = SRsel(it_smnr[(it_smnr['X_wt'] > 1.5)])
    if rel == 20:
        smnr = smnr[smnr['passTrig_'+yr_short]==True]
    arrs['smnr4']= smnr[smnr['ntag'] >= 4]

    if rel == 20:
        weights['smnr4'] =  xsLookup('SMNR', rel_for_xs)*lumi*arrs['smnr4']['bFix70_weight']*arrs['smnr4']['mc_weight']#*arrs['rel21_4']['pileup_weight']
    else:
        weights['smnr4'] = arrs['smnr4']['mc_sf']*lumi

    print("SUM =",np.sum(weights['smnr4']))
for mass in masses:
    grav = SRsel(it_grav[mass][(it_grav[mass]['X_wt'] > 1.5)])
    if rel == 20:
        grav = grav[grav['passTrig_'+yr_short]==True]
    arrs['g10_M%d_4'%mass]= grav[grav['ntag'] >= 4]

    if rel == 20:
        weights['g10_M%d_4'%mass] =  xsLookup('g10_M%d'%mass, rel_for_xs)*lumi*arrs['g10_M%d_4'%mass]['bFix70_weight']*arrs['g10_M%d_4'%mass]['mc_weight']#*arrs['rel21_4']['pileup_weight']
    else:
        weights['g10_M%d_4'%mass] = arrs['g10_M%d_4'%mass]['mc_sf']*lumi

for mass in scalar_masses:
    scalar = SRsel(it_scalar[mass][(it_scalar[mass]['X_wt'] > 1.5)])
    if rel == 20:
        scalar = scalar[scalar['passTrig_'+yr_short]==True]
    arrs['scalar_M%d_4'%mass]= scalar[scalar['ntag'] >= 4]

    if rel == 20:
        weights['scalar_M%d_4'%mass] =  xsLookup('scalar_M%d'%mass, rel_for_xs)*lumi*arrs['scalar_M%d_4'%mass]['bFix70_weight']*arrs['scalar_M%d_4'%mass]['mc_weight']#*arrs['rel21_4']['pileup_weight']
    else:
        weights['scalar_M%d_4'%mass] = arrs['scalar_M%d_4'%mass]['mc_sf']*lumi

if spline:
    tth = it_tth.copy()
    if rel == 20:
        tth = tth[tth['passTrig_'+yr_short]==True]
    tth = tth[tth['X_wt'] > 1.5]
    arrs['th2']= tth[tth['ntag'] == 2]
    if rel == 20:
        arrs['th4']= arrs['th2'].copy()
    else:
        arrs['th4']= tth[tth['ntag'] >= 4]

    arrs['th2'] = SRsel(arrs['th2'])
    arrs['th4'] = SRsel(arrs['th4'])

    if doShapeSysts:
        arrs['th2_LowHt']   = arrs['th2'][arrs['th2']['H_T'] < 300]
        arrs['th2_HighHt']  = arrs['th2'][arrs['th2']['H_T'] >= 300]
        arrs['th2_LowHtCRw']   = arrs['th2_LowHt'].copy()
        arrs['th2_HighHtCRw']  = arrs['th2_HighHt'].copy() 

    ttnh = it_ttnh.copy()
    if rel == 20:
        ttnh = ttnh[ttnh['passTrig_'+yr_short]==True]
    ttnh = ttnh[ttnh['X_wt'] > 1.5]
    arrs['tnh2']= ttnh[ttnh['ntag'] == 2]
    arrs['tnh4']= ttnh[ttnh['ntag'] >= 4]

    arrs['tnh2'] = SRsel(arrs['tnh2'])
    arrs['tnh4'] = SRsel(arrs['tnh4'])

    if doShapeSysts:
        arrs['tnh2_LowHt']   = arrs['tnh2'][arrs['tnh2']['H_T'] < 300]
        arrs['tnh2_HighHt']  = arrs['tnh2'][arrs['tnh2']['H_T'] >= 300]
        arrs['tnh2_LowHtCRw']   = arrs['tnh2_LowHt'].copy()
        arrs['tnh2_HighHtCRw']  = arrs['tnh2_HighHt'].copy()

    keys = ['dat2', 'dat4', 'th2', 'th4', 'tnh2', 'tnh4']

    if doShapeSysts:
        keys+=['dat2_LowHt', 'th2_LowHt', 'tnh2_LowHt', 
               'dat2_HighHt', 'th2_HighHt', 'tnh2_HighHt',
               'dat2_LowHtCRw', 'th2_LowHtCRw', 'tnh2_LowHtCRw',
               'dat2_HighHtCRw', 'th2_HighHtCRw', 'tnh2_HighHtCRw']

    if rel == 20:
       arrs['th4']['njet_weight'] = nJetWeight(arrs['th4'], fth24)

    for key in keys:
        doCRw = (key.find('CRw') != -1)
        weights[key] = makeWeights(arrs, key, int(it), int(year), rel, doCRw=doCRw)

from rootpy.plotting import *
ROOT.gROOT.SetBatch(ROOT.kTRUE)

all_keys=[]
if spline:
    keys_to_add = ['dat2', 'dat4', 'th2', 'th4', 'tnh2', 'tnh4']
    if doShapeSysts:
        keys_to_add += ['dat2_LowHt', 'th2_LowHt', 'tnh2_LowHt',
                     'dat2_HighHt', 'th2_HighHt', 'tnh2_HighHt',
                     'dat2_LowHtCRw', 'th2_LowHtCRw', 'tnh2_LowHtCRw',
                     'dat2_HighHtCRw', 'th2_HighHtCRw', 'tnh2_HighHtCRw']
    for key in keys_to_add:
        if key not in all_keys:
            all_keys.append(key)
if BDT:
    keys_to_add = ['dat2', 'dat4']
    if doShapeSysts:
        keys_to_add+=['dat2_LowHt', 'dat2_HighHt', 'dat2_LowHtCRw', 'dat2_HighHtCRw']
    for key in keys_to_add:
        if key not in all_keys:
            all_keys.append(key)

if smnr_file:
    all_keys+= ['smnr4']
all_keys += [("g10_M%d_4" % mass) for mass in masses]
all_keys += [("scalar_M%d_4" % mass) for mass in scalar_masses]

if HCrescale:
    print("About to rescale m_hh")
    h1 = TLorentzVector()
    h2 = TLorentzVector()
    for key in all_keys:
        print(key)
        for event in range(len(arrs[key])):
            if event % 10000 == 0: print(event, "out of", len(arrs[key]))
            h1.SetPtEtaPhiM(arrs[key]['pT_h1'][event], arrs[key]['eta_h1'][event], arrs[key]['phi_h1'][event], arrs[key]['m_h1'][event])
            h2.SetPtEtaPhiM(arrs[key]['pT_h2'][event], arrs[key]['eta_h2'][event], arrs[key]['phi_h2'][event], arrs[key]['m_h2'][event])
            arrs[key]['m_hh'][event] = corr_mass(h1,h2)

methods = []
if BDT:
    methods.append('BDT')
if spline:
    methods.append('spline')

reg_list = ['']
if doShapeSysts:
    reg_list = ['', '_LowHt', '_HighHt', '_LowHtCRw', '_HighHtCRw']

for method in methods:
    if doShapeSysts:
        store_for_symm = {}
        store_for_add = {}
    lim_file = ROOT.TFile(output_dir+"resolved_4bSR_"+method+label+"_"+year+".root", "recreate")
    for column in ['m_hh']:
        n_bins= 66
        xlim = [100,1400]

        for reg in reg_list:

            sfid = 'nom'
            if reg.find('CRw') != -1:
                sfid='CRw'

            if method == 'spline':
                if reg.find('Ht') == -1:
                    hist_tth_4tag = Hist(n_bins, xlim[0], xlim[1], title='All hadronic t#bar{t}', legendstyle='F', drawstyle='hist')
                    hist_tth_4tag.fill_array(arrs['th4'+reg][column], weights=weights['th4'+reg]*sfs[sfid]['th'])
                    hist_ttnh_4tag =  Hist(n_bins, xlim[0], xlim[1], title='Leptonic t#bar{t}', legendstyle='F', drawstyle='hist')
                    hist_ttnh_4tag.fill_array(arrs['tnh4'+reg][column], weights=weights['tnh4'+reg]*sfs[sfid]['tnh'])
                hist_qcd = Hist(n_bins, xlim[0], xlim[1], title='QCD', legendstyle='F',drawstyle='hist')
                hist_qcd.fill_array(arrs['dat2'+reg][column], weights=weights['dat2'+reg])

                hist_ttnh_2tag = Hist(n_bins, xlim[0], xlim[1], title='QCD', legendstyle='F',drawstyle='hist')
                hist_ttnh_2tag.fill_array(arrs['tnh2'+reg][column], weights=weights['tnh2'+reg]*sfs[sfid]['tnh2'])

                hist_tth_2tag = Hist(n_bins, xlim[0], xlim[1], title='QCD', legendstyle='F',drawstyle='hist')
                hist_tth_2tag.fill_array(arrs['th2'+reg][column], weights=weights['th2'+reg])

                print("ttnh2:", hist_ttnh_2tag.Integral(), "tth2:", hist_tth_2tag.Integral())
                hist_qcd.Add(hist_ttnh_2tag,-1)
                hist_qcd.Add(hist_tth_2tag,-1)
                hist_qcd.Scale(sfs[sfid]['qcd'])
 
                hist_qcd = makePositive_hist(hist_qcd)
                if reg.find('CRw') == -1:
                    hist_tth_4tag = makePositive_hist(hist_tth_4tag)
                    hist_ttnh_4tag = makePositive_hist(hist_ttnh_4tag)

                if reg.find('CRw') == -1:
                    hist_qcd.Write("qcd_hh"+reg)
                if reg.find('Ht') == -1:
                    hist_tth_4tag.Write("allhad_hh"+reg)
                    hist_ttnh_4tag.Write("nonallhad_hh"+reg)
                
                if doShapeSysts:
                    store_for_symm[reg] = hist_qcd.Clone()
                    store_for_add[reg] = hist_qcd.Clone()

            if method == 'BDT':
                BDT_weight_label = 'BDT_d24_weight'
                if reg.find('CRw') != -1:
                    BDT_weight_label = 'BDT_d24_weight_CRderiv'

                hist_qcd = Hist(n_bins, xlim[0], xlim[1], title='QCD', legendstyle='F',drawstyle='hist')
                hist_qcd.fill_array(arrs['dat2'+reg][column], weights=arrs['dat2'+reg][BDT_weight_label])
                hist_qcd.Scale(sfs[sfid]['BDT_norm'])
            
                hist_qcd = makePositive_hist(hist_qcd)

                if reg.find('CRw') == -1:
                    hist_qcd.Write("BDT_hh"+reg)

                if doShapeSysts:
                    store_for_symm[reg] = hist_qcd.Clone()
                    store_for_add[reg] = hist_qcd.Clone()

        if doShapeSysts:
            if method == 'BDT':
                back_label = 'BDT_hh'
            else:
                back_label = 'qcd_hh'

            store_for_symm['_LowHt'].Scale(2.)
            store_for_symm['_LowHt'].Add(store_for_symm['_LowHtCRw'],-1)
            store_for_add['_LowHtCRi'] = store_for_symm['_LowHt'].Clone()
    
            store_for_symm['_HighHt'].Scale(2.)
            store_for_symm['_HighHt'].Add(store_for_symm['_HighHtCRw'],-1)
            store_for_add['_HighHtCRi'] = store_for_symm['_HighHt'].Clone()
 
            store_for_add['_LowHtCRw'].Add(store_for_add['_HighHt'])
            store_for_add['_HighHtCRw'].Add(store_for_add['_LowHt'])

            store_for_add['_LowHtCRi'].Add(store_for_add['_HighHt'])
            store_for_add['_HighHtCRi'].Add(store_for_add['_LowHt'])

            store_for_add['_LowHtCRw'].Write(back_label+'_LowHtCRw')
            store_for_add['_HighHtCRw'].Write(back_label+'_HighHtCRw')
            store_for_add['_LowHtCRi'].Write(back_label+'_LowHtCRi')
            store_for_add['_HighHtCRi'].Write(back_label+'_HighHtCRi')

        if smnr_file: 
            hist_smnr = Hist(n_bins, xlim[0], xlim[1], title='SMNR', legendstyle='F',drawstyle='hist')
            hist_smnr.fill_array(arrs['smnr4'][column], weights=weights['smnr4'])
            hist_smnr = makePositive_hist(hist_smnr)
            hist_smnr.Write("sm_hh")

        for mass in masses:
            g_label = "g10_M%d_4" % mass
            g_title = "g_hh_m%d_c10" % mass 
            hist_grav = Hist(n_bins, xlim[0], xlim[1], title=g_label, legendstyle='F',drawstyle='hist')
            hist_grav.fill_array(arrs[g_label][column], weights=weights[g_label])
            hist_grav = makePositive_hist(hist_grav)
            hist_grav.Write(g_title)

        for mass in scalar_masses:
            s_label = "scalar_M%d_4" % mass
            s_title = "s_hh_m%d" % mass
            hist_scalar = Hist(n_bins, xlim[0], xlim[1], title=s_label, legendstyle='F',drawstyle='hist')
            hist_scalar.fill_array(arrs[s_label][column], weights=weights[s_label])
            hist_scalar = makePositive_hist(hist_scalar)
            hist_scalar.Write(s_title)


    lim_file.Close()
