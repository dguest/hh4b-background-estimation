# Pythonic reweighting for resolved HH->4b

Reweighting package using root\_numpy, Python 3 for the resolved HH->4b analysis. Written to be flexible for rel 20 (privately produced) and
rel 21 (centrally produced) nano n-tuples.

To run for rel 20.7, n-tuples are located at:
`/afs/cern.ch/work/s/sgasioro/public/hh4b_res/trees/rel20/`

The script for spline reweighting is `reweight.py`, which depends on a variety of things in `rw_utils.py`. Comments should hopefully be descriptive, but
some further details/an overview are given in `RW Demo.ipynb`. Plotting still needs a bit of work, but is done with ROOT dependence in
`make_rw_plots.py` or `RW Plotting.ipynb`.

Environment setup for lxplus is python3 LCG 94:

``` console
$ source /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3/x86_64-centos7-gcc7-opt/setup.sh
```

A sample reweighting run is as follows, where we specify input files (which by default are located in the input directory, if -i is specified)

``` console
$ ./reweight.py -i /afs/cern.ch/work/s/sgasioro/public/hh4b_res/trees/rel21/ -d data16_rel21.root -t ttbar_allhad_rel21.root -n ttbar_nonallhad_rel21.root -y 2016 -r 21
``` 

Newly added is the option to choose which reweighting tree to run on (sideband by default). This allows for the derivation of weights in the control region,
which appends a label `_CRderiv` (needed for systematics).

Full suite of options are
``` console
$ ./reweight.py -h
Usage: reweight.py [options]

Options:
  -h, --help            show this help message and exit
  -i INPUT_DIR, --input_dir=INPUT_DIR
                        Input directory
  -d DATA_FILE, --data=DATA_FILE
                        Input data filename
  -t TTH_FILE, --tth=TTH_FILE
                        Input all had ttbar filename
  -n TNH_FILE, --tnh=TNH_FILE
                        Input non all-had ttbar filename
  -o OUTPUT_DIR, --output_dir=OUTPUT_DIR
                        Output directory
  -y YEAR, --year=YEAR  Year
  -r REL, --release=REL
                        Release 20 or 21
  --no-fit              Turn off fit for rel 21
  -f, --full            Turn on write variables - otherwise, just
                        weights/event_number
  --reweight-tree=REWEIGHT_TREE
                        Tree on which to run reweighting.
  --not-all-trees       Turn off calculating weight for all trees
  -l LABEL, --label=LABEL
                        Label for output file (e.g., pflow)
```

Proper handling of triggers between years in MC is used for rel 21 (but not 20) - this relies on the random run numbers (`rand_run_nr`) used in resolved-recon. The code
here only allows for running one year at a time.

BDT reweighting is run very similarly with `BDT_RW.py`, as, e.g.:

``` console
$ ./BDT_RW.py -i /afs/cern.ch/work/s/sgasioro/public/hh4b_res/trees/rel21/ -d data16_rel21.root -t ttbar_allhad_rel21.root -n ttbar_nonallhad_rel21.root -y 2016 -r 21
```

Options are very similar to spline reweighting,

``` console
$ ./BDT_RW.py -h
Usage: BDT_RW.py [options]

Options:
  -h, --help            show this help message and exit
  -i INPUT_DIR, --input_dir=INPUT_DIR
                        Input directory
  -d DATA_FILE, --data=DATA_FILE
                        Input data filename
  -t TTH_FILE, --tth=TTH_FILE
                        Input all had ttbar filename
  -n TNH_FILE, --tnh=TNH_FILE
                        Input non all-had ttbar filename
  -o OUTPUT_DIR, --output_dir=OUTPUT_DIR
                        Output directory
  -y YEAR, --year=YEAR  Year
  -r REL, --release=REL
                        Release 20 or 21
  -f, --full            Turn on write variables - otherwise, just
                        weights/event_number
  --not-all-trees       Turn off calculating weight for all trees
  -p PARAMS, --parameters=PARAMS
                        Read in parameters for BDT optimization. Feed in
                        columns as a comma sep list or use a YAML file
                        (recommended)
  -s, --save            Turn on save BDT (pickle)
  --reweight-tree=REWEIGHT_TREE
                        Tree on which to run reweighting.
  --bootstrap=N_RESAMPLES
                        Turn on bootstrapping. Put in number of resamples.
  --job=JOB             Condor job number (to be used for bootstrap only)
  --n_jobs=N_JOBS       Total number of jobs (to be used for bootstrap only)
  -l LABEL, --label=LABEL
                        Label for output file (e.g., pflow)
```

but we also allow for the reading of BDT parameters (input variables, e.g.) with a YAML file. Bootstrap uncertainties can now also be derived using a Poisson bootstrap
method, and the total number of resamples can be split into jobs with the appropriate arguments.

The BDT method is completely data driven, and thus does not require inputs for ttbar files. It depends on the GBReweighter class from [hep\_ml](https://github.com/arogozhnikov/hep_ml). 

Both scripts mimic the structure of the input file, and write out only the weights, `event_number`, and `run_number`  for each event and each tree. 
For BDT reweighting, a TParameter with the overall normalization to be applied is also written out. For the spline reweighting, a separate file is written 
with the pseudo-tag factor and a histogram with each scale factor/error.
 
All of this can be combined in a post-processing step with `combine_weights.py`, which uses PyROOT and root\_numpy to 
add new branches with the weights to the existing input tree. Note that this combination is done by `event_number`, `run_number` pair, and is a fresh fill of these extra
branches (i.e., if you only run with 2015, you will only put those weights in, and 2016 weights will all be 1). This adds as well the BDT normalization, and repackages all of
the info from the spline iterations into one file.

This can be run as, e.g., 
``` console
$ ./combine_weights.py -i /afs/cern.ch/work/s/sgasioro/public/hh4b_res/trees/rel21/ -o with_weights_gradMuon -d data16_rel21.root -t ttbar_allhad_rel21.root -n ttbar_nonallhad_rel21.root -y 2016 -r 21 -s 7 
```
with options
``` console
$ ./combine_weights.py -h
Usage: combine_weights.py [options]

Options:
  -h, --help            show this help message and exit
  -i INPUT_DIR, --input_dir=INPUT_DIR
                        Input directory
  -d DATA_FILE, --data=DATA_FILE
                        Input data filename
  -t TTH_FILE, --tth=TTH_FILE
                        Input all had ttbar filename
  -n TNH_FILE, --tnh=TNH_FILE
                        Input non all-had ttbar filename
  -w WEIGHT_DIR, --weight_dir=WEIGHT_DIR
                        Directory with weight files
  -b, --BDT             Turn on storage of BDT weight
  -s SPLINE, --spline=SPLINE
                        Turn on storage of spline weight, give number of
                        iters. If default (=-1) don't store.
  -y YEAR, --year=YEAR  Year. If more than one, comma separate (2015,2016) -
                        branches written with year subscript. If you want to
                        do inclusive, pass option all.
  -o OUTPUT_STR, --output_str=OUTPUT_STR
                        Writes tree to file with argument as suffix. Use
                        --append to write onto input file.
  -a, --append          Turn on write to input file
  -r REL, --release=REL
                        Release 20 or 21
  -l LABEL, --label=LABEL
                        Label for output file (e.g., pflow)
  --isCRderiv           Adjust labeling for CR derived weights
  --bootstrap=N_RESAMPLES
                        Turn on bootstrapping. Put in number of resamples.
  --n_jobs=N_JOBS       Total number of jobs (to be used for bootstrap only)
  --BDTttbar            Turn on BDT weight for ttbar
```

which requires the `--isCRderiv` option to combine control region derived weights, and the `--bootstrap` option to combine the bootstrap derived weights.

Plotting of the variables used for reweighting (or whatever else, with some edits) can be done using, e.g.,
``` console
$ ./make_rw_plots.py -i /afs/cern.ch/work/s/sgasioro/public/hh4b_res/trees/rel21/ -d data16_rel21_with_weights_gradMuon.root -t ttbar_allhad_rel21_with_weights_gradMuon.root -n ttbar_nonallhad_rel21_with_weights_gradMuon.root -y 2016 -r 21 -s 6 -l gradMuon
```
where options are
``` console
$ ./make_rw_plots.py -h
Usage: make_rw_plots.py [options]

Options:
  -h, --help            show this help message and exit
  -i INPUT_DIR, --input_dir=INPUT_DIR
                        Input directory
  -o OUTPUT_DIR, --output_dir=OUTPUT_DIR
                        Output directory
  -d DATA_FILE, --data=DATA_FILE
                        Input data filename
  -t TTH_FILE, --tth=TTH_FILE
                        Input all had ttbar filename
  -n TNH_FILE, --tnh=TNH_FILE
                        Input non all-had ttbar filename
  -y YEAR, --year=YEAR  Year
  -r REL, --release=REL
                        Release 20 or 21
  -b, --BDT             Do plots with BDT
  -s SPLINE, --spline=SPLINE
                        Turn spline plot, give number of the iteration. If
                        default (=-1) don't plot.
  -l LABEL, --label=LABEL
                        Label for output
```

An example rel 21 sequence is:

``` console
$ ./reweight.py -i /afs/cern.ch/work/s/sgasioro/public/hh4b_res/trees/rel21/ -o /afs/cern.ch/work/s/sgasioro/public/hh4b_res/trees/rel21/ -d data16_rel21.root -t ttbar_allhad_rel21.root -n ttbar_nonallhad_rel21.root -y 2016 -r 21
$ ./combine_weights.py -i /afs/cern.ch/work/s/sgasioro/public/hh4b_res/trees/rel21/ -o with_weights_gradMuon -d data16_rel21.root -t ttbar_allhad_rel21.root -n ttbar_nonallhad_rel21.root -y 2016 -r 21 -s 7
$ ./make_rw_plots.py -i /afs/cern.ch/work/s/sgasioro/public/hh4b_res/trees/rel21/ -d data16_rel21_with_weights_gradMuon.root -t ttbar_allhad_rel21_with_weights_gradMuon.root -n ttbar_nonallhad_rel21_with_weights_gradMuon.root -y 2016 -r 21 -s 6 -l gradMuon
```

which makes the spline weights for 2016, combines them with the suffix `with_weights_gradMuon`, and plots the reweighting variables/number of additional jets with suffix `weights_gradMuon`.

Making histograms for limit inputs can be run as, e.g.,
``` console
$ ./make_limit_inputs.py -i /afs/cern.ch/work/s/sgasioro/public/hh4b_res/trees/rel21/ -d data16_rel21_with_weights_gradMuon.root -t ttbar_allhad_rel21_with_weights_gradMuon.root -n ttbar_nonallhad_rel21_with_weights_gradMuon.root --smnr SMNR_rel21.root --graviton g10_MXXX_rel21.root -r 21 -s 6 -y 2016
```

with options
``` console
$ ./make_limit_inputs.py -h
Usage: make_limit_inputs.py [options]

Options:
  -h, --help            show this help message and exit
  -i INPUT_DIR, --input_dir=INPUT_DIR
                        Input directory
  -o OUTPUT_DIR, --output_dir=OUTPUT_DIR
                        Output directory
  -d DATA_FILE, --data=DATA_FILE
                        Input data filename
  -t TTH_FILE, --tth=TTH_FILE
                        Input all had ttbar filename
  -n TNH_FILE, --tnh=TNH_FILE
                        Input non all-had ttbar filename
  -y YEAR, --year=YEAR  Year
  --graviton=GRAVITON   Input graviton signal file template. Should have mass
                        as mXXX
  --smnr=SMNR           Input SMNR signal file
  --signal-list=SIGNAL_LIST
                        Input text file with signal samples
  -r REL, --release=REL
                        Release 20 or 21
  -b, --BDT             Do plots with BDT
  --HCrescale           Rescale HC 4 vecs to have m=125 GeV
  -s SPLINE, --spline=SPLINE
                        Turn on spline plot, give number of the iteration. If
                        default (=-1) don't plot.
  -l LABEL, --label=LABEL
                        Label for output
  --run-shape-systs     Include multijet shape variations in output
```

Multijet shape variations can also be run here, and scalars should be run with the `--signal-list` option (see `filelists/` for the current central production).

A new class has also been added to make histograms for both the bootstrap errors and the shape systematics in `error_handling.py`. An example usage is shown in
`make_error_hists.py`.


Making the histograms for the signal systematics is a bit in development, but takes in a list of directories for each signal sample, grabbing systematics from all
variation files included there. 
``` console
$ ./make_signal_syst_hists.py -h
Usage: make_signal_syst_hists.py [options]

Options:
  -h, --help            show this help message and exit
  -i INPUT_DIR, --input_dir=INPUT_DIR
                        Input directory
  -o OUTPUT_DIR, --output_dir=OUTPUT_DIR
                        Output directory
  -y YEAR, --year=YEAR  Year
  --signal-list=SIGNAL_LIST
                        Input text file with signal sample dirs
  -r REL, --release=REL
                        Release 20 or 21
  --HCrescale           Rescale HC 4 vecs to have m=125 GeV
  -l LABEL, --label=LABEL
                        Label for output
```

A variety of other scripts are included to help with quick plotting (`plot_limits.py`, `check_shape_systs.py`, `check_limit_hists.py`, e.g.). For 2017/18, it is
often necessary to run on Condor. A script is included to help make the configuration/executable files:

``` console
$ ./condor/condor_submission.py -h
Usage: condor_submission.py [options]

Options:
  -h, --help            show this help message and exit
  -i INPUT_DIR, --input_dir=INPUT_DIR
                        Input directory
  -d DATA_FILE, --data=DATA_FILE
                        Data file
  -t TTH_FILE, --tth=TTH_FILE
                        Input all had ttbar filename
  -n TTNH_FILE, --tnh=TTNH_FILE
                        Input non all-had ttbar filename
  -o OUTPUT_DIR, --output-dir=OUTPUT_DIR
                        Directory to send output
  -y YEAR_IN, --year=YEAR_IN
                        Years to submit
  -l LABEL, --label=LABEL
                        Label (pflow, e.g.)
  -b, --bdt             Turn on run for BDT reweighting
  -s SPLINE, --spline=SPLINE
                        Turn on run for spline reweighting, give number of
                        iters
  --doCRw               Derive control region weights
  --submit              Submit to Condor

```

which runs the reweighting and combination steps.
