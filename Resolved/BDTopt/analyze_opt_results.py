#!/usr/bin/env python3

import yaml
import operator

import numpy as np
import matplotlib.pyplot as plt

from optparse import OptionParser

parser = OptionParser()
parser.add_option("-i", "--input_aucs", dest="input_aucs", default="",
                  help = "File with config id's and AUCs")
parser.add_option("-p", "--param_dir", dest="param_dir", default="",
                  help = "Directory with config files")

(options, args) = parser.parse_args()

input_aucs = options.input_aucs
param_dir = options.param_dir

if param_dir:
    if param_dir[-1] != '/':
        param_dir+='/'

auc_dict = {}
with open(input_aucs, 'r') as file:
    for line in file:
        fid = line[:17].strip()
        auc = float(line[17:].strip())
        auc_dict[fid] = auc

sorted_auc_dict = sorted(auc_dict.items(), key=lambda tup: abs(tup[1]-0.5))

best = sorted_auc_dict[:int(len(sorted_auc_dict)*0.1)]

best_dict = {}
for key,auc in best:
    best_dict[key] = {}
    best_dict[key]['auc'] = auc
    in_dir = param_dir
    with open((in_dir+key+'.yaml'), 'r') as stream:
        best_dict[key]['columns'] = yaml.load(stream)['columns']

all_columns = ['njets', 'm_hh', 'm_h1', 'E_h1', 'pT_h1', 'eta_h1', 'phi_h1', 'm_h2', 'E_h2', 'pT_h2', 'eta_h2', 'phi_h2',
               'm_h1_j1', 'E_h1_j1', 'pT_h1_j1', 'eta_h1_j1', 'phi_h1_j1', 'm_h1_j2', 'E_h1_j2', 'pT_h1_j2', 'eta_h1_j2',
               'phi_h1_j2', 'm_h2_j1', 'E_h2_j1', 'pT_h2_j1', 'eta_h2_j1', 'phi_h2_j1', 'm_h2_j2', 'E_h2_j2', 'pT_h2_j2',
               'eta_h2_j2', 'phi_h2_j2', 'pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2', 'X_wt']

col_counts = []
for column in all_columns:
    count = 0
    for key in best_dict.keys():
        count+=(column in best_dict[key]['columns'])
    col_counts.append(count)

keys = list(best_dict.keys())
min_auc = best_dict[keys[0]]['auc']
max_auc = best_dict[keys[-1]]['auc']

best_col_idxs = []
for column in best_dict[keys[0]]['columns']:
    for i in range(len(all_columns)):
        if column == all_columns[i]:
            best_col_idxs.append(i+1)
            
            
lengths = []
for key in best_dict.keys():
    lengths.append(len(best_dict[key]['columns']))

best_min_set = []
min_set_cols = best_dict[list(best_dict.keys())[np.argmin(lengths)]]['columns']
min_set_auc = best_dict[list(best_dict.keys())[np.argmin(lengths)]]['auc']
for column in min_set_cols:
    for i in range(len(all_columns)):
        if column == all_columns[i]:
            best_min_set.append(i+1)

high_val = sorted(list(zip(col_counts, all_columns)), 
                  key=lambda tup: tup[0], reverse=True)[:15]

plt.figure(figsize=(14,10))
plt.bar(np.arange(1,len(high_val)+1), [tup[0] for tup in high_val])
plt.xticks(np.arange(1,len(high_val)+1), [tup[1] for tup in high_val], 
           rotation=90, fontsize = 35)
xcoord=plt.xlim()[1]-plt.xlim()[0]
ycoord=plt.ylim()[1]-plt.ylim()[0]
plt.text(xcoord*0.20,ycoord*0.9, 'Best %d from random sample of %d input sets' % 
         (len(best_dict)-1, len(sorted_auc_dict)-1), fontsize=25)
plt.text(xcoord*0.27,ycoord*0.85, 'Sel. AUCs from %.3f to %.3f (nominal)' % 
         (min_auc, max_auc), fontsize=25)
plt.text(xcoord*0.35,ycoord*0.8, '%d Most Frequent Variables' % 
         (len(high_val)), fontsize=25)
plt.ylabel('Frequency of Appearance', fontsize=35)
plt.tight_layout()
plt.show()
