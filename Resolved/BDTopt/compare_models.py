#!/usr/bin/env python3

import numpy as np
from numpy.lib.recfunctions import append_fields
import scipy.optimize as opt
from scipy.interpolate import interp1d
from scipy import ndimage
import pickle

from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score

from pandas import DataFrame 
from os import listdir

import sys
sys.path.insert(0, 'hep_ml')
sys.path.insert(0, '../../hep_ml')
sys.path.insert(0, '..')

from hep_ml import reweight

from rw_utils import *

import yaml
from optparse import OptionParser


parser = OptionParser()
parser.add_option("-i", "--input_dir", dest="input_dir", default="",
                  help="Input directory")
parser.add_option("-d", "--data", dest="data_file", default="",
                  help="Input data filename")
parser.add_option("-y", "--year", dest="year", default="2015",
                  help="Year")

parser.add_option("-r", "--release", dest="rel", default="20",
                  help="Release 20 or 21")

parser.add_option("-p", "--param_dir", dest="param_dir", default="",
                  help = "Directory containing param files to compare")

parser.add_option("-s", "--save",
                  action="store_true", dest="save", default=False,
                  help="Turn on save BDT (pickle)")


(options, args) = parser.parse_args()

import root_numpy

year_in = options.year
input_dir = options.input_dir
data_file = options.data_file
rel = int(options.rel[:2])

param_dir = options.param_dir
save = options.save

if rel != 20 and rel !=21:
    rel = 20
    print("Invalid option, defaulting to rel 20")

year =''
yr_short=''
if len(year_in) == 4:
    year=year_in
    yr_short=year_in[2:]
elif len(year_in) == 2:
    year='20'+year_in
    yr_short=year_in
else:
    print("Invalid year format, defaulting to 2015")
    year='2015'
    yr_short='15'

if input_dir:
    if input_dir[-1] != '/': input_dir+='/'

param_file_list = []
if param_dir:
    if param_dir.find('.yml') != -1 or param_dir.find('.yaml') != -1:
        param_file_list.append(param_dir)
        param_dir = ''
    else:
        if param_dir[-1] != '/': param_dir+='/'
        param_file_list = listdir(param_dir)
else:
    param_file_list = ['high_freq_vars.yaml', 'template.yaml', 'best_min_vars.yaml']

if data_file:
    if data_file[0] == '/':
        input_dir = ''
    data_file = input_dir+data_file
    if data_file[-4:] != 'root':
        print("Warning! Data file not a root file")
    if data_file.find(yr_short) == -1:
        print("Warning! Input data file name may not match year.")
else:
    data_file = input_dir+'output_data'+yr_short+'.root'

print("Input data file:", data_file)
print("Year:", year)


#Set up appropriate columns for data, mc, and rw. Data/MC slightly different due to some weights
columns = ['n_jets', 'event.m_4j', 'event.m_h1', 'event.E_h1', 'event.pT_h1', 'event.eta_h1', 'event.phi_h1', 'event.m_h2',
           'event.E_h2', 'event.pT_h2', 'event.eta_h2', 'event.phi_h2', 'event.m_h1_j1', 'event.E_h1_j1', 'event.pT_h1_j1', 
           'event.eta_h1_j1', 'event.phi_h1_j1', 'event.m_h1_j2', 'event.E_h1_j2', 'event.pT_h1_j2', 'event.eta_h1_j2',
           'event.phi_h1_j2', 'event.m_h2_j1', 'event.E_h2_j1', 'event.pT_h2_j1', 'event.eta_h2_j1', 'event.phi_h2_j1', 
           'event.m_h2_j2', 'event.E_h2_j2', 'event.pT_h2_j2', 'event.eta_h2_j2', 'event.phi_h2_j2', 'rw_vals.pT_4', 
           'rw_vals.pT_2', 'rw_vals.ave_eta', 'rw_vals.dRjj_close', 'rw_vals.dRjj_notclose', 'event_number', 'n_tag',
           'passTrig_'+yr_short, 'Xwt']

if rel == 21:
    columns = ['njets', 'm_hh', 'm_h1', 'E_h1', 'pT_h1', 'eta_h1', 'phi_h1', 'm_h2', 'E_h2', 'pT_h2', 'eta_h2', 'phi_h2',
               'm_h1_j1', 'E_h1_j1', 'pT_h1_j1', 'eta_h1_j1', 'phi_h1_j1', 'm_h1_j2', 'E_h1_j2', 'pT_h1_j2', 'eta_h1_j2',
               'phi_h1_j2', 'm_h2_j1', 'E_h2_j1', 'pT_h2_j1', 'eta_h2_j1', 'phi_h2_j1', 'm_h2_j2', 'E_h2_j2', 'pT_h2_j2',
               'eta_h2_j2', 'phi_h2_j2', 'pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2', 'run_number', 'event_number', 'ntag', 'X_wt']


print("About to load in data...")
full_data = root_numpy.root2array(data_file,
                             treename='fullmassplane', branches=columns)

#Adjust naming to match (see rw_utils for mapping)
if rel == 20:
    rel20to21 = rel20to21_dict()

    columns = [rel20to21[key] for key in full_data.dtype.names]

    full_data.dtype.names = tuple(columns)

print("Loaded! Setting up for training...")

if rel == 21:
    #Grab appropriate events for each year's triggers. Run numbers taken from resolved-recon's trigger list
    run_bounds = { '2015' : [0, 296939], '2016' : [296939, 324320],
                   '2017' : [324320, 348197], '2018' : [348197, 364486] }
    year_run_range = run_bounds[year]

    full_data = full_data[(full_data['run_number'] > year_run_range[0]) & (full_data['run_number'] < year_run_range[1])]

    print('For year', year, 'selected runs between', year_run_range[0], 'and', year_run_range[1])


arrs = {}
data = SBsel(full_data)
if rel == 20:
    data = data[(data['passTrig_'+yr_short]==True)]
arrs['dat2']= data[data['ntag'] == 2]
arrs['dat4']= data[data['ntag'] >= 4]

orig_all = DataFrame(arrs['dat2'][columns[:-3]])
target_all = DataFrame(arrs['dat4'][columns[:-3]])

#Defaults
first = True
orig_score = 0.

param_labels = []
weight_arrs = {}
aucs = {}
for param_file in param_file_list:
    BDT_params = {}
    print("Loading in BDT parameters from file", param_file)
    param_label = param_file[:param_file.find('.y')]
    param_labels.append(param_label)

    with open(param_dir+param_file, 'r') as stream:
        BDT_params = yaml.load(stream)
    print("Loaded in params:")
    for key in BDT_params.keys():
        print(key+':', BDT_params[key])

    original = DataFrame(arrs['dat2'][BDT_params['columns']])
    target = DataFrame(arrs['dat4'][BDT_params['columns']])

    original_weights = np.ones(len(arrs['dat2']))

    reweighter = reweight.GBReweighter(n_estimators=BDT_params['n_estimators'], learning_rate=BDT_params['learning_rate'], max_depth=BDT_params['max_depth'], 
                                       min_samples_leaf=BDT_params['min_samples_leaf'], 
                                       gb_args=BDT_params['gb_args'])

    print("About to train...")
    reweighter.fit(original, target)

    data = np.concatenate([orig_all, target_all])
    labels = np.array([0] * len(orig_all) + [1] * len(target_all))

    weights = {}
    if first:
        weights['original'] = original_weights
    weights['gb_weights'] = reweighter.predict_weights(original)

    weight_arrs[param_label] = weights['gb_weights'].copy()

#    print("About to compare...")
#    for name, new_weights in weights.items():
#        W = np.concatenate([new_weights / new_weights.sum() * len(target_all), [1] * len(target_all)])
#        Xtr, Xts, Ytr, Yts, Wtr, Wts = train_test_split(data, labels, W, random_state=42, train_size=0.51, test_size=0.49)
#        clf = GradientBoostingClassifier(subsample=0.3, n_estimators=50).fit(Xtr, Ytr, sample_weight=Wtr)
#    
#        score = roc_auc_score(Yts, clf.predict_proba(Xts)[:, 1], sample_weight=Wts)
#        if first:
#            if name == 'original':
#                orig_score = score
#            print(name, score)
#        else:
#            print('original', orig_score)
#            print(name, score)
#
#        if name == 'gb_weights':
#            aucs[param_label] = score
#    first = False


from rootpy.plotting import *
import ROOT
from hep_ml.metrics_utils import ks_2samp_weighted

titles = {'pT_4': 'HC Jet 4 p_{T} [GeV]', 'pT_2': 'HC Jet 2 p_{T} [GeV]', 
          'eta_i' : '<|HC Jet #eta|>', 'dRjj_1': '#Delta R_{jj} Close',
          'dRjj_2': '#Delta R_{jj} Not Close', 'njets':'# of additional jets',
          'X_wt':'X_{Wt}', 'm_hh' : 'm_{HH}'}

legend_titles = {'best_min_vars': 'Best Minimal Set', 'high_freq_vars':'High frequency set',
                 'template': 'Spline variables', 'best_min_vars_16': 'Best Minimal Set', 'high_freq_vars_16':'High frequency set',
                 'template_16': 'Spline variables'}
hists = {}
ks_vals = {}
chi2_vals = {}
#for column in columns_for_rw:
for column in ['X_wt', 'm_hh']:
    hists[column] = {}
    ks_vals[column] = {}
    chi2_vals[column] = {}
    
    #xlim = np.percentile(np.hstack([arrs['dat4'][column]]), [0.01, 99.99])
    if column == 'X_wt':
        xlim = [1.5, 8.57]
    else:
        xlim = [180, 1180]
    n_bins = 100
    
    hist_dat_4tag = Hist(n_bins, xlim[0], xlim[1], title='Data', legendstyle='PE')
    hist_dat_4tag.fill_array(arrs['dat4'][column])
   
    reg_dict = whichRegions(full_data) 
    for key in param_labels:
        isSB = (reg_dict["SB"]==1) & (reg_dict["CR"]==0) & (reg_dict["SR"]==0)
        hist_qcd = Hist(n_bins, xlim[0], xlim[1], title='QCD', legendstyle='F',drawstyle='hist')
        hist_qcd.fill_array(arrs['dat2'][column], weights=weight_arrs[key])
        hist_qcd.Scale(hist_dat_4tag.Integral()/hist_qcd.Integral())

        ks = hist_dat_4tag.KolmogorovTest(hist_qcd)
        chi2 = hist_dat_4tag.Chi2Test(hist_qcd, "UW CHI2/NDF")

        print("KS value (1 is more compatible):", ks)
        print("Chi2/NDF (1 is more compatible):", chi2)

        hists[column][key] = hist_qcd
        ks_vals[column][key] = ks
        chi2_vals[column][key] = chi2

    
    hist_dat_4tag.SetMarkerStyle(20)
    hist_dat_4tag.SetMarkerSize(0.6)
    hist_dat_4tag.SetMarkerColor(1)
    hist_dat_4tag.SetLineColor(1)
        
    canvas = Canvas()
    canvas.cd()
    pad1 = Pad(0.0,0.33,1.0,1.0)
    pad2 = Pad(0.0,0.0,1.0,0.33)
    pad1.SetBottomMargin(0.015)
    pad1.SetTopMargin(0.05)
    pad2.SetTopMargin(0.05)
    pad2.SetBottomMargin(0.3)
    pad1.Draw()
    pad2.Draw()
    pad1.SetTicks(1, 1)

    pad1.cd()
    ROOT.gStyle.SetOptStat(0)
  
    colors = [ROOT.kGreen+1, ROOT.kCyan+1, ROOT.kBlue]
    tot_hists = []
    for i in range(len(param_labels)):
        key=param_labels[i]
        tot_hist= hists[column][key]
        if key in legend_titles.keys():
            tot_hist.SetTitle(legend_titles[key])#+', AUC = %.2f, KS = %.2f, chi2/NDF = %.2f' % (aucs[key], ks_vals[column][key], chi2_vals[column][key]))
        else:
            tot_hist.SetTitle(key)
        tot_hist.SetMarkerStyle(20)
        tot_hist.SetMarkerSize(0.6)
        tot_hist.SetMarkerColor(colors[i])
        tot_hist.SetLineColor(colors[i])
        tot_hists.append(tot_hist)


    legend = Legend([hist_dat_4tag]+tot_hists, textsize=0.11, leftmargin=0.27, entrysep=0.1)
    hist_dat_4tag.GetXaxis().SetLabelSize(0)
    hist_dat_4tag.GetYaxis().SetLabelSize(0.038)
    hist_dat_4tag.SetTitle("")
    hist_dat_4tag.Draw()
    
    for hist in tot_hists:
        hist.SetTitle("")
        hist.Draw("same pe")

    legend.SetHeader('BDT Reweight, '+year+' SB')

    legend.SetBorderSize(0)
    legend.SetFillStyle(0)
    legend.Draw()

    pad2.cd()
    ROOT.gStyle.SetOptStat(0)
    for i in range(len(param_labels)):
        key=param_labels[i]
        rat_dat_pred = hist_dat_4tag.Clone("rat_dat_pred")
        rat_dat_pred.Divide(tot_hists[i])
        rat_dat_pred.GetYaxis().SetRangeUser(0, 2)
        rat_dat_pred.GetYaxis().SetNdivisions(503)
        rat_dat_pred.GetXaxis().SetLabelSize(0.1)
        rat_dat_pred.GetYaxis().SetLabelSize(0.1)
        rat_dat_pred.GetYaxis().SetLabelOffset(0.03)
        rat_dat_pred.GetYaxis().SetTitle("Data / Bkgd")
        rat_dat_pred.GetYaxis().CenterTitle()
        rat_dat_pred.GetXaxis().SetTitle(titles[column])
        rat_dat_pred.SetTitle("")

        rat_dat_pred.GetYaxis().SetTitleSize(0.12)
        #rat_dat_pred.GetXaxis().SetTitleSize(0.1)
        rat_dat_pred.GetXaxis().SetTitleSize(0.15)
        rat_dat_pred.GetYaxis().SetTitleOffset(0.35)
        #rat_dat_pred.GetXaxis().SetTitleOffset(1.1)
        rat_dat_pred.GetXaxis().SetTitleOffset(0.8)

        rat_dat_pred.SetMarkerStyle(20)
        rat_dat_pred.SetMarkerSize(0.6)
        rat_dat_pred.SetMarkerColor(colors[i])
        rat_dat_pred.SetLineColor(colors[i])
        rat_dat_pred.Draw("same pe")

    line = ROOT.TLine(rat_dat_pred.GetXaxis().GetXmin(), 1, rat_dat_pred.GetXaxis().GetXmax(), 1)
    line.SetLineColor(1)
    line.SetLineWidth(1)
    line.SetLineStyle(1)
    line.Draw()

    canvas.SaveAs('BDT_RW_input_set_comparison_'+column+'_'+yr_short+'.pdf')
    canvas.SaveAs('BDT_RW_input_set_comparison_'+column+'_'+yr_short+'.root')
    canvas.Draw()
