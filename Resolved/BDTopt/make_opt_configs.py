#!/usr/bin/env python3

import yaml
import io
import numpy as np


all_columns = np.array(['njets', 'm_hh', 'm_h1', 'E_h1', 'pT_h1', 'eta_h1', 'phi_h1', 'm_h2', 'E_h2', 'pT_h2', 'eta_h2', 'phi_h2',
               'm_h1_j1', 'E_h1_j1', 'pT_h1_j1', 'eta_h1_j1', 'phi_h1_j1', 'm_h1_j2', 'E_h1_j2', 'pT_h1_j2', 'eta_h1_j2', 
               'phi_h1_j2', 'm_h2_j1', 'E_h2_j1', 'pT_h2_j1', 'eta_h2_j1', 'phi_h2_j1', 'm_h2_j2', 'E_h2_j2', 'pT_h2_j2',
               'eta_h2_j2', 'phi_h2_j2', 'pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2', 'X_wt'])


for i in range(5000):
     columns = all_columns[np.random.choice([0,1], size=len(all_columns))==1].tolist()
     #columns = np.random.choice(all_columns[1:], size=5, replace=False).tolist()+['njets']

     # Define data
     data = {'columns': columns,
             'n_estimators': 50,
             'learning_rate': 0.1,
             'max_depth': 3, 
             'min_samples_leaf': 125, 
             'gb_args': {'subsample': 0.4}}

     config_id = '210519%d' % i
     # Write YAML file
     with io.open('configs/210519/sweep_'+config_id+'.yaml', 'w+', encoding='utf8') as outfile:
         yaml.dump(data, outfile, default_flow_style=False, allow_unicode=True)
