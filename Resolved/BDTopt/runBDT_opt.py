#!/usr/bin/env python3

import numpy as np
from numpy.lib.recfunctions import append_fields
import scipy.optimize as opt
from scipy.interpolate import interp1d
from scipy import ndimage
import pickle

from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score

from pandas import DataFrame 
from os import listdir

import sys
sys.path.insert(0, 'hep_ml')
sys.path.insert(0, '../../hep_ml')
sys.path.insert(0, '..')

from hep_ml import reweight

from rw_utils import *

import yaml
from optparse import OptionParser


parser = OptionParser()
parser.add_option("-i", "--input_dir", dest="input_dir", default="",
                  help="Input directory")
parser.add_option("-o", "--out_name", dest="out_name", default="",
                  help="Output AUC file name")
parser.add_option("-d", "--data", dest="data_file", default="",
                  help="Input data filename")
parser.add_option("-y", "--year", dest="year", default="2015",
                  help="Year")

parser.add_option("-j", "--job", dest="job", default ="",
                  help="Condor job number")

parser.add_option("-n", "--n_jobs", dest="n_jobs", default="",
                  help="Total number of jobs")

parser.add_option("-r", "--release", dest="rel", default="20",
                  help="Release 20 or 21")

parser.add_option("-p", "--param_dir", dest="param_dir", default="",
                  help = "Read in parameters for BDT optimization. Feed in directory")

parser.add_option("-s", "--save",
                  action="store_true", dest="save", default=False,
                  help="Turn on save BDT (pickle)")


(options, args) = parser.parse_args()

import root_numpy

year_in = options.year
input_dir = options.input_dir
data_file = options.data_file
rel = int(options.rel[:2])

out_name = options.out_name
param_dir = options.param_dir
save = options.save

job = options.job
n_jobs = options.n_jobs

if (job and not n_jobs) or (n_jobs and not job):
    print("Invalid condor info - running everything here")
    job = ""
    n_jobs = ""

if rel != 20 and rel !=21:
    rel = 20
    print("Invalid option, defaulting to rel 20")

year =''
yr_short=''
if len(year_in) == 4:
    year=year_in
    yr_short=year_in[2:]
elif len(year_in) == 2:
    year='20'+year_in
    yr_short=year_in
else:
    print("Invalid year format, defaulting to 2015")
    year='2015'
    yr_short='15'

if input_dir:
    if input_dir[-1] != '/': input_dir+='/'

param_file_list = []
if param_dir:
    if param_dir.find('.yml') != -1 or param_dir.find('.yaml') != -1:
        param_file_list.append(param_dir)
        param_dir = ''
    else:
        if param_dir[-1] != '/': param_dir+='/'
        param_file_list = listdir(param_dir)

print(job, n_jobs)
if job and n_jobs:
    filecount = len(param_file_list)
    job = int(job)
    n_jobs = int(n_jobs)
    n_per_job = int(filecount/n_jobs)
    remainder = filecount % n_jobs

    print("Splitting", filecount, "configs into", n_jobs)

    start = job * (n_per_job + 1)* (job < remainder) + (remainder + job * n_per_job)*(job >= remainder)
    end = (job+1) * (n_per_job + 1)* (job < remainder) + (remainder + (job+1)* n_per_job)*(job >= remainder)

    param_file_list = param_file_list[start:end]

if data_file:
    if data_file[0] == '/':
        input_dir = ''
    data_file = input_dir+data_file
    if data_file[-4:] != 'root':
        print("Warning! Data file not a root file")
    if data_file.find(yr_short) == -1:
        print("Warning! Input data file name may not match year.")
else:
    data_file = input_dir+'output_data'+yr_short+'.root'

if out_name:
    if out_name[-3:] != 'txt':
         print("Warning! Out file not a txt file")
else:
    out_name = 'weighted_aucs.txt'

print("Input data file:", data_file)
print("Year:", year)


#Set up appropriate columns for data, mc, and rw. Data/MC slightly different due to some weights
columns = ['n_jets', 'event.m_4j', 'event.m_h1', 'event.E_h1', 'event.pT_h1', 'event.eta_h1', 'event.phi_h1', 'event.m_h2',
           'event.E_h2', 'event.pT_h2', 'event.eta_h2', 'event.phi_h2', 'event.m_h1_j1', 'event.E_h1_j1', 'event.pT_h1_j1', 
           'event.eta_h1_j1', 'event.phi_h1_j1', 'event.m_h1_j2', 'event.E_h1_j2', 'event.pT_h1_j2', 'event.eta_h1_j2',
           'event.phi_h1_j2', 'event.m_h2_j1', 'event.E_h2_j1', 'event.pT_h2_j1', 'event.eta_h2_j1', 'event.phi_h2_j1', 
           'event.m_h2_j2', 'event.E_h2_j2', 'event.pT_h2_j2', 'event.eta_h2_j2', 'event.phi_h2_j2', 'rw_vals.pT_4', 
           'rw_vals.pT_2', 'rw_vals.ave_eta', 'rw_vals.dRjj_close', 'rw_vals.dRjj_notclose', 'event_number', 'n_tag',
           'passTrig_'+yr_short, 'Xwt']

if rel == 21:
    columns = ['njets', 'm_hh', 'm_h1', 'E_h1', 'pT_h1', 'eta_h1', 'phi_h1', 'm_h2', 'E_h2', 'pT_h2', 'eta_h2', 'phi_h2',
               'm_h1_j1', 'E_h1_j1', 'pT_h1_j1', 'eta_h1_j1', 'phi_h1_j1', 'm_h1_j2', 'E_h1_j2', 'pT_h1_j2', 'eta_h1_j2',
               'phi_h1_j2', 'm_h2_j1', 'E_h2_j1', 'pT_h2_j1', 'eta_h2_j1', 'phi_h2_j1', 'm_h2_j2', 'E_h2_j2', 'pT_h2_j2',
               'eta_h2_j2', 'phi_h2_j2', 'pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2', 'run_number', 'event_number', 'ntag', 'X_wt']


print("About to load in data...")
full_data = root_numpy.root2array(data_file,
                             treename='fullmassplane', branches=columns)

#Adjust naming to match (see rw_utils for mapping)
if rel == 20:
    rel20to21 = rel20to21_dict()

    columns = [rel20to21[key] for key in full_data.dtype.names]

    full_data.dtype.names = tuple(columns)

print("Loaded! Setting up for training...")

if rel == 21:
    #Grab appropriate events for each year's triggers. Run numbers taken from resolved-recon's trigger list
    run_bounds = { '2015' : [0, 296939], '2016' : [296939, 324320],
                   '2017' : [324320, 348197], '2018' : [348197, 364486] }
    year_run_range = run_bounds[year]

    full_data = full_data[(full_data['run_number'] > year_run_range[0]) & (full_data['run_number'] < year_run_range[1])]

    print('For year', year, 'selected runs between', year_run_range[0], 'and', year_run_range[1])


arrs = {}
data = SBsel(full_data)
if rel == 20:
    data = data[(data['passTrig_'+yr_short]==True)]
arrs['dat2']= data[data['ntag'] == 2]
arrs['dat4']= data[data['ntag'] >= 4]

orig_all = DataFrame(arrs['dat2'][columns[:-3]])
target_all = DataFrame(arrs['dat4'][columns[:-3]])

#Defaults
first = True
orig_score = 0.
file_out = open(out_name,'a+')
for param_file in param_file_list:
    BDT_params = {}
    print("Loading in BDT parameters from file", param_file)
    param_label = param_file[:param_file.find('.y')]
    with open(param_dir+param_file, 'r') as stream:
        BDT_params = yaml.load(stream)
    print("Loaded in params:")
    for key in BDT_params.keys():
        print(key+':', BDT_params[key])

    original = DataFrame(arrs['dat2'][BDT_params['columns']])
    target = DataFrame(arrs['dat4'][BDT_params['columns']])

    original_weights = np.ones(len(arrs['dat2']))

    reweighter = reweight.GBReweighter(n_estimators=BDT_params['n_estimators'], learning_rate=BDT_params['learning_rate'], max_depth=BDT_params['max_depth'], 
                                       min_samples_leaf=BDT_params['min_samples_leaf'], 
                                       gb_args=BDT_params['gb_args'])

    print("About to train...")
    reweighter.fit(original, target)

    data = np.concatenate([orig_all, target_all])
    labels = np.array([0] * len(orig_all) + [1] * len(target_all))

    weights = {}
    if first:
        weights['original'] = original_weights
    weights['gb_weights'] = reweighter.predict_weights(original)

    print("About to compare...")
    for name, new_weights in weights.items():
        W = np.concatenate([new_weights / new_weights.sum() * len(target_all), [1] * len(target_all)])
        Xtr, Xts, Ytr, Yts, Wtr, Wts = train_test_split(data, labels, W, random_state=42, train_size=0.51, test_size=0.49)
        clf = GradientBoostingClassifier(subsample=0.3, n_estimators=50).fit(Xtr, Ytr, sample_weight=Wtr)
    
        score = roc_auc_score(Yts, clf.predict_proba(Xts)[:, 1], sample_weight=Wts)
        if first:
            if name == 'original':
                orig_score = score
            print(name, score)
        else:
            print('original', orig_score)
            print(name, score)
        
        if name == 'gb_weights':
            file_out.write(('{:>15}'.format(param_label)+'{:>15}'.format('%.4f' % score)))
            file_out.write('\n')

    first = False

    if save:
        if param_label:
            param_label = '_config_'+param_label
        BDT_fname = ('BDT_d24_'+yr_short+'_rel%d'+param_label+'.p') % rel
        pickle.dump(reweighter, open( BDT_fname, "wb" ))

file_out.close()
