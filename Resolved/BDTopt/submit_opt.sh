#!/bin/bash

echo "We are node $2"

source /cvmfs/sft.cern.ch/lcg/views/LCG_94python3/x86_64-centos7-gcc8-opt/setup.sh

echo "About to unpack hep_ml tarball"
tar -zxf hep_ml.tar.gz
echo "Unpacked tarball"
ls -lth

python runBDT_opt.py -i /afs/cern.ch/work/s/sgasioro/public/hh4b_res/trees/rel21/ -d data16_rel21.root -r 21 -y 16 -p /afs/cern.ch/user/s/sgasioro/private/hh4b-background-estimation/Resolved/BDTopt/configs/210519/ -j $2 -n 100 -o aucs_210519_$2.txt

ls -lth

