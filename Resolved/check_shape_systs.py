#!/usr/bin/env python3

import numpy as np
import scipy.optimize as opt
from scipy.interpolate import interp1d
from scipy import ndimage

from rw_utils import *

from optparse import OptionParser

parser = OptionParser()
parser.add_option("-i", "--limit-input", dest="limit_input", default="",
                  help="Limit input file")
parser.add_option("-l", "--label", dest="label", default="",
                  help="Label for output")

(options, args) = parser.parse_args()

input_file = options.limit_input
label = options.label

if input_file.find('HCrescale') != -1:
    label+="_HCrescale"

if label and label[0] != '_':
    label = '_'+label

from rootpy.plotting import *
from rootpy.io import root_open
import ROOT
ROOT.gROOT.SetBatch(ROOT.kTRUE)

methods = []
if input_file.find('BDT') != -1:
    methods.append('BDT')
if input_file.find('spline') != -1:
    methods.append('spline')

if input_file.find('16') != -1:
    year = "2016"
elif input_file.find('17') != -1:
    year = "2017"
elif input_file.find('18') != -1:
    year = "2018"
else:
    year = "2015"

if input_file.find('rel20') != -1:
    rel_label = ", rel 20"
elif input_file.find('rel21') != -1:
    rel_label = ", rel 21"
else:
    rel_label = ""

f = root_open(input_file)

#BDT_hh
#BDT_hh_LowHt
#BDT_hh_HighHt
#BDT_hh_LowHtCRw
#BDT_hh_HighHtCRw
#BDT_hh_LowHtCRi
#BDT_hh_HighHtCRi
#qcd_hh for spline

for method in methods:
    if method == 'BDT':
        hist_label = 'BDT_hh'
        title_str = 'BDT'
    else:
        hist_label = 'qcd_hh'
        title_str = 'Multijet'

    for reg in ["LowHt", "HighHt"]:
        if reg == "LowHt":
            title_label = "Low"
        else:
            title_label = "High"

        hist_nom = f.Get(hist_label)
        hist_CRw = f.Get(hist_label+"_"+reg+"CRw")
        hist_CRi = f.Get(hist_label+"_"+reg+"CRi")

        hist_nom.SetFillStyle(0)
        hist_nom.SetLineColor(ROOT.kBlack)
        hist_nom.SetLineWidth(2)
        hist_nom.SetTitle("Nominal " + title_str)

        hist_CRw.SetFillStyle(0)
        hist_CRw.SetLineColor(ROOT.kRed)
        hist_CRw.SetLineWidth(2)
        hist_CRw.SetTitle(title_label+" H_{T} CR Weighted")

        hist_CRi.SetFillStyle(0)
        hist_CRi.SetLineColor(ROOT.kBlue)
        hist_CRi.SetLineWidth(2)
        hist_CRi.SetTitle(title_label+" H_{T} CR Inverted")
 
        canvas = Canvas()
        canvas.cd()
        ROOT.gStyle.SetOptStat(0)
        pad1 = Pad(0.0,0.33,1.0,1.0)
        pad2 = Pad(0.0,0.0,1.0,0.33)
        pad1.SetBottomMargin(0.015)
        pad1.SetTopMargin(0.05)
        pad2.SetTopMargin(0.05)
        pad2.SetBottomMargin(0.3)
        pad1.Draw()
        pad2.Draw()
        pad1.SetTicks(1, 1)

        pad1.cd() 
        hist_nom.legendstyle = 'L'
        hist_CRw.legendstyle = 'L'
        hist_CRi.legendstyle = 'L'
        legend = Legend([hist_nom, hist_CRw, hist_CRi], leftmargin=0.25, margin=0.3, entrysep=0.05, textsize=0.07)

        legend.SetHeader(year+" Signal Region"+rel_label)
    
        if input_file.find('HCrescale') != -1:
            x_axis_title = 'm_{HH} (corrected) [GeV]'
        else:
            x_axis_title = 'm_{HH} [GeV]'

        hist_nom.SetTitle("")
        hist_CRw.SetTitle("")
        hist_CRi.SetTitle("")

        max_y = 1600*(year == "2016") + 2000*(year=="2017") + 2000*(year=="2018") 
        hist_nom.GetYaxis().SetRangeUser(0, max_y)
        hist_nom.Draw("hist")
        hist_CRw.Draw("hist same")
        hist_CRi.Draw("hist same")

        hist_nom.GetXaxis().SetLabelSize(0)
        hist_nom.GetYaxis().SetLabelSize(0.038)

        legend.SetBorderSize(0)
        legend.SetFillStyle(0)
        legend.Draw()



        pad2.cd()
        ROOT.gStyle.SetOptStat(0)
        rat_CRwnom = hist_CRw.Clone("rat_CRwnom")
        rat_CRwnom.Divide(hist_nom)

        rat_CRinom = hist_CRi.Clone("rat_CRinom")
        rat_CRinom.Divide(hist_nom)

        if reg == 'LowHt':
            ratrange=[0.94,1.06]
        else:
            ratrange=[0.5,1.5]
        rat_CRwnom.GetYaxis().SetRangeUser(ratrange[0], ratrange[1])
        rat_CRwnom.GetYaxis().SetNdivisions(503)
        rat_CRwnom.GetXaxis().SetLabelSize(0.1)
        rat_CRwnom.GetYaxis().SetLabelSize(0.1)
        rat_CRwnom.GetYaxis().SetLabelOffset(0.03)
        rat_CRwnom.GetYaxis().SetTitle("Var / Nom ")
        rat_CRwnom.GetYaxis().CenterTitle();
        rat_CRwnom.GetXaxis().SetTitle(x_axis_title)
        rat_CRwnom.SetTitle('')

        rat_CRwnom.GetYaxis().SetTitleSize(0.12)
        rat_CRwnom.GetXaxis().SetTitleSize(0.1)
        rat_CRwnom.GetYaxis().SetTitleOffset(0.35)
        rat_CRwnom.GetXaxis().SetTitleOffset(1.1)

        rat_CRwnom.SetMarkerStyle(20)
        rat_CRwnom.SetMarkerSize(0.6)
        rat_CRwnom.SetMarkerColor(ROOT.kRed)
        rat_CRwnom.SetLineColor(ROOT.kRed)
        rat_CRwnom.SetLineStyle(1)
        rat_CRwnom.SetLineWidth(1)
        rat_CRwnom.Draw("pe")
   
        rat_CRinom.SetMarkerStyle(20)
        rat_CRinom.SetMarkerSize(0.6)
        rat_CRinom.SetMarkerColor(ROOT.kBlue)
        rat_CRinom.SetLineColor(ROOT.kBlue)
        rat_CRinom.SetLineStyle(1)
        rat_CRinom.SetLineWidth(1)
        rat_CRinom.Draw("pe same")

        line = ROOT.TLine(rat_CRwnom.GetXaxis().GetXmin(), 1, rat_CRwnom.GetXaxis().GetXmax(), 1)
        line.SetLineColor(1)
        line.SetLineWidth(1)
        line.SetLineStyle(1)
        line.Draw()

        canvas.Draw()

        canvas.SaveAs(input_file[:-5]+label+"_mhh_shapeVar_"+reg+".pdf")

        pad1.cd()
        ROOT.gPad.SetLogy()
        hist_nom.GetYaxis().SetRangeUser(1e-1,max_y)
        canvas.SaveAs(input_file[:-5]+label+"_mhh_shapeVar_"+reg+"_logscale.pdf")
