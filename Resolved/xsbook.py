
def xsLookup(label, rel=20, with_init=True, updated=True):
    xs = 1.
    k = 1.
    filt_eff = 1.
    init_events = 1.

    if label == 'tth':
        xs = 729780. #696210.
        k = 1.13974 #1.195
        filt_eff = 0.45627 #0.456
        if with_init: init_events = 6.9573584*(rel==20)+2.91e+10*(rel==21)
        return xs*k*filt_eff/init_events
 
    #SMNR
    if label == 'SMNR':
        print('Getting xs factors for SMNR')

        xs = 27.47
        k = 1.13
        filt_eff = 0.3392
        if with_init: init_events = 10708.3*(rel==20)+26458.*(rel==21) 
        return xs*k*filt_eff/init_events

    if label == 'g10_M400':
        print('Getting xs factors for g10_M400')

        if updated:
            xs = 2.74e+03
            k = 1.
            filt_eff = 0.3392
        else:
            xs = 1898.7 
            k = 1.
            filt_eff = 0.3392
        if with_init: init_events = 99800*(rel==20)+100000*(rel==21)
        return xs*k*filt_eff/init_events

    if label == 'g10_M500':
        print('Getting xs factors for g10_M500')

        if updated:
            xs = 1.45e+03
            k = 1.
            filt_eff = 0.3392
        else:
            xs = 891.44
            k = 1.
            filt_eff = 0.3392
        if with_init: init_events = 94400*(rel==20)+95000*(rel==21)
        return xs*k*filt_eff/init_events

    if label == 'g10_M600':
        print('Getting xs factors for g10_M600')

        if updated:
            xs = 6.76e+02
            k = 1.
            filt_eff = 0.3392
        else:
            xs = 410.18
            k = 1.
            filt_eff = 0.3392
        if with_init: init_events = 99800*(rel==20)+100000*(rel==21)
        return xs*k*filt_eff/init_events

    if label == 'g10_M700':
        print('Getting xs factors for g10_M700')

        if updated:
            xs = 3.35e+02
            k = 1.
            filt_eff = 0.3392
        else:
            xs = 201.4
            k = 1.
            filt_eff = 0.3392
        if with_init: init_events = 54800*(rel==20)+55000*(rel==21)
        return xs*k*filt_eff/init_events

    if label == 'g10_M800':
        print('Getting xs factors for g10_M800')

        if updated:
            xs = 1.77e+02
            k = 1.
            filt_eff = 0.3392
        else:
            xs = 105.44
            k = 1.
            filt_eff = 0.3392
        if with_init: init_events = 70000*(rel==20)+70000*(rel==21)
        return xs*k*filt_eff/init_events

    if label == 'g10_M900':
        print('Getting xs factors for g10_M900')

        if updated:
            xs = 9.63e+01
            k = 1.
            filt_eff = 0.3392
        else:
            xs = 58.282
            k = 1.
            filt_eff = 0.3392
        if with_init: init_events = 83000*(rel==20)+85000*(rel==21)
        return xs*k*filt_eff/init_events

    if label == 'g10_M1000':
        print('Getting xs factors for g10_M1000')

        if updated:
            xs = 5.59e+01
            k = 1.
            filt_eff = 0.3392
        else:
            xs = 33.696
            k = 1.
            filt_eff = 0.3392
        if with_init: init_events = 100000*(rel==20)+100000*(rel==21)
        return xs*k*filt_eff/init_events

    if label == 'scalar_M251':
        print('Getting xs factors for scalar_M251')
        xs = 79.626*1e3
        k = 1.
        filt_eff = 0.3392
        if with_init: init_events = 100000*(rel==20)+100000*(rel==21)
        return xs*k*filt_eff/init_events

    if label == 'scalar_M260':
        print('Getting xs factors for scalar_M260')
        xs = 224.55*1e3
        k = 1.
        filt_eff = 0.3392
        if with_init: init_events = 100000*(rel==20)+100000*(rel==21)
        return xs*k*filt_eff/init_events

    if label == 'scalar_M280':
        print('Getting xs factors for scalar_M280')
        xs = 308.42*1e3
        k = 1.
        filt_eff = 0.3392
        if with_init: init_events = 100000*(rel==20)+100000*(rel==21)
        return xs*k*filt_eff/init_events

    if label == 'scalar_M300':
        print('Getting xs factors for scalar_M300')
        xs = 326.84*1e3
        k = 1.
        filt_eff = 0.3392
        if with_init: init_events = 100000*(rel==20)+100000*(rel==21)
        return xs*k*filt_eff/init_events

    if label == 'scalar_M350':
        print('Getting xs factors for scalar_M300')
        xs = 367.26*1e3
        k = 1.
        filt_eff = 0.3392
        if with_init: init_events = 100000*(rel==20)+100000*(rel==21)
        return xs*k*filt_eff/init_events

    if label == 'scalar_M400':
        print('Getting xs factors for scalar_M400')
        xs = 336.43*1e3
        k = 1.
        filt_eff = 0.3392
        if with_init: init_events = 100000*(rel==20)+100000*(rel==21)
        return xs*k*filt_eff/init_events 

    if label == 'scalar_M500':
        print('Getting xs factors for scalar_M500')
        xs = 145.41*1e3
        k = 1.
        filt_eff = 0.3392
        if with_init: init_events = 100000*(rel==20)+100000*(rel==21)
        return xs*k*filt_eff/init_events

    if label == 'scalar_M600':
        print('Getting xs factors for scalar_M600')
        xs = 57.092*1e3
        k = 1.
        filt_eff = 0.3392
        if with_init: init_events = 100000*(rel==20)+100000*(rel==21)
        return xs*k*filt_eff/init_events 

    if label == 'scalar_M700':
        print('Getting xs factors for scalar_M700')
        xs = 23.478*1e3
        k = 1.
        filt_eff = 0.3392
        if with_init: init_events = 100000*(rel==20)+100000*(rel==21)
        return xs*k*filt_eff/init_events

    if label == 'scalar_M800':
        print('Getting xs factors for scalar_M800')
        xs = 10.295*1e3
        k = 1.
        filt_eff = 0.3392
        if with_init: init_events = 100000*(rel==20)+100000*(rel==21)
        return xs*k*filt_eff/init_events
  
    if label == 'scalar_M900':
        print('Getting xs factors for scalar_M900')
        xs = 4.7977*1e3
        k = 1.
        filt_eff = 0.3392
        if with_init: init_events = 100000*(rel==20)+100000*(rel==21)
        return xs*k*filt_eff/init_events

    print("Label not found! Returning 1.")
    return 1
