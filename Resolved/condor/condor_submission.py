#!/usr/bin/env python3
import subprocess
import os

from optparse import OptionParser

parser = OptionParser()
parser.add_option("-i", "--input_dir", dest="input_dir", default="",
                  help="Input directory")
parser.add_option("-d", "--data", dest="data_file", default="",
                  help="Data file")
parser.add_option("-t", "--tth", dest="tth_file", default="",
                  help="Input all had ttbar filename")
parser.add_option("-n", "--tnh", dest="ttnh_file", default="",
                  help="Input non all-had ttbar filename")
parser.add_option("-o", "--output-dir", dest="output_dir", default="",
                  help="Directory to send output")
parser.add_option("-y", "--year", dest="year_in",  default="all",
                  help="Years to submit")
parser.add_option("-l", "--label", dest="label", default="",
                  help="Label (pflow, e.g.)")
parser.add_option("-b", "--bdt",
                  action="store_true", dest="bdt", default=False,
                  help="Turn on run for BDT reweighting")
parser.add_option("-s", "--spline", dest="spline", default="-1",
                  help="Turn on run for spline reweighting, give number of iters")
parser.add_option("--doCRw",
                  action="store_true", dest="doCRw", default=False,
                  help="Derive control region weights")
parser.add_option("--submit", 
                  action="store_true",dest="submit", default=False,
                  help="Submit to Condor")

(options, args) = parser.parse_args()

input_dir = options.input_dir
data_file = options.data_file
tth_file = options.tth_file
ttnh_file = options.ttnh_file
year_in = options.year_in
label = options.label
output_dir = options.output_dir
submit = options.submit
doBDT = options.bdt
doCRw = options.doCRw

n_it = int(options.spline)
doSpline = (n_it != -1)

CRlabel=''
if doCRw:
    CRlabel='_withCR'

if input_dir:
    if input_dir[-1] != '/': input_dir+='/'

if data_file:
    if data_file[0] == '/':
        input_dir = ''
    data_file = input_dir+data_file

    data_dir = os.path.dirname(data_file)
    data_file_name = os.path.basename(data_file)
    data_file_label = data_file_name[:-5]
    if data_dir:
        if data_dir[-1] != '/':
            data_dir+='/'

if tth_file:
    if tth_file[0] == '/':
        input_dir = ''
    tth_file = input_dir+tth_file

    tth_dir = os.path.dirname(tth_file)
    tth_file_name = os.path.basename(tth_file)
    tth_file_label = tth_file_name[:-5]
    if tth_dir:
        if tth_dir[-1] != '/':
            tth_dir+='/'

if ttnh_file:
    if ttnh_file[0] == '/':
        input_dir = ''
    ttnh_file = input_dir+ttnh_file

    ttnh_dir = os.path.dirname(ttnh_file)
    ttnh_file_name = os.path.basename(ttnh_file)
    ttnh_file_label = ttnh_file_name[:-5]
    if ttnh_dir:
        if ttnh_dir[-1] != '/':
            ttnh_dir+='/'

if output_dir:
    if output_dir[-1] != '/':
        output_dir+= '/'

years = ["15", "16", "17", "18"]
if year_in != "all":
    if len(year_in)==4:
        years = [year_in[-2:]]
    else:
        if year_in in years:
            years = [year_in]
        else:
            print("Unrecognized option for year! Check inputs")
            years = []

label_opt = ""
if label:
    if label[0] != '_':
        label='_'+label
    label_opt = "-l " + label[1:]
   
for year in years:
    in_transfer_list = ["../rw_utils.py", "../combine_weights.py", data_file]

    method = ""
    comb_opt = ""
    file_opt = ""
    file_opt_CRw = ""
    if doBDT:
        method+="_BDT"
        comb_opt+="-b "
        file_opt="-d "+data_file_name
        file_opt_CRw="-d "+data_file_name[:-5]+"_with_weights.root"
        in_transfer_list+=["../BDT_RW.py", "../BDTopt/hep_ml.tar.gz"]

    if doSpline:
        method+="_spline"
        comb_opt+=("-s %d " % n_it)
        file_opt = "-d "+data_file_name+" -t "+tth_file_name+" -n "+ttnh_file_name
        file_opt_CRw = "-d "+data_file_name[:-5]+"_with_weights.root"+" -t "+tth_file_name[:-5]+"_with_weights.root"+" -n "+ttnh_file_name[:-5]+"_with_weights.root"
        in_transfer_list += ["../reweight.py", tth_file, ttnh_file]

    if not doBDT and not doSpline:
        break

    out_transfer = "outputs.tar.gz\n"
    out_remaps = "\"outputs.tar.gz = "+output_dir+"outputs"+year+label+method+CRlabel+".tar.gz\"\n"

    in_transfer=""
    to_delete=""
    for in_idx in range(len(in_transfer_list)):
        fname = in_transfer_list[in_idx]
        if in_idx != len(in_transfer_list)-1:
            to_delete+=(os.path.basename(fname)+" ")
            in_transfer+=(fname+",")
        else:
            to_delete+=(os.path.basename(fname)+"\n")
            in_transfer+=(fname+"\n")


    config_name = "submit"+year+label+method+CRlabel+".sub"
    exec_name = "submit"+year+label+method+CRlabel+".sh"
    outputs_name = "outputs"+year+label+method+CRlabel
    print("Writing config/executables", config_name, "and", exec_name, "for method", method)

    with open(config_name, "w") as config:
        config.write("universe = vanilla\n")
        config.write("Executable = "+exec_name+"\n")
        config.write("arguments = $(ClusterID) $(ProcId)\n")
        config.write("Output = output/back"+year+".$(ClusterId).$(ProcId).out\n")
        config.write("Error = error/back"+year+".$(ClusterId).$(ProcId).err\n")
        config.write("Log = log/back"+year+".$(ClusterId).$(ProcId).log\n")
        config.write("request_memory = 32GB\n")
        config.write("+JobFlavour = \"workday\"\n")
        config.write("transfer_input_files = "+in_transfer) 
        config.write("transfer_output_files = "+out_transfer)
        config.write("transfer_output_remaps = "+out_remaps)
        config.write("when_to_transfer_output = ON_EXIT\n")
        config.write("max_transfer_output_mb = -1\n")
        config.write("Queue 1")

    with open(exec_name, "w") as fexec:
        fexec.write("#!/bin/bash\n")
        fexec.write("echo \"We are node $2\"\n")
        fexec.write("source /cvmfs/sft.cern.ch/lcg/views/LCG_94python3/x86_64-centos7-gcc8-opt/setup.sh\n")
        fexec.write("echo \"About to unpack hep_ml tarball\"\n")
        fexec.write("tar -zxf hep_ml.tar.gz\n")
        fexec.write("echo \"Unpacked tarball\"\n")
        fexec.write("ls -lth\n")
        if doBDT:
            fexec.write("./BDT_RW.py -d "+data_file_name+" -r 21 -y 20"+year+" "+label_opt+"\n")
            if doCRw:
                fexec.write("./BDT_RW.py -d "+data_file_name+" -r 21 -y 20"+year+" "+label_opt+" --reweight-tree control\n")
        if doSpline:
            fexec.write("./reweight.py -d "+data_file_name+" -t "+tth_file_name+" -n "+ttnh_file_name+" -r 21 -y 20"+year+" "+label_opt+"\n")
            if doCRw:
                fexec.write("./reweight.py -d "+data_file_name+" -t "+tth_file_name+" -n "+ttnh_file_name+" -r 21 -y 20"+year+" "+label_opt+" --reweight-tree control\n")
        fexec.write("./combine_weights.py "+file_opt+" -r 21 -y 20"+year+" -w . "+comb_opt+label_opt+"\n")
        if doCRw:
            fexec.write("./combine_weights.py "+file_opt_CRw+" -r 21 -y 20"+year+" -w . "+comb_opt+label_opt+" --isCRderiv -o with_weights\n")
        fexec.write("mkdir "+outputs_name+"\n")
        fexec.write("rm "+to_delete)
        fexec.write("mv *.root "+outputs_name+"\n")
        fexec.write("tar -cvzf outputs.tar.gz "+outputs_name+"\n")
        fexec.write("ls -lth\n")

    

    if submit:
        print("Running submission for year 20"+year)
        print(subprocess.check_output(["condor_submit", config_name]).decode("utf-8"))
