#!/bin/bash

./condor_submission.py -i /eos/user/s/sgasioro/public/nano_tuples/rel21/ -d data15_rel21.root -t ttbar_allhad_15-16_rel21.root -n ttbar_nonallhad_15-16_rel21.root -o /eos/user/s/sgasioro/private/condor_out/ -y 2015 -b -s 7 --doCRw --submit
./condor_submission.py -i /eos/user/s/sgasioro/public/nano_tuples/rel21/ -d data16_rel21.root -t ttbar_allhad_15-16_rel21.root -n ttbar_nonallhad_15-16_rel21.root -o /eos/user/s/sgasioro/private/condor_out/ -y 2016 -b -s 7 --doCRw --submit
./condor_submission.py -i /eos/user/s/sgasioro/public/nano_tuples/rel21/ -d data17_rel21.root -t ttbar_allhad_17_rel21.root -n ttbar_nonallhad_17_rel21.root -o /eos/user/s/sgasioro/private/condor_out/ -y 2017 -b -s 7 --doCRw --submit
./condor_submission.py -i /eos/user/s/sgasioro/public/nano_tuples/rel21/ -d data18_rel21.root -t ttbar_allhad_18_rel21.root -n ttbar_nonallhad_18_rel21.root -o /eos/user/s/sgasioro/private/condor_out/ -y 2018 -b -s 7 --doCRw --submit
