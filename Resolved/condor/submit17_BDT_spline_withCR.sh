#!/bin/bash
echo "We are node $2"
source /cvmfs/sft.cern.ch/lcg/views/LCG_94python3/x86_64-centos7-gcc8-opt/setup.sh
echo "About to unpack hep_ml tarball"
tar -zxf hep_ml.tar.gz
echo "Unpacked tarball"
ls -lth
./BDT_RW.py -d data17.root -r 21 -y 2017 
./BDT_RW.py -d data17.root -r 21 -y 2017  --reweight-tree control
./reweight.py -d data17.root -t ttbar_allhad_17_rel21.root -n ttbar_nonallhad_17_rel21.root -r 21 -y 2017 
./reweight.py -d data17.root -t ttbar_allhad_17_rel21.root -n ttbar_nonallhad_17_rel21.root -r 21 -y 2017  --reweight-tree control
./combine_weights.py -d data17.root -t ttbar_allhad_17_rel21.root -n ttbar_nonallhad_17_rel21.root -r 21 -y 2017 -w . -b -s 7 
./combine_weights.py -d data17_with_weights.root -t ttbar_allhad_17_rel21_with_weights.root -n ttbar_nonallhad_17_rel21_with_weights.root -r 21 -y 2017 -w . -b -s 7  --isCRderiv -o with_weights
mkdir outputs17_BDT_spline_withCR
rm rw_utils.py combine_weights.py data17.root BDT_RW.py hep_ml.tar.gz reweight.py ttbar_allhad_17_rel21.root ttbar_nonallhad_17_rel21.root
mv *.root outputs17_BDT_spline_withCR
tar -cvzf outputs.tar.gz outputs17_BDT_spline_withCR
ls -lth
