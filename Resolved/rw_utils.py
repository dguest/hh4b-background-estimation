#!/usr/bin/env python3

import numpy as np
from scipy import stats
from scipy.special import binom

def rel20to21_dict():
    rel20to21 = {}
    rel20to21['event_number'] = 'event_number'
 
    rel20to21['n_jets'] = 'njets'
    rel20to21['n_tag'] = 'ntag'
    rel20to21['njet_weight'] = 'njet_weight'
    rel20to21['rw_weight'] ='rw_weight'

    #Only in rel20
    rel20to21['Xwt'] = 'X_wt'
    rel20to21['Xhh'] = 'Xhh'
    rel20to21['passTrig_15'] = 'passTrig_15'
    rel20to21['passTrig_16'] = 'passTrig_16'
    rel20to21['nmuon_isIsolated_custom'] = 'nmuon_isIsolated_custom'
    rel20to21['bFix70_weight'] = 'bFix70_weight'
    rel20to21['mc_weight'] = 'mc_weight'
    rel20to21['muon_pt'] = 'muon_pt'

    #Event variables
    rel20to21['event.m_4j'] = 'm_hh'

    rel20to21['event.pT_h1'] = 'pT_h1'
    rel20to21['event.eta_h1'] = 'eta_h1'
    rel20to21['event.phi_h1'] = 'phi_h1'
    rel20to21['event.m_h1'] = 'm_h1'
    rel20to21['event.E_h1'] = 'E_h1'

    rel20to21['event.pT_h2'] = 'pT_h2'
    rel20to21['event.eta_h2'] = 'eta_h2'
    rel20to21['event.phi_h2'] = 'phi_h2'
    rel20to21['event.m_h2'] = 'm_h2'
    rel20to21['event.E_h2'] = 'E_h2'

    rel20to21['event.pT_h1_j1'] = 'pT_h1_j1'
    rel20to21['event.eta_h1_j1'] = 'eta_h1_j1'
    rel20to21['event.phi_h1_j1'] = 'phi_h1_j1'
    rel20to21['event.m_h1_j1'] = 'm_h1_j1'
    rel20to21['event.E_h1_j1'] = 'E_h1_j1'

    rel20to21['event.pT_h1_j2'] = 'pT_h1_j2'
    rel20to21['event.eta_h1_j2'] = 'eta_h1_j2'
    rel20to21['event.phi_h1_j2'] = 'phi_h1_j2'
    rel20to21['event.m_h1_j2'] = 'm_h1_j2'
    rel20to21['event.E_h1_j2'] = 'E_h1_j2'

    rel20to21['event.pT_h2_j1'] = 'pT_h2_j1'
    rel20to21['event.eta_h2_j1'] = 'eta_h2_j1'
    rel20to21['event.phi_h2_j1'] = 'phi_h2_j1'
    rel20to21['event.m_h2_j1'] = 'm_h2_j1'
    rel20to21['event.E_h2_j1'] = 'E_h2_j1'

    rel20to21['event.pT_h2_j2'] = 'pT_h2_j2'
    rel20to21['event.eta_h2_j2'] = 'eta_h2_j2'
    rel20to21['event.phi_h2_j2'] = 'phi_h2_j2'
    rel20to21['event.m_h2_j2'] = 'm_h2_j2'
    rel20to21['event.E_h2_j2'] = 'E_h2_j2'

    rel20to21['event.pT_h2'] = 'pT_h2'
    rel20to21['event.eta_h2'] = 'eta_h2'
    rel20to21['event.phi_h2'] = 'phi_h2'
    rel20to21['event.m_h2'] = 'm_h2' 
   
    rel20to21['event.m_4j'] = 'm_hh'
 
    rel20to21['pT_h1'] = 'pT_h1'
    rel20to21['eta_h1'] = 'eta_h1'
    rel20to21['phi_h1'] = 'phi_h1'
    rel20to21['m_h1'] = 'm_h1'

    rel20to21['pT_h2'] = 'pT_h2'
    rel20to21['eta_h2'] = 'eta_h2'
    rel20to21['phi_h2'] = 'phi_h2'
    rel20to21['m_h2'] = 'm_h2'

    rel20to21['m_4j'] = 'm_hh'
 
    #Reweight variables
    rel20to21['rw_vals.pT_4'] = 'pT_4'
    rel20to21['rw_vals.pT_2'] = 'pT_2'
    rel20to21['rw_vals.ave_eta'] = 'eta_i'
    rel20to21['rw_vals.dRjj_close'] = 'dRjj_1'
    rel20to21['rw_vals.dRjj_notclose'] = 'dRjj_2'
    rel20to21['pT_4'] = 'pT_4'
    rel20to21['pT_2'] = 'pT_2'
    rel20to21['ave_eta'] = 'eta_i'
    rel20to21['dRjj_close'] = 'dRjj_1'
    rel20to21['dRjj_notclose'] = 'dRjj_2'
    
    #Weights
    rel20to21['BDT_d24_weight'] = 'BDT_d24_weight'
    rel20to21['BDT_d24_weight_15'] = 'BDT_d24_weight_15'
    rel20to21['BDT_d24_weight_16'] = 'BDT_d24_weight_16'
    rel20to21['BDT_d24_weight_17'] = 'BDT_d24_weight_17'
    rel20to21['BDT_d24_weight_18'] = 'BDT_d24_weight_18'
    for it in range(10):
        rel20to21['njet_weight_it_%d' % it] = 'njet_weight_it_%d' % it
        rel20to21['rw_weight_it_%d' % it] = 'rw_weight_it_%d' % it
   
        rel20to21['njet_weight_it_%d_15' % it] = 'njet_weight_it_%d_15' % it
        rel20to21['rw_weight_it_%d_15' % it] = 'rw_weight_it_%d_15' % it
  
        rel20to21['njet_weight_it_%d_16' % it] = 'njet_weight_it_%d_16' % it
        rel20to21['rw_weight_it_%d_16' % it] = 'rw_weight_it_%d_16' % it
   
        rel20to21['njet_weight_it_%d_17' % it] = 'njet_weight_it_%d_17' % it
        rel20to21['rw_weight_it_%d_17' % it] = 'rw_weight_it_%d_17' % it

        rel20to21['njet_weight_it_%d_18' % it] = 'njet_weight_it_%d_18' % it
        rel20to21['rw_weight_it_%d_18' % it] = 'rw_weight_it_%d_18' % it

    return rel20to21

def vanillize_name(branchname):
    if branchname.find('BDT_d24_weight') != -1:
        if branchname.find('CRderiv') != -1:
            return 'BDT_d24_weight_CRderiv'
        else:
            return 'BDT_d24_weight'
    if branchname.find('njet_weight') != -1:
        if branchname.find('CRderiv') != -1:
            return 'njet_weight_CRderiv'
        else:
            return 'njet_weight'
    if branchname.find('rw_weight') != -1:
        if branchname.find('CRderiv') != -1:
            return 'rw_weight_CRderiv'
        else:
            return 'rw_weight'

    return branchname

#Fit function
def f(x, p0, p1, p2, fit_counts):
    ret_arr = np.zeros(len(x))

    #Frame as a 3 binned step function [0,1) is tnh, [1,2) is th, [2,3) is qcd region.
    #Note that in tnh we force qcd counts to 0 - this is by construction with the 2 tag tnh scale factor
    #fit_counts is a dict of dicts indexed as fit_counts[data type key][fit region]
    for i in range(len(x)):
        if(0 <= x[i] < 1):
                val=p0*fit_counts['tnh4']['tnh'] + p1*fit_counts['th4']['tnh'] + p2*fit_counts['qcd']['tnh']*0
                ret_arr[i] = val
        if(1 <= x[i] < 2):
                val=p0*fit_counts['tnh4']['th'] + p1*fit_counts['th4']['th'] + p2*fit_counts['qcd']['th']
                ret_arr[i] = val
        if(2 <= x[i] < 3):
                val=p0*fit_counts['tnh4']['qcd'] + p1*fit_counts['th4']['qcd'] + p2*fit_counts['qcd']['qcd']
                ret_arr[i] = val
    return ret_arr

def makeMCweights(arrs, key, lumi, rel=20, region=''):
    arr = arrs[key]
    if region:
        arr = arr[region]
    mc_weights = np.ones(len(arr))

    if key.find('th') != -1:
        if rel == 20:
            xs = 729780. #696210.
            k = 1.13974 #1.195
            br = 0.45627 #0.456
           # init_events = 2.91e+10
            init_events = 6.9573584*(rel==20)+2.91e+10*(rel==21)
            mc_weights = (xs*br*lumi*k/init_events)*arr['bFix70_weight']*arr['mc_weight']
        else:
            mc_weights = arr['mc_sf']*lumi

    if key.find('tnh') != -1:
        if rel == 20:
            xs = 729770 #696110.
            k = 1.13976 #1.195
            br = 0.54384 #0.54383
           # init_events = 8.72e+10
            init_events = 49386600*(rel==20) + 8.72e+10*(rel==21)
            mc_weights = (xs*br*lumi*k/init_events)*arr['bFix70_weight']*arr['mc_weight']
        else:
            mc_weights = arr['mc_sf']*lumi

    return mc_weights

#Calculate weights from stored values ('njet_weight', 'rw_weight', 'mc_weight', etc)
def makeWeights(arrs, key, it, year, rel=20, withnjet=True, region='', doCRw=False):
    arr = arrs[key]
    if region:
        arr=arr[region]
    nj_weight=[]
    year=int(year)
    rel=int(rel)

    deriv_label=''
    if doCRw:
        deriv_label='_CRderiv'

    #Bit of non-trivial logic - if th4, want njet weight so that it's actually 4 tag model, always
    if key == 'th4':
        if rel == 20:
            nj_weight = arr['njet_weight'+deriv_label].copy()
        else:
            nj_weight = np.ones(len(arr))
 
    #If want weight with nJet, split by iteration - for it 0, default is f=0.22
    #otherwise take it from stored column. For 4 tag, always have ones
    elif withnjet:
        if it == 0:
            if key.find('2') != -1:
                nj_weight = nJetWeight(arr, 0.22)
            else:
                nj_weight = np.ones(len(arr))
        else:
            if key.find('2') != -1:
                nj_weight = arr['njet_weight'+deriv_label].copy()
            else:
                nj_weight = np.ones(len(arr))

    #If not th4 and don't want njet, just have njet weight of one
    else:
        nj_weight = np.ones(len(arr))
        
    #Base calc weight value is njet*mc*rw - should be 1 for data, by design
    weights = nj_weight*arr['rw_weight'+deriv_label]

    #For MC, need to scale by cross section and luminosity, as well as b-tagging weights
    #Trigger scale factors need to be added as well, but this is a small effect
    lumi15 = 3.23749*(rel==20)+3.2195*(rel==21)
    lumi16 = 24.3219*(rel==20)+24.5556*(rel==21)
    lumi17 = 44.3072
    lumi18 = 36.1593
    lumi = (year==2015)*lumi15 + (year==2016)*lumi16 + (year==2017)*lumi17 + (year==2018)*lumi18

    #For rel21 15+16, overall normalization is to combined 15+16 lumi
    if rel == 21 and (year == 2015 or year == 2016):
        lumi = lumi15+lumi16
    
    if rel == 20 and (year == 2017 or year == 2018):
        print('Warning! Rel 20 should not exist for', year,'. Continuing, but check inputs!')
 
    weights*=makeMCweights(arrs, key, lumi, rel, region)

    return weights


#Calculate counts in fit regions. Errors are sqrt(sum of w^2)
def getFitCounts(arrs, weights, key, region, rel=20):
    arr = arrs[key]
    weight = weights[key]

    if rel == 20:
        #tnh region is number of prompt muons > 0
        if region == 'tnh':
            count = np.sum(weight[(arr['nmuon_isIsolated_custom']>0)])#& (arr['X_wt'] < 1.5)])
            err = np.sqrt(np.sum((weight[(arr['nmuon_isIsolated_custom']>0)])**2))
            return count, err 

        #qcd region is Xwt > 0.75
        if region == 'qcd':
            count = np.sum(weight[(arr['X_wt'] > 0.75)])
            err = np.sqrt(np.sum((weight[(arr['X_wt'] > 0.75)])**2))
            return count, err 

        #th region is Xwt < 0.75
        if region == 'th':
            count = np.sum(weight[(arr['X_wt'] < 0.75)])
            err = np.sqrt(np.sum((weight[(arr['X_wt'] < 0.75)])**2))
            return count, err 

    else: #21, regions handled in tree
        count = np.sum(weight[region])
        err = np.sqrt(np.sum(weight[region]**2))
        return count, err

    print("Invalid regions?")
    return 0,0

#reg = SB, SR, CR
def whichRegions(full):
    mlead = full['m_h1']
    msubl = full['m_h2']

    Xhh = np.sqrt(((mlead-120.)/(0.1*mlead))**2+((msubl-110.)/(0.1*msubl))**2)
    SB_hh = np.sqrt((mlead - (120*1.05))**2 + (msubl - (110*1.05))**2)
    CR_hh = np.sqrt((mlead - (120*1.03))**2 + (msubl - (110*1.03))**2)

    isSR = (Xhh < 1.6)
    isSB = (SB_hh < 45)
    isCR = (CR_hh < 30)

    return {"SB" : isSB, "SR":isSR, "CR":isCR }

#Select SB region
def SBsel(full):
    mlead = full['m_h1']
    msubl = full['m_h2']

    Xhh = np.sqrt(((mlead-120.)/(0.1*mlead))**2+((msubl-110.)/(0.1*msubl))**2)
    SB_hh = np.sqrt((mlead - (120*1.05))**2 + (msubl - (110*1.05))**2)
    CR_hh = np.sqrt((mlead - (120*1.03))**2 + (msubl - (110*1.03))**2)

    notSR = (Xhh >= 1.6)
    isSB = (SB_hh < 45)
    notCR = (CR_hh >= 30)

    return full[(isSB) & (notCR) & (notSR)]

#Select CR
def CRsel(full):
    mlead = full['m_h1']
    msubl = full['m_h2']

    Xhh = np.sqrt(((mlead-120.)/(0.1*mlead))**2+((msubl-110.)/(0.1*msubl))**2)
    SB_hh = np.sqrt((mlead - (120*1.05))**2 + (msubl - (110*1.05))**2)
    CR_hh = np.sqrt((mlead - (120*1.03))**2 + (msubl - (110*1.03))**2)

    notSR = (Xhh >= 1.6)
    notSB = (SB_hh >= 45)
    isCR = (CR_hh < 30)
    
    return full[(isCR) & (notSR)]

#Select SR
def SRsel(full):
    mlead = full['m_h1']
    msubl = full['m_h2']

    Xhh = np.sqrt(((mlead-120.)/(0.1*mlead))**2+((msubl-110.)/(0.1*msubl))**2)

    isSR = (Xhh < 1.6)

    return full[(isSR)]

#Calculate reweighting weight from splines
def calc_Weight(fns, arr, rw_columns, it, doCRw=False):
    deriv_label=''
    if doCRw:
        deriv_label='_CRderiv'

    rw_weight = arr['rw_weight'+deriv_label].copy()
    twotag_arr = (arr['ntag']==2)
    
    for var in rw_columns:
        it_weight = ((fns[var](arr[var])-1)*(1-2**(-it-1))+1)*twotag_arr + 1*(~twotag_arr)
        rw_weight *= it_weight

    return rw_weight

#Calculate nJet weight
def nJetWeight(arr, f):
    njets = np.array(arr['njets'])
    ntag = np.array(arr['ntag'])
    
    nj_weight_it = 0
    n_nontag = njets - ntag
   
    if len(njets) == 0:
        return np.array([])  
    #Optimization thing - make bank of possible values for nJet weight (only a few)
    #depends on number of nontagged jets only - max and min give the range
    max_nontag = n_nontag.max()
    min_nontag = n_nontag.min()
    
    #Initialize array of possible weights
    nj_weight_it = np.zeros(max_nontag+1 - min_nontag)
    
    #Initialize return array - default is 1 (four tag value)
    ret_weight = np.ones(len(n_nontag))
    
    #Make possible weight values for given arr
    for nontag in range(min_nontag, max_nontag+1):
        for n_choose in range(2,nontag+1):
            binomial = binom(nontag, n_choose)
            nj_weight_it[nontag-min_nontag] += binomial*(f**n_choose)*((1-f)**(nontag-n_choose))
    
    #Fill nJet weight array by grabbing appropriate values (faster than calculating each time)
    #Indexed, by design, at n_nontag - min_nontag
    for i in range(len(n_nontag)):
        if ntag[i] == 2:
            ret_weight[i] = nj_weight_it[n_nontag[i]-min_nontag]

    return ret_weight


#Calculate weighted chi^2 between hists - see later for usage
def weighted_chisquare(f_obs, f_exp, f_obs_err, f_exp_err):
    #Calculate weighted chi-square using method in arXiv:physics/0605123
    w1 = f_obs
    w2 = f_exp
    s1 = f_obs_err  # noqa
    s2 = f_exp_err  # noqa
    W1 = np.sum(w1)  # noqa
    W2 = np.sum(w2)  # noqa
    X2 = np.sum((W1*w2 - W2*w1)**2 / (W1**2 * s2**2 + W2**2 * s1**2))
    p = stats.chi2.sf(X2, np.size(w1) - 1)
    return (X2, p)

#Prevent errors with zeros by keeping things (slightly) nonzero
def makePositive(arr, arr_err):
    for i in range(len(arr)):
        if arr[i] <= 0:
            arr[i] = 0.000001
            arr_err[i] = 0.000001
    return arr, arr_err

def makePositive_hist(hist):
    for i in range(1, hist.GetNbinsX()+1):
        content = hist.GetBinContent(i)
        if content <= 0.0:
            hist.SetBinContent(i, 0.0000001)
            hist.SetBinError(i,0.0)
    return hist

#Find midpoints of fixed width bins array
def mid(bins):
    return (bins[1]-bins[0])*0.5+bins[:-1]

#Minimizer class for pseudo-tag optimization
class f_min:
    #Initialize with data/ttbar info (arrs), keys, and year. Need to also set sfs (done in it loop)
    def __init__(self, arrs, keys, year, rel=20, sf_vals={}, doCRw=False):
        self.arrs = arrs
        self.keys = keys
        self.sf_vals = sf_vals
        self.year = year
        self.rel = rel
        self.doCRw = doCRw
  
    #Set scale factors from fits
    def set_sfs(self, sf_vals):
        self.sf_vals = sf_vals 

    # tth f factor function to minimize. For a given f, calculates the difference in yield between
    # weighted 2 tag and real 4 tag.
    def to_min_tth(self, f):
        arrs = self.arrs
        nj_hists = {}
        weights_it = {}
  
        n_jets_2 = arrs['th2']['njets']
        n_tag_2 = arrs['th2']['ntag']

        n_jets_4 = arrs['th4_real']['njets']
        n_tag_4 = arrs['th4_real']['ntag']

        weights_it['th2'] = makeWeights(arrs, 'th2', 1, self.year, self.rel, False, doCRw=self.doCRw)*nJetWeight(arrs['th2'], f)
        weights_it['th4_real'] = makeWeights(arrs, 'th4_real', 1, self.year, self.rel, False, doCRw=self.doCRw)

        return abs(np.sum(weights_it['th2']) - np.sum(weights_it['th4_real']))

    # Pseudo-tag rate function to minimize (for all 2 tag). For a given f, sets up background and 4 tag data nJet histograms.
    # Returns chi2 between them.
    def to_min(self, f):
        arrs = self.arrs
        sf = self.sf_vals
        year =int(self.year)
        nj_hists = {}
        hist_errs = {}
        weights_it = {}
   
        #Key loop is over all data/ttbar regions
        for key in self.keys:
            n_jets = arrs[key]['njets']
            n_tag = arrs[key]['ntag']

            #Make weights with nJet weight for 2 tag, given f, without nJet for 4 tag
            if key.find('2') != -1:
                weights_it[key] = makeWeights(arrs, key, 1, year, self.rel, False, doCRw=self.doCRw)*nJetWeight(arrs[key], f)
            else:
                weights_it[key] = makeWeights(arrs, key, 1, year, self.rel, False, doCRw=self.doCRw)

            #Set up njet histograms in array (bins are all the same)
            nj_hists[key], bins = np.histogram(arrs[key]['njets']-4, bins=10, range=[-0.5,9.5], weights=weights_it[key])

            #Make errors - sqrt(sum of squares of weights) in each bin, needed for chi2 calc
            w_arr = []
            for i in range(len(bins)-1):
                 w_arr.append(np.sqrt(np.sum(np.array(weights_it[key][((arrs[key]['njets']-4)>=bins[i]) &
                                                    ((arrs[key]['njets']-4)< bins[i+1])])**2)))
            hist_errs[key] = np.array(w_arr)

        #Construct qcd and background histograms (qcd = dat2 - th2 - tnh2, with appropriate weights, back = qcd+th4+tnh4)
        nj_hists['qcd'] = nj_hists['dat2'] - nj_hists['th2'] - nj_hists['tnh2']*sf['tnh2']
        nj_hists['back'] = nj_hists['qcd']*sf['qcd'] + nj_hists['th4']*sf['th'] + nj_hists['tnh4']*sf['tnh']

        #Get final errors for histograms with standard error propagation
        hist_errs['qcd'] = np.sqrt(hist_errs['dat2']**2 + (hist_errs['th2'])**2 + (hist_errs['tnh2']*sf['tnh2'])**2)
        hist_errs['back'] = np.sqrt((hist_errs['qcd']*sf['qcd'])**2 + (hist_errs['th4']*sf['th'])**2 + (hist_errs['tnh4']*sf['tnh'])**2)

        #Remove zeros so that chi2 doesn't toss errors
        nj_hists['back'], hist_errs['back'] = makePositive(nj_hists['back'], hist_errs['back'])
        nj_hists['dat4'], hist_errs['dat4'] = makePositive(nj_hists['dat4'], hist_errs['dat4'])

        #Calculate and return chi2
        chi2 = weighted_chisquare(nj_hists['back'], nj_hists['dat4'], hist_errs['back'], hist_errs['dat4'])[0]
        return chi2

def corr_mass(h1, h2):
    if h1.M():
        alpha1 = 125.0/h1.M()
    else:
        alpha1 = 1.0
    h1cor = h1 * alpha1

    if h2.M():
        alpha2 = 125.0/h2.M()
    else:
        alpha2 = 1.0
    h2cor = h2 * alpha2

    return (h1cor+h2cor).M()

